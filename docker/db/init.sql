CREATE OR REPLACE USER 'langunion'@'localhost' IDENTIFIED BY 'langunionpassword';
CREATE OR REPLACE USER 'langunion'@'172.17.0.1' IDENTIFIED BY 'langunionpassword'; -- docker
GRANT ALL PRIVILEGES ON languniondb.* TO 'langunion'@'localhost';
GRANT ALL PRIVILEGES ON languniondb.* TO 'langunion'@'172.17.0.1'; -- docker
-- Added for integration test using sqlx
GRANT ALL PRIVILEGES ON *.* TO 'langunion'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'langunion'@'172.17.0.1'; -- docker
FLUSH PRIVILEGES;
