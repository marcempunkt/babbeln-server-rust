#!/bin/bash
docker pull mariadb:latest
cargo install sqlx-cli

docker run --name langunion_db \
       -p 3306:3306 \
       -e MARIADB_ROOT_USER=langunion \
       -e MARIADB_ROOT_PASSWORD=langunionpassword \
       -e MARIADB_DATABASE=languniondb \
       -v $PWD/docker/db/init.sql:/docker-entrypoint-initdb.d/init.sql \
       -d mariadb:latest

echo "Wait before running migrate"
sleep 10

sqlx migrate run
