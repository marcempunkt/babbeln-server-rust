#!/bin/bash
#### --------------------------------------------- Check if docker-compose is installed
if ! [ -x "$(command -v docker-compose)" ]; then
  echo 'Error: docker-compose is not installed.' >&2
  exit 1
fi

#### --------------------------------------------- Variables
domains_langunionapp=(langunion.app www.langunion.app)
domains_serverlangunionapp=(server.langunion.app www.server.langunion.app)
domains_turnlangunionapp=(turn.langunion.app www.turn.langunion.app)
rsa_key_size=4096
data_path="./docker/web/data/certbot"
email="marc.maeurer@pm.me" # Adding a valid address is strongly recommended # TODO add email to dotenv
staging=0 # Set to 1 if you're testing your setup to avoid hitting request limits

#### --------------------------------------------- Check if there are already certs
if [ -d "$data_path" ]; then
  read -p "Existing data found for $domains. Continue and replace existing certificate? (y/N) " decision
  if [ "$decision" != "Y" ] && [ "$decision" != "y" ]; then
    exit
  fi
fi

#### --------------------------------------------- Download options-ssl-nginx.conf & ssl-dhparams.pem
if [ ! -e "$data_path/conf/options-ssl-nginx.conf" ] || [ ! -e "$data_path/conf/ssl-dhparams.pem" ]; then
  echo "### Downloading recommended TLS parameters ..."
  mkdir -p "$data_path/conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > "$data_path/conf/options-ssl-nginx.conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > "$data_path/conf/ssl-dhparams.pem"
  echo
fi

#### --------------------------------------------- Create dummy certificates 
## ----------------- Langunion.app
echo "### Creating dummy certificate for $domains_langunionapp ..."
path_langunionapp="/etc/letsencrypt/live/$domains_langunionapp"
mkdir -p "$data_path/conf/live/$domains_langunionapp"
docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
    -keyout '$path_langunionapp/privkey.pem' \
    -out '$path_langunionapp/fullchain.pem' \
    -subj '/CN=localhost'" langunion_web-certbot
echo
## ----------------- server.Langunion.app
echo "### Creating dummy certificate for $domains_serverlangunionapp ..."
path_serverlangunionapp="/etc/letsencrypt/live/$domains_serverlangunionapp"
mkdir -p "$data_path/conf/live/$domains_serverlangunionapp"
docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
    -keyout '$path_serverlangunionapp/privkey.pem' \
    -out '$path_serverlangunionapp/fullchain.pem' \
    -subj '/CN=localhost'" langunion_web-certbot
echo
## ----------------- turn.Langunion.app
echo "### Creating dummy certificate for $domains_turnlangunionapp ..."
path_turnlangunionapp="/etc/letsencrypt/live/$domains_turnlangunionapp"
mkdir -p "$data_path/conf/live/$domains_turnlangunionapp"
docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
    -keyout '$path_turnlangunionapp/privkey.pem' \
    -out '$path_turnlangunionapp/fullchain.pem' \
    -subj '/CN=localhost'" langunion_web-certbot
echo

#### --------------------------------------------- Start nginx-master container
echo "### Starting nginx ..."
docker-compose -f ./docker/web/docker-compose.yaml up --force-recreate -d langunion_web-nginx-master
echo

#### --------------------------------------------- Delete dummy certs
echo "### Deleting dummy certificate for $domains_langunionapp ..."
docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  rm -Rfv /etc/letsencrypt/live/$domains_langunionapp && \
  rm -Rfv /etc/letsencrypt/archive/$domains_langunionapp && \
  rm -Rfv /etc/letsencrypt/renewal/$domains_langunionapp.conf" langunion_web-certbot
echo
echo "### Deleting dummy certificate for $domains_serverlangunionapp ..."
docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  rm -Rfv /etc/letsencrypt/live/$domains_serverlangunionapp && \
  rm -Rfv /etc/letsencrypt/archive/$domains_serverlangunionapp && \
  rm -Rfv /etc/letsencrypt/renewal/$domains_serverlangunionapp.conf" langunion_web-certbot
echo
echo "### Deleting dummy certificate for $domains_turnlangunionapp ..."
docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  rm -Rfv /etc/letsencrypt/live/$domains_turnlangunionapp && \
  rm -Rfv /etc/letsencrypt/archive/$domains_turnlangunionapp && \
  rm -Rfv /etc/letsencrypt/renewal/$domains_turnlangunionapp.conf" langunion_web-certbot
echo

#### --------------------------------------------- Create let's encrypt certs
# Select appropriate email arg
case "$email" in
  "") email_arg="--register-unsafely-without-email" ;;
  *) email_arg="--email $email" ;;
esac

# Enable staging mode if needed
if [ $staging != "0" ]; then staging_arg="--staging"; fi

## ----------------- langunion.app
echo "### Requesting Let's Encrypt certificate for $domains_langunionapp ..."
domain_langunionapp_args=""
for domain in "${domains_langunionapp[@]}"; do
  domain_langunionapp_args="$domain_langunionapp_args -d $domain"
done

docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/ \
    $staging_arg \
    $email_arg \
    $domain_langunionapp_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" langunion_web-certbot
echo

## ----------------- server.langunion.app
echo "### Requesting Let's Encrypt certificate for $domains_serverlangunionapp ..."
domain_serverlangunionapp_args=""
for domain in "${domains_serverlangunionapp[@]}"; do
  domain_serverlangunionapp_args="$domain_serverlangunionapp_args -d $domain"
done

docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/ \
    $staging_arg \
    $email_arg \
    $domain_serverlangunionapp_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" langunion_web-certbot
echo

## ----------------- turn.langunion.app
echo "### Requesting Let's Encrypt certificate for $domains_turnlangunionapp ..."
domain_turnlangunionapp_args=""
for domain in "${domains_turnlangunionapp[@]}"; do
  domain_turnlangunionapp_args="$domain_turnlangunionapp_args -d $domain"
done

docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/ \
    $staging_arg \
    $email_arg \
    $domain_turnlangunionapp_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" langunion_web-certbot
echo

#### --------------------------------------------- Reload nginx-master with the let's encrypt SSL Certificates
echo "### Reloading nginx ..."
docker-compose -f ./docker/web/docker-compose.yaml exec langunion_web-nginx-master nginx -s reload
