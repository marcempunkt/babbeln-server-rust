#!/bin/bash
docker pull swaggerapi/swagger-ui:latest

docker run --name langunion_swagger \
       -p 8573:8080 \
       -v $PWD/openapi/:/openapid \
       -e SWAGGER_JSON=/openapid/openapi.yaml \
       --rm \
       -d swaggerapi/swagger-ui:latest

echo -e "\033[1;33m"
echo 
echo 
echo "Info: Swagger is being started inside a docker container."
echo "Any file upload (multipart/form-data) using the swagger-ui can lead to Network Errors"
echo 
echo "Swagger-UI startet on http://localhost:8573"
echo 
echo 
echo -e "\033[0m"
