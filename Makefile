start:
	@cargo run
build:
	@cargo build --release
install:
	@echo todo

swagger_start:
	@bash ./docker/swagger/create_swagger.sh
swagger_stop:
	@docker stop langunion_swagger

asyncapi_start:
	@echo todo
asyncapi_stop:
	@echo todo

db_create:
	@bash ./docker/db/create_db.sh
db_start:
	@docker start langunion_db
db_stop:
	@docker stop langunion_db
db_nuke:
	@cargo run-script db_stop && docker rm langunion_db
