<img 
src="https://cdn.rapidweb.biz/wp-content/uploads/2015/06/Open-Source-Logo-517x500.png"
align="right"
width="100"
height="100" />

# langunion-server 
> Where language and community meets.  
> Licensed under `GNU GPLv3 or later`

`langunion-server` is the back-end (http, socket, brain) server of the whole langunion ecosystem.

<div align="center">[Introduction](#Introduction) • [Install](#install) • [Contribute](#contribute)</div>

## What is __langunion__?

__langunion__ has everything that you'll need to learn any language. 
Mainly its a series of apps and services to make learning languages 
for my student as pleasant and easy as possible.

## Project Goal(s)

TODO

## Getting started

TODO

### Installation

TODO

### Scripts

`make <script-name>`

| Script name    | Description                |
|----------------|----------------------------|
| start          | Starts dev server          |
| build          | Builds production build    |
| install        | Locally install on machine |
| swagger_start  | Starts swagger ui          |
| swagger_stop   | Stops/removes swagger      |
| asyncapi_start | Starts asyncapi ui         |
| asyncapi_stop  | Stops/removes asyncapi ui  |
| db_create      | Create mariadb             |
| db_start       | Starts mariadb             |
| db_stop        | Stops mariadb              |
| db_nuke        | Removes mariadb            |


