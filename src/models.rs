use crate::dbclient::{DbClient, PostExt};
use crate::ws::server::WebSocketServerHandle;
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use sqlx::types::chrono::{DateTime, Utc};
use sqlx::FromRow;

// ------------------------------------------------------------------------- User
pub enum Status {
    On,
    Off,
    Dnd,
    Away,
}

impl Status {
    pub fn new(status: impl Into<String>) -> Self {
        match status.into().as_str() {
            "on" => Status::On,
            "off" => Status::Off,
            "dnd" => Status::Dnd,
            "away" => Status::Away,
            _ => Status::On,
        }
    }

    pub fn to_str(&self) -> &str {
        match self {
            Status::On => "on",
            Status::Off => "off",
            Status::Dnd => "dnd",
            Status::Away => "away",
        }
    }

    pub fn to_string(&self) -> String {
        self.to_str().to_string()
    }
}

fn get_user_url(domain: impl Into<String>, user_id: impl Into<usize>) -> String {
    let domain: String = domain.into();
    let user_id: usize = user_id.into();

    format!("{}/users/{}", domain, user_id)
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DbUser {
    pub id: u64,
    pub username: String,
    pub email: String,
    pub password: String,
    pub is_active: i8,
    pub status: String,
    pub register_date: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ClientUser {
    pub id: usize,
    pub username: String,
    pub register_date: DateTime<Utc>,
    pub status: String,
    pub avatar: String,
}

#[async_trait]
pub trait IntoClientUserExt {
    async fn into_client(
        self,
        domain: impl Into<String> + Send,
        websocket_server_handle: WebSocketServerHandle,
    ) -> ClientUser;
}

#[async_trait]
impl IntoClientUserExt for DbUser {
    async fn into_client(
        self,
        domain: impl Into<String> + Send,
        websocket_server_handle: WebSocketServerHandle,
    ) -> ClientUser {
        let domain: String = domain.into();
        let avatar: String = format!("{domain}/users/avatar/{}", self.id);

        let status: String = {
            let is_online = websocket_server_handle
                .is_user_connected(self.id as usize)
                .await;
            match is_online {
                true => self.status,
                false => "off".to_string(),
            }
        };

        ClientUser {
            id: self.id as usize,
            username: self.username.to_owned(),
            status,
            register_date: self.register_date,
            avatar,
        }
    }
}

// ------------------------------------------------------------------------- Token
#[derive(Serialize, Deserialize, Debug)]
pub struct DbToken {
    pub id: u64,
    pub user_id: u64,
    pub refresh: String,
}

// ------------------------------------------------------------------------- Friend
#[derive(Serialize, Deserialize, Debug)]
pub struct DbFriend {
    pub id: u64,
    pub user_one: u64,
    pub user_two: u64,
}

impl DbFriend {
    pub fn get_user_id_if_match(&self, user_id: usize) -> Option<usize> {
        let matches_one: bool = user_id == self.user_one as usize;
        let matches_two: bool = user_id == self.user_two as usize;

        if matches_one || matches_two {
            return Some(user_id);
        };

        None
    }

    pub fn get_user_id_of_friend(&self, user_id: usize) -> Option<usize> {
        let user_one: usize = self.user_one as usize;
        let user_two: usize = self.user_two as usize;
        let matches_one: bool = user_id == user_one;
        let matches_two: bool = user_id == user_two;

        if matches_one && !matches_two {
            return Some(user_two);
        }

        if matches_two && !matches_one {
            return Some(user_one);
        }

        None
    }
}

// ------------------------------------------------------------------------- Friendrequest
#[derive(Serialize, Deserialize, Debug)]
pub struct DbFriendrequest {
    pub id: u64,
    pub from_user: u64,
    pub to_user: u64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ClientFriendrequest {
    pub id: usize,
    pub from_user: String,
    pub to_user: String,
    pub accept: String,
    pub decline: String,
}

pub trait IntoClientFriendrequestExt {
    fn into_client(self, domain: impl Into<String>) -> ClientFriendrequest;
}

impl IntoClientFriendrequestExt for DbFriendrequest {
    fn into_client(self, domain: impl Into<String>) -> ClientFriendrequest {
        let domain: String = domain.into();
        let from_user: String = get_user_url(&domain, self.from_user as usize);
        let to_user: String = get_user_url(&domain, self.to_user as usize);
        let accept: String = format!("{}/friendrequests/accept/{}", &domain, self.id);
        let decline: String = format!("{}/friendrequests/decline/{}", &domain, self.id);

        ClientFriendrequest {
            id: self.id as usize,
            from_user,
            to_user,
            accept,
            decline,
        }
    }
}

// ------------------------------------------------------------------------- Blocked User
#[derive(Serialize, Deserialize)]
pub struct DbBlockedUser {
    pub id: u64,
    pub user_id: u64,
    pub blocked_user_id: u64,
}

// ------------------------------------------------------------------------- Message
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DbMessage {
    pub id: u64,
    pub sender: u64,
    pub receiver: u64,
    pub content: String,
    pub send_at: DateTime<Utc>,
    pub edited_at: Option<DateTime<Utc>>,
    pub is_deleted_by_sender: i8,
    pub is_deleted_by_receiver: i8,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ClientMessage {
    pub id: usize,
    pub sender: String,
    pub receiver: String,
    pub content: String,
    pub send_at: DateTime<Utc>,
    pub edited_at: Option<DateTime<Utc>>,
    pub is_deleted_by_sender: bool,
    pub is_deleted_by_receiver: bool,
}

pub trait IntoClientMessageExt {
    fn into_client(self, domain: impl Into<String>) -> ClientMessage;
}

impl IntoClientMessageExt for DbMessage {
    fn into_client(self, domain: impl Into<String>) -> ClientMessage {
        let domain: String = domain.into();
        let sender: String = get_user_url(&domain, self.sender as usize);
        let receiver: String = get_user_url(&domain, self.receiver as usize);

        ClientMessage {
            id: self.id as usize,
            sender,
            receiver,
            content: self.content,
            send_at: self.send_at,
            edited_at: self.edited_at,
            is_deleted_by_sender: self.is_deleted_by_sender != 0,
            is_deleted_by_receiver: self.is_deleted_by_receiver != 0,
        }
    }
}

// ------------------------------------------------------------------------- Posts
#[derive(Serialize, Deserialize, Debug, FromRow)]
pub struct DbPost {
    pub id: u64,
    pub author: u64,
    pub content: String,
    pub created_at: DateTime<Utc>,
    pub image_count: u64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DbPostLike {
    pub id: u64,
    pub post_id: u64,
    pub user_id: u64,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DbPostComment {
    pub id: u64,
    pub post_id: u64,
    pub author: u64,
    pub content: String,
    pub created_at: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ClientPostComment {
    pub id: usize,
    pub author: String,
    pub content: String,
    pub created_at: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ClientPost {
    pub id: usize,
    pub author: String,
    pub content: String,
    pub created_at: DateTime<Utc>,
    pub likes: Vec<String>,
    pub images: Vec<String>,
    pub comments: Vec<ClientPostComment>,
}

#[async_trait]
pub trait IntoClientPostExt {
    async fn into_client(self, dbclient: &DbClient, domain: impl Into<String> + Send)
        -> ClientPost;
}

#[async_trait]
impl IntoClientPostExt for DbPost {
    async fn into_client(
        self,
        dbclient: &DbClient,
        domain: impl Into<String> + Send,
    ) -> ClientPost {
        let domain: String = domain.into();
        let author: String = get_user_url(&domain, self.author as usize);

        let mut likes: Vec<String> = Vec::new();
        for post_like in dbclient.get_post_likes(self.id as usize).await {
            let user_url: String = get_user_url(&domain, post_like.user_id as usize);
            likes.push(user_url);
        }

        let mut images: Vec<String> = Vec::new();
        if self.image_count > 0 {
            for index in 1..=self.image_count {
                let post_pic_name: String = format!("{}_{}", self.id, index - 1);
                let url: String = format!("{}/posts/pics/{}", domain, post_pic_name);
                images.push(url);
            }
        }

        let mut comments: Vec<ClientPostComment> = Vec::new();
        for post_comment in dbclient.get_post_comments(self.id as usize).await {
            comments.push(post_comment.into_client(&domain));
        }

        ClientPost {
            id: self.id as usize,
            author,
            content: self.content,
            created_at: self.created_at,
            likes,
            images,
            comments,
        }
    }
}

pub trait IntoClientPostCommentExt {
    fn into_client(self, domain: impl Into<String>) -> ClientPostComment;
}

impl IntoClientPostCommentExt for DbPostComment {
    fn into_client(self, domain: impl Into<String>) -> ClientPostComment {
        let author: String = get_user_url(domain, self.author as usize);

        ClientPostComment {
            id: self.id as usize,
            author,
            content: self.content,
            created_at: self.created_at,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::config::Config;
    use crate::dbclient::{DbClient, FriendExt, FriendrequestExt, MessageExt, UserExt};
    use crate::utils::test_utils::{
        init_test_friendrequests, init_test_post_comments, init_test_post_likes, init_test_posts,
        init_test_users,
    };
    use crate::ws::server::WebSocketServer;
    use sqlx::mysql::MySqlPool;

    #[test]
    fn test_get_user_url() {
        let url: String = get_user_url("www.localhost", 69 as usize);
        let expected: &str = "www.localhost/users/69";
        assert_eq!(url, expected);
    }

    #[sqlx::test]
    async fn test_dbuser_into_clientuser(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient);

        let db_user = DbUser {
            id: 1,
            username: "marc".to_string(),
            email: "marc@mail.de".to_string(),
            password: "1234asdf".to_string(),
            is_active: 0,
            status: "off".to_string(),
            register_date: DateTime::<Utc>::default(),
        };

        let expected = ClientUser {
            id: 1,
            username: "marc".to_string(),
            register_date: DateTime::<Utc>::default(),
            status: "off".to_string(),
            avatar: "http://localhost:4000/users/avatar/1".to_string(),
        };

        assert_eq!(
            serde_json::to_string(
                &db_user
                    .into_client(&config.domain, websocket_server_handle.clone())
                    .await
            )
            .unwrap(),
            serde_json::to_string(&expected).unwrap()
        );
    }

    #[test]
    fn test_dbfriend_get_user_id_if_match() {
        let friend_item = DbFriend {
            id: 1,
            user_one: 1,
            user_two: 2,
        };

        assert_eq!(friend_item.get_user_id_if_match(1 as usize).unwrap(), 1);
    }

    #[test]
    fn test_dbfriend_get_user_id_if_match_failed() {
        let friend_item = DbFriend {
            id: 1,
            user_one: 1,
            user_two: 2,
        };

        assert!(friend_item.get_user_id_if_match(69 as usize).is_none());
    }

    #[test]
    fn test_dbfriend_get_user_id_of_friend() {
        let friend_item = DbFriend {
            id: 1,
            user_one: 1,
            user_two: 2,
        };

        assert_eq!(friend_item.get_user_id_of_friend(1 as usize).unwrap(), 2);
    }

    #[test]
    fn test_dbfriend_get_user_id_of_friend_failed() {
        let friend_item = DbFriend {
            id: 1,
            user_one: 1,
            user_two: 2,
        };

        assert!(friend_item.get_user_id_of_friend(69 as usize).is_none());
    }

    #[sqlx::test]
    async fn test_dbfriendrequest_into_clientfriendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.get_user(1).await.unwrap();
        dbclient.get_user(2).await.unwrap();

        let expected = ClientFriendrequest {
            id: 1,
            from_user: "http://localhost:4000/users/1".to_string(),
            to_user: "http://localhost:4000/users/2".to_string(),
            accept: "http://localhost:4000/friendrequests/accept/1".to_string(),
            decline: "http://localhost:4000/friendrequests/decline/1".to_string(),
        };

        assert_eq!(
            serde_json::to_string(
                &dbclient
                    .get_friendrequest_by_id(1)
                    .await
                    .unwrap()
                    .into_client("http://localhost:4000")
            )
            .unwrap(),
            serde_json::to_string(&expected).unwrap()
        );
    }

    #[sqlx::test]
    async fn test_dbmessage_into_clientmessage(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_message(1, 2, "hiii").await.unwrap();
        let from_user: String = "http://localhost:4000/users/1".to_string();
        let to_user: String = "http://localhost:4000/users/2".to_string();

        let message: ClientMessage = dbclient
            .get_message(1)
            .await
            .unwrap()
            .into_client("http://localhost:4000");

        assert_eq!(message.id, 1);
        assert_eq!(message.sender, from_user);
        assert_eq!(message.receiver, to_user);
        assert_eq!(message.content, "hiii");
        assert_eq!(message.is_deleted_by_sender, false);
        assert_eq!(message.is_deleted_by_sender, false);
    }

    #[sqlx::test]
    async fn test_dbpost_into_clientpost(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_posts(&pool).await;
        init_test_post_comments(&pool).await;
        init_test_post_likes(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let post: ClientPost = dbclient
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient, &config.upload_dir)
            .await;
        assert_eq!(post.likes.len(), 2);
        assert_eq!(post.comments.len(), 3);
        assert_eq!(post.images.len(), 0);
    }

    #[sqlx::test]
    async fn test_dbpostcomment_into_clientpostcomment(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        dbclient.save_post_comment(1, 1, "haha").await.unwrap();
        let comment: ClientPostComment = dbclient.get_post_comments(1).await[0]
            .clone()
            .into_client(&config.domain);
        assert_eq!(comment.id, 1);
        assert_eq!(comment.author, "http://localhost:4000/users/1");
        assert_eq!(comment.content, "haha");
    }
}
