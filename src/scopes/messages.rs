use crate::dbclient::MessageExt;
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{ClientMessage, DbMessage, IntoClientMessageExt};
use crate::success::SuccessMessage;
use crate::utils::paginate::Paginator;
use crate::AppState;
use actix_web::{web, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use validator::Validate;

/// Scope for the "/messages" route
pub fn messages_scope() -> Scope {
    web::scope("/messages")
        .route("/", web::get().to(get_messages))
        .route("/", web::post().to(send_message))
        .route("/{message_id}", web::get().to(get_message))
        .route("/{message_id}", web::patch().to(update_message))
        .route("/{message_id}", web::delete().to(delete_message))
        .route("/unsend/{message_id}", web::delete().to(unsend_message))
        .route("/chat/{user_id}", web::get().to(get_chat))
        .route("/chat/{user_id}", web::delete().to(delete_chat))
}

#[derive(Serialize, Deserialize, Validate)]
struct GetMessagesQuery {
    #[validate(range(min = 1))]
    page: Option<usize>,
    #[validate(range(min = 1, max = 50))]
    page_size: Option<usize>,
}

async fn get_messages(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    query: web::Query<GetMessagesQuery>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let query: GetMessagesQuery = query.into_inner();

    query
        .validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let messages: Vec<ClientMessage> = app_state
        .dbclient
        .get_messages(user_id)
        .await
        .into_iter()
        .map(|m: DbMessage| m.into_client(&app_state.config.domain))
        .collect();

    Ok(HttpResponse::Ok().json(
        Paginator::new()
            .page(query.page)
            .page_size(query.page_size)
            .url(&app_state.config.domain)
            .pathname("/messages")
            .data(messages)
            .into_paginated_response(),
    ))
}

#[derive(Serialize, Deserialize)]
struct SendMessageBody {
    to_user: usize,
    content: String,
}

#[derive(Serialize, Deserialize)]
struct SendMessageResponse {
    message: String,
    result: ClientMessage,
}

async fn send_message(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    body: web::Json<SendMessageBody>,
) -> Result<HttpResponse, HttpError> {
    let body: SendMessageBody = body.into_inner();
    let user_id: usize = auth_token.id;

    let message: ClientMessage = app_state
        .dbclient
        .save_message(user_id, body.to_user, body.content)
        .await?
        .into_client(&app_state.config.domain);

    Ok(HttpResponse::Ok().json(SendMessageResponse {
        message: SuccessMessage::SentMessage.into(),
        result: message,
    }))
}

#[derive(Serialize, Deserialize)]
struct GetMessageResponse {
    message: String,
    result: ClientMessage,
}

async fn get_message(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let message_id: usize = path.into_inner();
    let message: ClientMessage = app_state
        .dbclient
        .get_message(message_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoMessageFound))
        .and_then(|message: DbMessage| {
            let is_allowed_to_see: bool =
                user_id == message.sender as usize || user_id == message.receiver as usize;
            if !is_allowed_to_see {
                return Err(HttpError::bad_request(ErrorMessage::NotYourMessage));
            }
            Ok(message)
        })?
        .into_client(&app_state.config.domain);

    Ok(HttpResponse::Ok().json(GetMessageResponse {
        message: SuccessMessage::GotMessage.into(),
        result: message,
    }))
}

#[derive(Serialize, Deserialize)]
struct UpdateMessageBody {
    new_content: String,
}

#[derive(Serialize, Deserialize)]
struct UpdateMessageResponse {
    message: String,
    result: ClientMessage,
}

async fn update_message(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
    body: web::Json<UpdateMessageBody>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let message_id: usize = path.into_inner();
    let body: UpdateMessageBody = body.into_inner();
    // check if its legal to update/edit the message
    app_state
        .dbclient
        .get_message(message_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoMessageFound))
        .and_then(|message: DbMessage| {
            let is_allowed_to_udpate: bool =
                user_id == message.sender as usize || user_id == message.receiver as usize;
            if !is_allowed_to_udpate {
                return Err(HttpError::bad_request(ErrorMessage::NotYourMessage));
            }
            Ok(())
        })?;
    // update message
    app_state
        .dbclient
        .update_message(message_id, body.new_content)
        .await?;

    let updated_message: ClientMessage = app_state
        .dbclient
        .get_message(message_id)
        .await
        .ok_or(HttpError::server_error())?
        .into_client(&app_state.config.domain);

    Ok(HttpResponse::Ok().json(UpdateMessageResponse {
        message: SuccessMessage::UpdatedMessage.into(),
        result: updated_message,
    }))
}

#[derive(Serialize, Deserialize)]
struct DeleteMessageResponse {
    message: String,
}

async fn delete_message(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let message_id: usize = path.into_inner();
    // check if its legal to delete
    let message: DbMessage = app_state
        .dbclient
        .get_message(message_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoMessageFound))?;

    let is_sender: bool = user_id == message.sender as usize;
    let is_receiver: bool = user_id == message.receiver as usize;

    if is_sender {
        app_state
            .dbclient
            .delete_message_for_sender(message_id)
            .await?;
    } else if is_receiver {
        app_state
            .dbclient
            .delete_message_for_receiver(message_id)
            .await?;
    } else {
        return Err(HttpError::bad_request(ErrorMessage::NotYourMessage));
    }

    Ok(HttpResponse::Ok().json(DeleteMessageResponse {
        message: SuccessMessage::DeletedMessage.into(),
    }))
}

#[derive(Serialize, Deserialize)]
struct UnsendMessageResponse {
    message: String,
}

async fn unsend_message(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let message_id: usize = path.into_inner();

    // check if its your message
    app_state
        .dbclient
        .get_message(message_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoMessageFound))
        .and_then(|msg: DbMessage| {
            if msg.sender as usize == user_id {
                return Ok(());
            } else {
                return Err(HttpError::bad_request(ErrorMessage::CannotDeleteMessage));
            }
        })?;

    //  remove message
    app_state.dbclient.delete_message(message_id).await?;

    Ok(HttpResponse::Ok().json(DeleteMessageResponse {
        message: SuccessMessage::UnsentMessage.into(),
    }))
}

#[derive(Serialize, Deserialize, Validate)]
struct GetChatQuery {
    #[validate(range(min = 1))]
    page: Option<usize>,
    #[validate(range(min = 1, max = 50))]
    page_size: Option<usize>,
}

async fn get_chat(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
    query: web::Query<GetChatQuery>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let user_id_of_friend: usize = path.into_inner();
    let query: GetChatQuery = query.into_inner();

    query
        .validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let messages: Vec<ClientMessage> = app_state
        .dbclient
        .get_chat(user_id, user_id_of_friend)
        .await
        .into_iter()
        .map(|m: DbMessage| m.into_client(&app_state.config.domain))
        .collect();

    Ok(HttpResponse::Ok().json(
        Paginator::new()
            .page(query.page)
            .page_size(query.page_size)
            .pathname("/messages/chat")
            .data(messages)
            .into_paginated_response(),
    ))
}

#[derive(Serialize, Deserialize)]
struct DeleteChatResponse {
    message: String,
}

async fn delete_chat(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let user_id_of_friend: usize = path.into_inner();

    app_state
        .dbclient
        .delete_chat(user_id, user_id_of_friend)
        .await?;

    Ok(HttpResponse::Ok().json(DeleteChatResponse {
        message: SuccessMessage::DeletedChat.into(),
    }))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::dbclient::{DbClient, FriendExt, MessageExt, UserExt};
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils::{
        get_test_token, init_test_friends, init_test_messages, init_test_users,
    };
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, AppState};
    use actix_web::{body::MessageBody, http, test, web, App};
    use sqlx::mysql::MySqlPool;
    use tokio::time::{sleep, Duration};

    #[sqlx::test]
    async fn test_get_messages(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_messages)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_messages_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let message: ClientMessage = dbclient
            .save_message(1, 2, "Hi")
            .await
            .unwrap()
            .into_client(&config.domain);

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![message],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_messages)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_messages_next_url(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_message(1, 2, "hi")
            .await
            .unwrap()
            .into_client(&config.domain);

        sleep(Duration::from_secs(1)).await;

        let message: ClientMessage = dbclient
            .save_message(1, 2, "hey")
            .await
            .unwrap()
            .into_client(&config.domain);

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: Some("http://localhost:4000/messages/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![message],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_messages)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page=1&page_size=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_messages_previous_url(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let message: ClientMessage = dbclient
            .save_message(1, 2, "hi")
            .await
            .unwrap()
            .into_client(&config.domain);

        sleep(Duration::from_secs(1)).await;

        dbclient
            .save_message(1, 2, "hey")
            .await
            .unwrap()
            .into_client(&config.domain);

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some("http://localhost:4000/messages/?page=1&page_size=1".to_string()),
            result: vec![message],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_messages)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page=2&page_size=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_messages_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();
        let first: ClientMessage = dbclient
            .save_message(1, 2, "Hi")
            .await
            .unwrap()
            .into_client(&config.domain);
        let second: ClientMessage = dbclient
            .save_message(1, 2, "Hi")
            .await
            .unwrap()
            .into_client(&config.domain);

        let messages: Vec<ClientMessage> = vec![second, first];

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: messages,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_messages)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_messages_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_messages)),
        )
        .await;

        let req = test::TestRequest::default().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_send_message(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(SendMessageBody {
                to_user: 2,
                content: "Hi".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_send_message_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(SendMessageBody {
                to_user: 2,
                content: "Hi".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        let expected: String = serde_json::to_string(&SendMessageResponse {
            message: SuccessMessage::SentMessage.into(),
            result: dbclient
                .get_message(1)
                .await
                .unwrap()
                .into_client(&config.domain),
        })
        .unwrap();

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_send_message_to_user_who_isnt_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(SendMessageBody {
                to_user: 999,
                content: "Hi".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_send_message_to_yourself(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(SendMessageBody {
                to_user: 1,
                content: "Hi".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_send_message_with_no_content(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(SendMessageBody {
                to_user: 2,
                content: "".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_send_message_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_message)),
        )
        .await;

        let req = test::TestRequest::post().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_get_message(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::get().to(get_message)),
        )
        .await;

        let req = test::TestRequest::get()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_message_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&GetMessageResponse {
            message: SuccessMessage::GotMessage.into(),
            result: dbclient
                .get_message(1)
                .await
                .unwrap()
                .into_client(&config.domain),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::get().to(get_message)),
        )
        .await;

        let req = test::TestRequest::get()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_message_that_isnt_mine(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::get().to(get_message)),
        )
        .await;

        let req = test::TestRequest::get()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_message_that_doesnt_exist(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::get().to(get_message)),
        )
        .await;

        let req = test::TestRequest::get()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_message_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::get().to(get_message)),
        )
        .await;

        let req = test::TestRequest::get().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_update_message(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                new_content: "new content".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_message_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                new_content: "new content".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        let expected: String = serde_json::to_string(&UpdateMessageResponse {
            message: SuccessMessage::UpdatedMessage.to_string(),
            result: dbclient
                .get_message(1)
                .await
                .unwrap()
                .into_client(&config.domain),
        })
        .unwrap();

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_update_message_with_no_content(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                new_content: "".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_message_that_doesnt_exist(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/999")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                new_content: "".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_message_that_isnt_mine(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                new_content: "".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_message_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_update_message_updates_content(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_message(1, 2, "old content").await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                new_content: "new content".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let updated_message: DbMessage = dbclient.get_message(1).await.unwrap();
        assert_eq!(updated_message.content, "new content");
    }

    #[sqlx::test]
    async fn test_update_message_updates_edited_at(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let old_message: DbMessage = dbclient.save_message(1, 2, "old content").await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::patch().to(update_message)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(UpdateMessageBody {
                new_content: "new content".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let updated_message: DbMessage = dbclient.get_message(1).await.unwrap();
        assert_ne!(old_message.edited_at, updated_message.edited_at);
    }

    #[sqlx::test]
    async fn test_delete_message(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_delete_message_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&DeleteMessageResponse {
            message: SuccessMessage::DeletedMessage.to_string(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_delete_message_that_doesnt_exist(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_message_that_isnt_mine(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_message_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_message_deleted_the_message_for_sender(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_message(1, 2, "hii").await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let message: DbMessage = dbclient.get_message(1).await.unwrap();
        assert_eq!(message.is_deleted_by_sender, 1);
    }

    #[sqlx::test]
    async fn test_delete_message_deleted_the_message_for_receiver(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_message(1, 2, "hii").await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(delete_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let message: DbMessage = dbclient.get_message(1).await.unwrap();
        assert_eq!(message.is_deleted_by_receiver, 1);
    }

    #[sqlx::test]
    async fn test_unsend_message(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(unsend_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_unsend_message_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&DeleteMessageResponse {
            message: SuccessMessage::UnsentMessage.to_string(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(unsend_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_unsend_message_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(unsend_message)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_unsend_message_is_that_doesnt_exist(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(unsend_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_unsend_message_is_that_isnt_mine(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(unsend_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_unsend_message_that_i_sent_to_myself(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_message(1, 1, "this is a message just for myself")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/{message_id}", web::delete().to(unsend_message)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let is_deleted: bool = dbclient.get_message(1).await.is_none();
        assert!(is_deleted);
    }

    #[sqlx::test]
    async fn test_get_chat(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_chat)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_chat_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let message: ClientMessage = dbclient
            .save_message(1, 2, "Hi")
            .await
            .unwrap()
            .into_client(&config.domain);

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![message],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_chat)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_next_url(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_message(1, 2, "hi")
            .await
            .unwrap()
            .into_client(&config.domain);

        sleep(Duration::from_secs(1)).await;

        let message: ClientMessage = dbclient
            .save_message(1, 2, "hey")
            .await
            .unwrap()
            .into_client(&config.domain);

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: Some("http://localhost:4000/messages/chat/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![message],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_chat)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2?page=1&page_size=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_previous_url(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let message: ClientMessage = dbclient
            .save_message(1, 2, "hi")
            .await
            .unwrap()
            .into_client(&config.domain);

        sleep(Duration::from_secs(1)).await;

        dbclient
            .save_message(1, 2, "hey")
            .await
            .unwrap()
            .into_client(&config.domain);

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some("http://localhost:4000/messages/chat/?page=1&page_size=1".to_string()),
            result: vec![message],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_chat)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2?page=2&page_size=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();
        let first: ClientMessage = dbclient
            .save_message(1, 2, "Hi")
            .await
            .unwrap()
            .into_client(&config.domain);
        let second: ClientMessage = dbclient
            .save_message(1, 2, "Hi")
            .await
            .unwrap()
            .into_client(&config.domain);

        let messages: Vec<ClientMessage> = vec![second, first];

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: messages,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_chat)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_chat)),
        )
        .await;

        let req = test::TestRequest::default().uri("/2").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_chat(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(delete_chat)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_delete_chat_deleted_chat(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(delete_chat)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let deleted_chat: Vec<DbMessage> = dbclient_clone.get_chat(1, 2).await;
        for message in deleted_chat {
            let is_soft_deleted: bool =
                message.is_deleted_by_sender == 1 || message.is_deleted_by_receiver == 1;
            assert!(is_soft_deleted);
        }
    }

    #[sqlx::test]
    async fn test_delete_chat_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&DeleteChatResponse {
            message: SuccessMessage::DeletedChat.into(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(delete_chat)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_delete_chat_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(delete_chat)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/2").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }
}
