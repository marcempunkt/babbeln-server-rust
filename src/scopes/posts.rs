use crate::dbclient::PostExt;
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{ClientPost, DbPost, DbPostComment, IntoClientPostExt};
use crate::success::SuccessMessage;
use crate::utils::paginate::Paginator;
use crate::utils::posts_utils;
use crate::utils::upload::{IntoPayloadExt, PostPayload, Uploader, WriteToDiskExt};
use crate::AppState;
use actix_multipart::Multipart;
use actix_web::{web, HttpRequest, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use std::path::Path;
use tokio::fs;
use validator::Validate;

pub fn posts_scope() -> Scope {
    web::scope("/posts")
        .route("/", web::get().to(get_posts))
        .route("/", web::post().to(save_post))
        .route("/{post_id}", web::get().to(get_post))
        .route("/{post_id}", web::delete().to(delete_post))
        .route("/like/{post_id}", web::patch().to(like_post))
        .route("/unlike/{post_id}", web::patch().to(unlike_post))
        .route("/comments/{post_id}", web::post().to(post_comment))
        .route("/comments/{comment_id}", web::delete().to(delete_comment))
        .route("/pics/{post_pic_name}", web::get().to(get_post_pic))
}

#[derive(Serialize, Deserialize, Validate)]
struct GetPostsQuery {
    #[validate(range(min = 1))]
    page: Option<usize>,
    #[validate(range(min = 1, max = 50))]
    page_size: Option<usize>,
}

async fn get_posts(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    query: web::Query<GetPostsQuery>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let query: GetPostsQuery = query.into_inner();

    query
        .validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let posts: Vec<DbPost> = app_state.dbclient.get_posts(user_id).await;
    let mut client_posts: Vec<ClientPost> = Vec::new();

    for post in posts {
        let post = post
            .into_client(&app_state.dbclient, &app_state.config.domain)
            .await;
        client_posts.push(post);
    }

    Ok(HttpResponse::Ok().json(
        Paginator::new()
            .page(query.page)
            .page_size(query.page_size)
            .url(&app_state.config.domain)
            .pathname("/posts")
            .data(client_posts)
            .into_paginated_response(),
    ))
}

#[derive(Serialize, Deserialize)]
struct SavePostResponse {
    message: String,
    result: ClientPost,
}

async fn save_post(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    req: HttpRequest,
    payload: Multipart,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;

    let post_payload = Uploader::new()
        .required(false)
        .max_size_in_mb(5)
        .max_count(5)
        .into_post_payload(payload, &req)
        .await
        .and_then(|p: PostPayload| {
            if p.files.len() == 0 && p.content.is_empty() {
                return Err(HttpError::bad_request(ErrorMessage::EmptyPost));
            }
            Ok(p)
        })?;

    let post: ClientPost = app_state
        .dbclient
        .save_post(user_id, &post_payload.content, post_payload.files.len())
        .await?
        .into_client(&app_state.dbclient, &app_state.config.domain)
        .await;

    if let Err(e) = post_payload
        .write_to_disk(post.id, &app_state.config.upload_dir, None)
        .await
    {
        app_state.dbclient.delete_post(post.id).await?;
        return Err(e);
    }

    Ok(HttpResponse::Ok().json(SavePostResponse {
        message: SuccessMessage::SavedPost.into(),
        result: post,
    }))
}

#[derive(Serialize, Deserialize)]
struct GetPostResponse {
    message: String,
    result: ClientPost,
}

async fn get_post(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let post_id: usize = path.into_inner();
    let post: ClientPost = app_state
        .dbclient
        .get_post(post_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))?
        .into_client(&app_state.dbclient, &app_state.config.domain)
        .await;

    if !posts_utils::is_my_or_friends_post(user_id, post_id, &app_state.dbclient).await {
        return Err(HttpError::bad_request(ErrorMessage::CannotViewThisPost));
    }

    Ok(HttpResponse::Ok().json(GetPostResponse {
        message: SuccessMessage::GotPost.into(),
        result: post,
    }))
}

#[derive(Serialize, Deserialize)]
struct DeletePostResponse {
    message: String,
}

async fn delete_post(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let post_id: usize = path.into_inner();

    if !posts_utils::is_my_post(user_id, post_id, &app_state.dbclient).await {
        return Err(HttpError::bad_request(ErrorMessage::NotYourPost));
    }

    let post: DbPost = app_state
        .dbclient
        .get_post(post_id)
        .await
        .ok_or(HttpError::server_error())?;

    app_state.dbclient.delete_post(post_id).await?;

    if post.image_count > 0 {
        web::block(move || async move {
            for i in 1..=post.image_count {
                fs::remove_file(format!(
                    "{}/posts/{}_{}.jpeg",
                    app_state.config.upload_dir,
                    post_id,
                    i - 1,
                ))
                .await
                .unwrap();
            }
        })
        .await
        .map_err(|_| HttpError::server_error())?
        .await;
    }

    Ok(HttpResponse::Ok().json(DeletePostResponse {
        message: SuccessMessage::DeletedPost.into(),
    }))
}

#[derive(Serialize, Deserialize)]
struct LikePostResponse {
    message: String,
    result: ClientPost,
}

async fn like_post(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let post_id: usize = path.into_inner();

    if !posts_utils::is_my_or_friends_post(user_id, post_id, &app_state.dbclient).await {
        return Err(HttpError::bad_request(ErrorMessage::CannotLikeThisPost));
    }

    app_state.dbclient.like_post(post_id, user_id).await?;

    let post: ClientPost = app_state
        .dbclient
        .get_post(post_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))?
        .into_client(&app_state.dbclient, &app_state.config.domain)
        .await;

    Ok(HttpResponse::Ok().json(LikePostResponse {
        message: SuccessMessage::LikedPost.into(),
        result: post,
    }))
}

#[derive(Serialize, Deserialize)]
struct UnlikePostResponse {
    message: String,
    result: ClientPost,
}

async fn unlike_post(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let post_id: usize = path.into_inner();

    if !posts_utils::is_my_or_friends_post(user_id, post_id, &app_state.dbclient).await {
        return Err(HttpError::bad_request(ErrorMessage::CannotUnlikeThisPost));
    }

    app_state.dbclient.unlike_post(post_id, user_id).await?;

    let post: ClientPost = app_state
        .dbclient
        .get_post(post_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))?
        .into_client(&app_state.dbclient, &app_state.config.domain)
        .await;

    Ok(HttpResponse::Ok().json(UnlikePostResponse {
        message: SuccessMessage::UnlikedPost.into(),
        result: post,
    }))
}

#[derive(Serialize, Deserialize)]
struct PostCommentBody {
    content: String,
}

#[derive(Serialize, Deserialize)]
struct PostCommentResponse {
    message: String,
    result: ClientPost,
}

async fn post_comment(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
    body: web::Json<PostCommentBody>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let post_id: usize = path.into_inner();
    let body: PostCommentBody = body.into_inner();

    /* Check if user is allowed to post comment */
    if !posts_utils::is_my_or_friends_post(user_id, post_id, &app_state.dbclient).await {
        return Err(HttpError::bad_request(
            ErrorMessage::CannotCommentOnThisPost,
        ));
    }

    app_state
        .dbclient
        .save_post_comment(post_id, user_id, body.content)
        .await?;

    let post: ClientPost = app_state
        .dbclient
        .get_post(post_id)
        .await
        .ok_or(HttpError::server_error())?
        .into_client(&app_state.dbclient, &app_state.config.domain)
        .await;

    Ok(HttpResponse::Ok().json(PostCommentResponse {
        message: SuccessMessage::CommentPost.into(),
        result: post,
    }))
}

#[derive(Serialize, Deserialize)]
struct DeleteCommentResponse {
    message: String,
    result: ClientPost,
}

async fn delete_comment(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let comment_id: usize = path.into_inner();
    let post_id: usize = app_state
        .dbclient
        .get_post_comment(comment_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoPostCommentFound))
        .and_then(
            |comment: DbPostComment| match comment.author as usize == user_id {
                true => Ok(comment.post_id as usize),
                false => Err(HttpError::bad_request(
                    ErrorMessage::CannotDeletePostComment,
                )),
            },
        )?;

    app_state.dbclient.delete_post_comment(comment_id).await?;

    let post: ClientPost = app_state
        .dbclient
        .get_post(post_id)
        .await
        .ok_or(HttpError::server_error())?
        .into_client(&app_state.dbclient, &app_state.config.domain)
        .await;

    Ok(HttpResponse::Ok().json(DeleteCommentResponse {
        message: SuccessMessage::DeletedPostComment.into(),
        result: post,
    }))
}

async fn get_post_pic(
    app_state: web::Data<AppState>,
    path: web::Path<String>,
    auth_token: AuthenticationToken,
) -> Result<HttpResponse, HttpError> {
    let post_pic_name: String = path.into_inner();
    let user_id: usize = auth_token.id;
    let post_id: usize = post_pic_name
        .split("_")
        .collect::<Vec<&str>>()
        .get(0)
        .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))
        .and_then(|s: &&str| {
            let s: String = s.to_string();
            match s.parse::<usize>() {
                Ok(post_id) => Ok(post_id),
                Err(_) => Err(HttpError::bad_request(ErrorMessage::NoPostFound)),
            }
        })?;

    let post_pic_path = Path::new(&app_state.config.upload_dir)
        .join("posts")
        .join(format!("{}.jpeg", post_pic_name));

    if !posts_utils::is_my_or_friends_post(user_id, post_id, &app_state.dbclient).await {
        return Err(HttpError::bad_request(ErrorMessage::CannotViewThisPost));
    }

    let pic: Vec<u8> = fs::read(post_pic_path)
        .await
        .map_err(|_| HttpError::bad_request(ErrorMessage::NoPostPicFound))?;

    Ok(HttpResponse::Ok().content_type("image/jpeg").body(pic))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::models::{ClientPostComment, DbPostComment, DbPostLike, IntoClientPostExt};
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils::{
        get_test_token, init_test_friends, init_test_post_comments, init_test_post_likes,
        init_test_posts, init_test_users,
    };
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, dbclient::DbClient, AppState};
    use actix_web::web::Bytes;
    use actix_web::{body::MessageBody, http, test, web, App};
    use serde_json;
    use sqlx::mysql::MySqlPool;

    #[sqlx::test]
    async fn test_get_posts(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_posts)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_posts_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "content", 0).await.unwrap();
        let post: DbPost = dbclient.get_post(1).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![post.into_client(&dbclient, &config.domain).await],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_posts)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_posts_next_url(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());
        dbclient.save_post(1, "content", 0).await.unwrap();
        dbclient.save_post(1, "content", 0).await.unwrap();
        let post: DbPost = dbclient.get_post(2).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: Some("http://localhost:4000/posts/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![post.into_client(&dbclient, &config.domain).await],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_posts)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?&page_size=1&page=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_posts_previous_url(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "content", 0).await.unwrap();
        dbclient.save_post(1, "content", 0).await.unwrap();
        let post: DbPost = dbclient.get_post(1).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some("http://localhost:4000/posts/?page=1&page_size=1".to_string()),
            result: vec![post.into_client(&dbclient, &config.domain).await],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_posts)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?&page_size=1&page=2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_posts_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let first: ClientPost = dbclient
            .save_post(1, "content", 0)
            .await
            .unwrap()
            .into_client(&dbclient, &config.domain)
            .await;
        let second: ClientPost = dbclient
            .save_post(1, "content", 0)
            .await
            .unwrap()
            .into_client(&dbclient, &config.domain)
            .await;
        let posts: Vec<ClientPost> = vec![second, first];

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: posts,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_posts)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_posts_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_posts)),
        )
        .await;

        let req = test::TestRequest::default().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_save_post(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let payload: Bytes = Bytes::from(
            "testasdadsad\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
             Content-Disposition: form-data; name=\"content\"\r\n\
             Content-Type: text/plain; charset=utf-8\r\nContent-Length: 15\r\n\r\n\
             This is a post!\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n",
        );

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();

        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post = dbclient_clone.get_post(1).await.unwrap();
        assert_eq!(post.content, "This is a post!");
    }

    #[sqlx::test]
    async fn test_save_post_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let payload: Bytes = Bytes::from(
            "testasdadsad\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
             Content-Disposition: form-data; name=\"content\"\r\n\
             Content-Type: text/plain; charset=utf-8\r\nContent-Length: 15\r\n\r\n\
             This is a post!\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n",
        );

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();

        let resp = test::call_service(&app, req).await;

        let post: ClientPost = dbclient_clone
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient_clone, &config.domain)
            .await;
        let expected: String = serde_json::to_string(&SavePostResponse {
            message: SuccessMessage::SavedPost.into(),
            result: post,
        })
        .unwrap();

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_save_post_with_nothing(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header(http::header::ContentType(mime::MULTIPART_FORM_DATA))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();

        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_save_post_with_too_many_characters(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut content: String = String::new();

        for _ in 0..=255 {
            content.push_str("w");
        }

        let payload: Bytes = Bytes::from(format!(
            "testasdadsad\r\n\
                 --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
                 Content-Disposition: form-data; name=\"content\"\r\n\
                 Content-Type: text/plain; charset=utf-8\r\nContent-Length: 256\r\n\r\n\
                 {}\r\n\
                 --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n",
            content,
        ));

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();

        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_save_post_with_text_and_images(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());
        let upload_dir: String = config.upload_dir.clone();

        let mut payload: Vec<u8> = Vec::new();

        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"content\"\r\n\
          Content-Type: text/plain; charset=utf-8\r\nContent-Length: 15\r\n\r\n\
          This is a post!\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"image\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          \x89PNG\r\n\x1a\n\0\0\0\rIHDR\0\0\0\x01\0\0\0\x01\x08\x02\0\0\0\x90wS\xde\0\0\0\x0fIDAT\x08\x1d\x01\x04\0\xfb\xff\0\0\0\0\0\x04\0\x01/R\xb4\x8d\0\0\0\0IEND\xaeB`\x82\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n".into_iter().for_each(|b: &u8| payload.push(*b));

        let payload: Bytes = Bytes::from(payload);

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();

        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post = dbclient_clone.get_post(1).await.unwrap();
        assert_eq!(post.content, "This is a post!");
        assert_eq!(post.image_count, 1);
        assert_eq!(
            Path::new(&format!("{}/posts/1_0.jpeg", upload_dir)).exists(),
            true
        );
    }

    #[sqlx::test]
    async fn test_save_post_with_only_images(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());
        let upload_dir: String = config.upload_dir.clone();

        let mut payload: Vec<u8> = Vec::new();
        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"image\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          \x89PNG\r\n\x1a\n\0\0\0\rIHDR\0\0\0\x01\0\0\0\x01\x08\x02\0\0\0\x90wS\xde\0\0\0\x0fIDAT\x08\x1d\x01\x04\0\xfb\xff\0\0\0\0\0\x04\0\x01/R\xb4\x8d\0\0\0\0IEND\xaeB`\x82\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n".into_iter().for_each(|b: &u8| payload.push(*b));

        let payload: Bytes = Bytes::from(payload);

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();
        let resp = test::call_service(&app, req).await;
        let post = dbclient_clone.get_post(1).await.unwrap();

        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(post.content, "");
        assert_eq!(post.image_count, 1);
        assert_eq!(
            Path::new(&format!("{}/posts/1_0.jpeg", upload_dir)).exists(),
            true
        );
    }

    #[sqlx::test]
    async fn test_save_post_with_garbage_as_image(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());
        let config = Config::new_test();
        let upload_dir: String = config.upload_dir.clone();

        let mut payload: Vec<u8> = Vec::new();

        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"image\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          some_hacked_garbage\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n"
            .into_iter()
            .for_each(|b: &u8| payload.push(*b));

        let payload: Bytes = Bytes::from(payload);

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;
        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();

        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);

        let post: Option<DbPost> = dbclient_clone.get_post(1).await;
        assert_eq!(post.is_none(), true);

        let is_posts_dir_empty: bool = std::fs::read_dir(Path::new(&upload_dir).join("posts"))
            .unwrap()
            .next()
            .is_none();
        assert!(is_posts_dir_empty);
    }

    #[sqlx::test]
    async fn test_save_post_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header(http::header::ContentType(mime::MULTIPART_FORM_DATA))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_get_post(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::get().to(get_post)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_post_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let post: ClientPost = dbclient
            .save_post(1, "hii", 0)
            .await
            .unwrap()
            .into_client(&dbclient, &config.domain)
            .await;

        let expected: String = serde_json::to_string(&GetPostResponse {
            message: SuccessMessage::GotPost.into(),
            result: post,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::get().to(get_post)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_post_which_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::get().to(get_post)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_post_which_im_not_allowed_to_view(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(2, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::get().to(get_post)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_post_which_belongs_to_a_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(2, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::get().to(get_post)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_post_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::get().to(get_post)),
        )
        .await;

        let req = test::TestRequest::default().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_post(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_delete_post_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(
            resp.into_body().try_into_bytes().unwrap(),
            serde_json::to_string(&DeletePostResponse {
                message: SuccessMessage::DeletedPost.into(),
            })
            .unwrap(),
        );
    }

    #[sqlx::test]
    async fn test_delete_post_deletes_all_likes(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_posts(&pool).await;
        init_test_post_likes(&pool).await;
        let dbclient = DbClient::new(pool.clone());
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let likes: Vec<DbPostLike> =
            sqlx::query_as!(DbPostLike, "SELECT * FROM post_likes WHERE post_id = ?", 1,)
                .fetch_all(&pool)
                .await
                .unwrap();

        assert_eq!(likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_post_deletes_all_comments(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_posts(&pool).await;
        init_test_post_comments(&pool).await;
        let dbclient = DbClient::new(pool.clone());
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let comments: Vec<DbPostComment> = sqlx::query_as!(
            DbPostComment,
            "SELECT * FROM post_comments WHERE post_id = ?",
            1,
        )
        .fetch_all(&pool)
        .await
        .unwrap();

        assert_eq!(comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_post_deletes_all_images(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool.clone());
        let config = Config::new_test();
        let upload_dir: String = config.upload_dir.clone();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut payload: Vec<u8> = Vec::new();
        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"image\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          \x89PNG\r\n\x1a\n\0\0\0\rIHDR\0\0\0\x01\0\0\0\x01\x08\x02\0\0\0\x90wS\xde\0\0\0\x0fIDAT\x08\x1d\x01\x04\0\xfb\xff\0\0\0\0\0\x04\0\x01/R\xb4\x8d\0\0\0\0IEND\xaeB`\x82\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n".into_iter().for_each(|b: &u8| payload.push(*b));

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req_save_post = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();
        let req_delete_post = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();

        let resp_save_post = test::call_service(&app, req_save_post).await;
        let resp_delete_post = test::call_service(&app, req_delete_post).await;

        assert_eq!(resp_save_post.status(), http::StatusCode::OK);
        assert_eq!(resp_delete_post.status(), http::StatusCode::OK);
        assert_eq!(
            Path::new(&format!("{}/posts/1_0.jpeg", upload_dir)).exists(),
            false
        );
    }

    #[sqlx::test]
    async fn test_delete_post_which_isnt_mine(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool.clone());
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(2, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_post_which_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool.clone());
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_post_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::delete().to(delete_post)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_like_post(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(like_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post: ClientPost = dbclient_clone
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient_clone, "")
            .await;
        assert_eq!(post.likes.len(), 1);
    }

    #[sqlx::test]
    async fn test_like_post_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let config_clone = config.clone();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(like_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post: ClientPost = dbclient_clone
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient_clone, &config_clone.domain)
            .await;

        let expected: String = serde_json::to_string(&LikePostResponse {
            message: SuccessMessage::LikedPost.into(),
            result: post,
        })
        .unwrap();

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_like_post_which_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(like_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_like_post_twice(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();
        dbclient.like_post(1, 1).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(like_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_like_post_of_whom_isnt_your_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(2, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(like_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_like_post_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(like_post)),
        )
        .await;

        let req = test::TestRequest::patch().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_unlike_post(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();
        dbclient.like_post(1, 1).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(unlike_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post: ClientPost = dbclient_clone
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient_clone, &config.domain)
            .await;
        assert_eq!(post.likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_unlike_post_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();
        dbclient.like_post(1, 1).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(unlike_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post: ClientPost = dbclient_clone
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient_clone, &config.domain)
            .await;
        let expected: String = serde_json::to_string(&UnlikePostResponse {
            message: SuccessMessage::UnlikedPost.into(),
            result: post,
        })
        .unwrap();
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_unlike_post_which_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(unlike_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_unlike_post_twice(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(unlike_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_unlike_post_of_whom_isnt_your_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(2, "hii", 0).await.unwrap();
        dbclient.like_post(1, 2).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(unlike_post)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_unlike_post_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::patch().to(unlike_post)),
        )
        .await;

        let req = test::TestRequest::patch().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_post_comment(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::post().to(post_comment)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/1")
            .set_json(PostCommentBody {
                content: "lol".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let comments: Vec<ClientPostComment> = dbclient_clone
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient_clone, &config.domain)
            .await
            .comments;
        assert_eq!(comments.len(), 1);
    }

    #[sqlx::test]
    async fn test_post_comment_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::post().to(post_comment)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/1")
            .set_json(PostCommentBody {
                content: "lol".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post: ClientPost = dbclient
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient, config.domain)
            .await;
        let expected: String = serde_json::to_string(&PostCommentResponse {
            message: SuccessMessage::CommentPost.into(),
            result: post,
        })
        .unwrap();

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_post_comment_to_which_doesnt_exist(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::post().to(post_comment)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/1")
            .set_json(PostCommentBody {
                content: "lol".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_post_comment_of_whom_isnt_your_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(2, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::post().to(post_comment)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/1")
            .set_json(PostCommentBody {
                content: "lol".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_post_comment_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_id}", web::post().to(post_comment)),
        )
        .await;

        let req = test::TestRequest::post().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_comment(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();
        dbclient.save_post_comment(1, 1, "lol").await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{comment_id}", web::delete().to(delete_comment)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let comments: Vec<ClientPostComment> = dbclient
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient, &config.domain)
            .await
            .comments;
        assert_eq!(comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_comment_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();
        dbclient.save_post_comment(1, 1, "lol").await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle,
                }))
                .route("/{comment_id}", web::delete().to(delete_comment)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let post: ClientPost = dbclient
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient, &config.domain)
            .await;
        let expected: String = serde_json::to_string(&DeleteCommentResponse {
            message: SuccessMessage::DeletedPostComment.into(),
            result: post,
        })
        .unwrap();
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_delete_comment_which_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{comment_id}", web::delete().to(delete_comment)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_comment_which_im_not_the_author(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_post(1, "hii", 0).await.unwrap();
        dbclient.save_post_comment(1, 2, "lol").await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{comment_id}", web::delete().to(delete_comment)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_comment_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{comment_id}", web::delete().to(delete_comment)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_get_post_pic(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut payload: Vec<u8> = Vec::new();
        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"image\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          \x89PNG\r\n\x1a\n\0\0\0\rIHDR\0\0\0\x01\0\0\0\x01\x08\x02\0\0\0\x90wS\xde\0\0\0\x0fIDAT\x08\x1d\x01\x04\0\xfb\xff\0\0\0\0\0\x04\0\x01/R\xb4\x8d\0\0\0\0IEND\xaeB`\x82\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n".into_iter().for_each(|b: &u8| payload.push(*b));

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(save_post))
                .route("/{post_pic_name}", web::get().to(get_post_pic)),
        )
        .await;

        let req_save_post = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();
        let req_post_pic = test::TestRequest::default()
            .uri("/1_0")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();

        let resp_save_post = test::call_service(&app, req_save_post).await;
        let resp_post_pic = test::call_service(&app, req_post_pic).await;
        assert_eq!(resp_save_post.status(), http::StatusCode::OK);
        assert_eq!(resp_post_pic.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_post_pic_which_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{post_pic_name}", web::get().to(get_post_pic)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1_0")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }
}
