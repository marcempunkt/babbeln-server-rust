use crate::dbclient::{BlockedUserExt, UserExt};
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{ClientUser, DbBlockedUser, IntoClientUserExt};
use crate::success::SuccessMessage;
use crate::utils::paginate::Paginator;
use crate::AppState;
use actix_web::{web, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use validator::Validate;

pub fn blocked_users_scope() -> Scope {
    web::scope("/blocked-users")
        .route("/", web::get().to(get_blocked_users))
        .route("/", web::post().to(block_user))
        .route("/{user_id}", web::get().to(get_blocked_user))
        .route("/{user_id}", web::delete().to(unblock_user))
}

#[derive(Serialize, Deserialize, Validate)]
struct GetBlockedUsersQuery {
    #[validate(range(min = 1))]
    page: Option<usize>,
    #[validate(range(min = 1, max = 50))]
    page_size: Option<usize>,
}

async fn get_blocked_users(
    query: web::Query<GetBlockedUsersQuery>,
    auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let query: GetBlockedUsersQuery = query.into_inner();
    let user_id: usize = auth_token.id;

    query
        .validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let mut users: Vec<ClientUser> = Vec::new();

    for blocked_user in app_state.dbclient.get_blocked_users(user_id).await {
        if let Some(user) = app_state
            .dbclient
            .get_user(blocked_user.blocked_user_id as usize)
            .await
        {
            users.push(
                user.into_client(
                    &app_state.config.domain,
                    app_state.websocket_server_handle.clone(),
                )
                .await,
            );
        }
    }

    users.sort_by_key(|user: &ClientUser| user.username.to_lowercase());

    Ok(HttpResponse::Ok().json(
        Paginator::new()
            .page(query.page)
            .page_size(query.page_size)
            .url(&app_state.config.domain)
            .pathname("/blocked-users")
            .data(users)
            .into_paginated_response(),
    ))
}

#[derive(Serialize, Deserialize)]
struct BlockUserBody {
    user_id: usize,
}

#[derive(Serialize, Deserialize)]
struct BlockUserResponse {
    message: String,
    result: ClientUser,
}

async fn block_user(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    body: web::Json<BlockUserBody>,
) -> Result<HttpResponse, HttpError> {
    let body: BlockUserBody = body.into_inner();
    let user_id: usize = auth_token.id;
    let block_user_id: usize = body.user_id;

    if app_state.dbclient.get_user(block_user_id).await.is_none() {
        return Err(HttpError::bad_request(ErrorMessage::NoUserFound));
    }

    app_state
        .dbclient
        .block_user(user_id, block_user_id)
        .await?;

    let user: ClientUser = app_state
        .dbclient
        .get_user(block_user_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            &app_state.config.domain,
            app_state.websocket_server_handle.clone(),
        )
        .await;

    Ok(HttpResponse::Ok().json(BlockUserResponse {
        message: SuccessMessage::BlockedUser.into(),
        result: user,
    }))
}

#[derive(Serialize, Deserialize)]
struct GetBlockedUserResponse {
    message: String,
    result: ClientUser,
}

async fn get_blocked_user(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let blocked_user_id: usize = path.into_inner();

    let blocked_user: DbBlockedUser = app_state
        .dbclient
        .get_blocked_user(user_id, blocked_user_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::UserIsntBlocked))?;

    let user: ClientUser = app_state
        .dbclient
        .get_user(blocked_user.blocked_user_id as usize)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            &app_state.config.domain,
            app_state.websocket_server_handle.clone(),
        )
        .await;

    Ok(HttpResponse::Ok().json(GetBlockedUserResponse {
        message: SuccessMessage::GotBlockedUser.into(),
        result: user,
    }))
}

#[derive(Serialize, Deserialize)]
struct UnblockUserResponse {
    message: String,
}

async fn unblock_user(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let unblock_user_id: usize = path.into_inner();

    app_state
        .dbclient
        .unblock_user(user_id, unblock_user_id)
        .await?;

    Ok(HttpResponse::Ok().json(UnblockUserResponse {
        message: SuccessMessage::UnblockedUser.into(),
    }))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::dbclient::DbClient;
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils::{get_test_token, init_test_blocked_users, init_test_users};
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, AppState};
    use actix_web::{body::MessageBody, http, test, web, App};
    use sqlx::mysql::MySqlPool;

    #[sqlx::test]
    async fn test_get_blocked_users(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_blocked_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_blocked_users_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.block_user(2, 1).await.unwrap();

        let blocked_users: ClientUser = dbclient
            .get_user(1)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![blocked_users],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_blocked_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_blocked_users_next_url(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.block_user(1, 2).await.unwrap();
        dbclient.block_user(1, 3).await.unwrap();

        let user: ClientUser = dbclient
            .get_user(3)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: Some("http://localhost:4000/blocked-users/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![user],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_blocked_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page=1&page_size=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_blocked_users_previous_url(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.block_user(1, 2).await.unwrap();
        dbclient.block_user(1, 3).await.unwrap();

        let user: ClientUser = dbclient
            .get_user(2)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some("http://localhost:4000/blocked-users/?page=1&page_size=1".to_string()),
            result: vec![user],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_blocked_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page=2&page_size=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_blocked_users_is_sorted_by_username(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("bleon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("athomas", "thomas@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.block_user(1, 2).await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient.block_user(1, 3).await.unwrap();

        let blocked_users: Vec<ClientUser> = vec![
            dbclient
                .get_user(3)
                .await
                .unwrap()
                .into_client(&config.domain, websocket_server_handle.clone())
                .await,
            dbclient
                .get_user(2)
                .await
                .unwrap()
                .into_client(&config.domain, websocket_server_handle.clone())
                .await,
        ];

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: blocked_users,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_blocked_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_blocked_users_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_blocked_users)),
        )
        .await;

        let req = test::TestRequest::default().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_block_user(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(block_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(BlockUserBody { user_id: 2 })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_block_user_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let blocked_user: ClientUser = dbclient
            .get_user(2)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;
        let expected: String = serde_json::to_string(&BlockUserResponse {
            message: SuccessMessage::BlockedUser.into(),
            result: blocked_user,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(block_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(BlockUserBody { user_id: 2 })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_block_user_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(block_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(BlockUserBody { user_id: 2 })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_block_user_who_is_already_blocked(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_blocked_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(block_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(BlockUserBody { user_id: 2 })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_block_user_who_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(block_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_json(BlockUserBody { user_id: 2 })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_blocked_user(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_blocked_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_blocked_user)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_blocked_user_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_blocked_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let blocked_user: ClientUser = dbclient
            .get_user(2)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;
        let expected: String = serde_json::to_string(&GetBlockedUserResponse {
            message: SuccessMessage::GotBlockedUser.into(),
            result: blocked_user,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_blocked_user)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_blocked_user_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_blocked_user)),
        )
        .await;

        let req = test::TestRequest::default().uri("/2").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_get_blocked_user_who_isnt_blocked(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_blocked_user)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_unblock_user(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_blocked_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(unblock_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_unblock_user_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_blocked_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&UnblockUserResponse {
            message: SuccessMessage::UnblockedUser.into(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(unblock_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_unblock_user_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(unblock_user)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/2").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_unblock_user_who_wasnt_blocked(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(unblock_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }
}
