use crate::dbclient::{TokenExt, UserExt};
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::DbUser;
use crate::utils::password_utils;
use crate::AppState;
use actix_web::{web, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use validator::Validate;

pub fn token_scope() -> Scope {
    web::scope("/token")
        .route("/", web::post().to(create_token))
        .route("/refresh", web::post().to(refresh_token))
}

#[derive(Serialize, Deserialize, Validate)]
struct CreateTokenBody {
    #[validate(email)]
    email: String,
    #[validate(length(min = 8, max = 255))]
    password: String,
}

#[derive(Serialize, Deserialize)]
struct CreateTokenResponse {
    access: String,
    refresh: String,
}
async fn create_token(
    app_state: web::Data<AppState>,
    body: web::Json<CreateTokenBody>,
) -> Result<HttpResponse, HttpError> {
    let body: CreateTokenBody = body.into_inner();

    body.validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let user_id: usize = app_state
        .dbclient
        .get_user_by_email(&body.email)
        .await
        .ok_or(HttpError::unauthorized(ErrorMessage::WrongCredentials))
        .and_then(|user: DbUser| {
            let is_password_correct: bool = password_utils::compare(&body.password, &user.password);
            match is_password_correct {
                true => Ok(user.id as usize),
                false => Err(HttpError::unauthorized(ErrorMessage::WrongCredentials)),
            }
        })?;
    /* Create new token pair */
    let access: String =
        AuthenticationToken::create_access_token(user_id, &app_state.config.secret_token);
    let refresh: String =
        AuthenticationToken::create_refresh_token(user_id, &app_state.config.secret_token);
    app_state
        .dbclient
        .save_refresh_token(user_id, &refresh)
        .await?;
    Ok(HttpResponse::Ok().json(CreateTokenResponse { access, refresh }))
}

#[derive(Serialize, Deserialize)]
struct RefreshTokenBody {
    refresh: String,
}

#[derive(Serialize, Deserialize)]
struct RefreshTokenResponse {
    access: String,
    refresh: String,
}

async fn refresh_token(
    body: web::Json<RefreshTokenBody>,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let body: RefreshTokenBody = body.into_inner();
    let secret: &String = &app_state.config.secret_token;
    let user_id: usize = AuthenticationToken::decode_token(&body.refresh, secret)?;
    /* Check if refresh token is in db */
    app_state
        .dbclient
        .get_refresh_token(&body.refresh)
        .await
        .ok_or(HttpError::unauthorized(ErrorMessage::Unauthorized))?;
    /* Invalid this refresh token */
    app_state
        .dbclient
        .delete_refresh_token(&body.refresh)
        .await?;
    /* Create new token pair */
    let access: String = AuthenticationToken::create_access_token(user_id, secret);
    let refresh: String = AuthenticationToken::create_refresh_token(user_id, secret);
    app_state
        .dbclient
        .save_refresh_token(user_id, &refresh)
        .await?;

    Ok(HttpResponse::Ok().json(RefreshTokenResponse { access, refresh }))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::utils::test_utils::{get_expired_test_token, get_test_token};
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, dbclient::DbClient, AppState};
    use actix_web::{http, test, web, App};
    use sqlx::mysql::MySqlPool;

    #[sqlx::test]
    async fn test_create_token(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(create_token)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(CreateTokenBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_create_token_but_user_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(create_token)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(CreateTokenBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_create_token_but_with_wrong_credentials(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "asdf1234")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(create_token)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(CreateTokenBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_create_token_validation(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(create_token)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(CreateTokenBody {
                email: "marc.mail".to_string(),
                password: "1234".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_refresh_token(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let refresh: String = get_test_token(1, "shhh!");
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_refresh_token(1, &refresh).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(refresh_token)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RefreshTokenBody { refresh })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_refresh_token_with_expired_token(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let refresh: String = get_expired_test_token(1, "shhh!");
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient.save_refresh_token(1, &refresh).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(refresh_token)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RefreshTokenBody { refresh })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_refresh_token_with_invalid_token(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(refresh_token)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RefreshTokenBody {
                refresh: "invalid_token".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }
}
