use crate::dbclient::{
    BlockedUserExt, FriendExt, FriendrequestExt, MessageExt, PostExt, TokenExt, UserExt,
};
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{
    ClientUser, DbBlockedUser, DbFriend, DbFriendrequest, DbUser, IntoClientUserExt, Status,
};
use crate::scopes::{
    blocked_users::blocked_users_scope, chat_partners::chat_partners_scope, friends::friends_scope,
};
use crate::success::SuccessMessage;
use crate::utils::default_pics::DEFAULT_AVATAR_IMG;
use crate::utils::paginate::Paginator;
use crate::utils::password_utils;
use crate::utils::storage;
use crate::utils::upload::{IntoPayloadExt, Uploader, WriteToDiskExt};
use crate::AppState;
use actix_multipart::Multipart;
use actix_web::{web, HttpRequest, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use std::path::Path;
use tokio::fs;
use validator::Validate;

/// Scope aka. Router for the "/users" route
pub fn users_scope() -> Scope {
    web::scope("/users")
        .route("/", web::get().to(get_users))
        .route("/", web::post().to(register_user))
        .route("/", web::delete().to(delete_user))
        .route("/", web::patch().to(update_user))
        .route("/me", web::get().to(get_me))
        .route("/status", web::patch().to(update_user_status))
        .route("/{user_id}", web::get().to(get_user))
        .route("/avatar/", web::post().to(update_avatar))
        .route("/avatar/{user_id}", web::get().to(get_avatar))
        .service(friends_scope())
        .service(blocked_users_scope())
        .service(chat_partners_scope())
}

#[derive(Serialize, Deserialize, Validate)]
struct GetUsersQuery {
    query: Option<String>,
    #[validate(range(min = 1))]
    page: Option<usize>,
    #[validate(range(min = 1, max = 50))]
    page_size: Option<usize>,
    filter_me: Option<bool>,
    filter_friends: Option<bool>,
    filter_pending_friendrequests: Option<bool>,
    filter_blocked_users: Option<bool>,
}

async fn get_users(
    query: web::Query<GetUsersQuery>,
    auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let query_params: GetUsersQuery = query.into_inner();
    let user_id: usize = auth_token.id;

    query_params
        .validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let query: String = query_params.query.unwrap_or(String::new());
    let filter_me: bool = query_params.filter_me.unwrap_or(false);
    let filter_friends: bool = query_params.filter_friends.unwrap_or(false);
    let filter_pending_friendrequests: bool =
        query_params.filter_pending_friendrequests.unwrap_or(false);
    let filter_blocked_users: bool = query_params.filter_blocked_users.unwrap_or(false);

    let mut users: Vec<ClientUser> = Vec::new();

    let friends: Vec<usize> = {
        match filter_friends {
            true => app_state
                .dbclient
                .get_friends(user_id)
                .await
                .into_iter()
                .map(|friend: DbFriend| friend.get_user_id_of_friend(user_id).unwrap())
                .collect(),
            false => Vec::new(),
        }
    };

    let friendrequests: Vec<usize> = {
        match filter_pending_friendrequests {
            true => app_state
                .dbclient
                .get_friendrequests(user_id)
                .await
                .into_iter()
                .map(|req: DbFriendrequest| {
                    if req.from_user as usize == user_id {
                        return Some(req.to_user);
                    }

                    if req.to_user as usize == user_id {
                        return Some(req.from_user);
                    }

                    None
                })
                .map(|user_id: Option<u64>| user_id.unwrap() as usize)
                .collect(),
            false => Vec::new(),
        }
    };

    let blocked_users: Vec<usize> = {
        match filter_blocked_users {
            true => app_state
                .dbclient
                .get_blocked_users(user_id)
                .await
                .into_iter()
                .map(|blocked_user: DbBlockedUser| blocked_user.blocked_user_id as usize)
                .collect(),
            false => Vec::new(),
        }
    };

    if !query.is_empty() {
        let db_users: Vec<DbUser> = app_state.dbclient.get_users(&query).await;
        for db_user in db_users {
            let db_user_id: usize = db_user.id as usize;

            if filter_me && db_user_id == user_id {
                continue;
            }

            if filter_friends && friends.contains(&db_user_id) {
                continue;
            }

            if filter_pending_friendrequests && friendrequests.contains(&db_user_id) {
                continue;
            }

            if filter_blocked_users && blocked_users.contains(&db_user_id) {
                continue;
            }

            users.push(
                db_user
                    .into_client(
                        &app_state.config.domain,
                        app_state.websocket_server_handle.clone(),
                    )
                    .await,
            );
        }
    }

    Ok(HttpResponse::Ok().json(
        Paginator::new()
            .page(query_params.page)
            .page_size(query_params.page_size)
            .url(&app_state.config.domain)
            .pathname("/users")
            .extra_params(vec![("query".to_string(), query)])
            .data(users)
            .into_paginated_response(),
    ))
}

#[derive(Serialize, Deserialize, Validate)]
struct RegisterUserBody {
    #[validate(length(min = 4, max = 20), does_not_contain = " ")]
    username: String,
    #[validate(email)]
    email: String,
    #[validate(length(min = 8, max = 255))]
    password: String,
}

#[derive(Serialize, Deserialize)]
struct RegisterUserResponse {
    message: String,
    result: ClientUser,
}

async fn register_user(
    body: web::Json<RegisterUserBody>,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let body: RegisterUserBody = body.into_inner();

    body.validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;
    /* try to save user in DB (checks also for username, email) */
    app_state
        .dbclient
        .save_user(&body.username, &body.email, &body.password)
        .await?;

    let user: ClientUser = app_state
        .dbclient
        .get_user_by_email(&body.email)
        .await
        .ok_or(HttpError::server_error())?
        .into_client(
            &app_state.config.domain,
            app_state.websocket_server_handle.clone(),
        )
        .await;

    Ok(HttpResponse::Created().json(RegisterUserResponse {
        message: SuccessMessage::Register.into(),
        result: user,
    }))
}

#[derive(Serialize, Deserialize, Validate)]
struct DeleteUserBody {
    #[validate(email)]
    email: String,
    #[validate(length(min = 8, max = 255))]
    password: String,
}

#[derive(Serialize, Deserialize)]
struct DeleteUserResponse {
    message: String,
}

async fn delete_user(
    body: web::Json<DeleteUserBody>,
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let body: DeleteUserBody = body.into_inner();
    body.validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;
    /* Check credentials */
    app_state
        .dbclient
        .get_user(user_id)
        .await
        .ok_or(HttpError::unauthorized(ErrorMessage::WrongCredentials))
        .and_then(|user: DbUser| {
            let is_email_correct: bool = &user.email == &body.email;
            let is_password_correct: bool = password_utils::compare(&body.password, &user.password);
            match is_email_correct && is_password_correct {
                true => Ok(()),
                false => Err(HttpError::unauthorized(ErrorMessage::WrongCredentials)),
            }
        })?;

    /* Delete user specific data */
    app_state.dbclient.delete_refresh_tokens(user_id).await?;
    app_state.dbclient.delete_friends(user_id).await?;
    app_state.dbclient.delete_friendrequests(user_id).await?;
    app_state.dbclient.unblock_users(user_id).await?;
    app_state.dbclient.delete_messages(user_id).await?;
    app_state.dbclient.delete_posts(user_id).await?;
    // storage::delete_post_pics(user_id, &app_satte.config.upload_dir).await;
    storage::delete_avatar(user_id, &app_state.config.upload_dir).await;

    /* Delete account for real */
    app_state.dbclient.delete_user(user_id).await?;

    Ok(HttpResponse::Ok().json(DeleteUserResponse {
        message: SuccessMessage::DeletedUser.into(),
    }))
}

#[derive(Serialize, Deserialize, Validate, Debug)]
struct UpdateUserBody {
    #[validate(length(min = 4, max = 20))]
    new_username: Option<String>,
    #[validate(email)]
    new_email: Option<String>,
    #[validate(length(min = 8, max = 255))]
    new_password: Option<String>,
    password: String,
}

#[derive(Serialize, Deserialize)]
struct UpdateUserResponse {
    message: String,
    result: ClientUser,
}

async fn update_user(
    body: web::Json<UpdateUserBody>,
    auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let body: UpdateUserBody = body.into_inner();
    let user_id: usize = auth_token.id;
    let user: DbUser = app_state
        .dbclient
        .get_user(user_id)
        .await
        .ok_or(HttpError::unauthorized(ErrorMessage::Unauthorized))?;

    body.validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    /* Check password */
    if !password_utils::compare(&body.password, &user.password) {
        return Err(HttpError::bad_request(ErrorMessage::WrongCredentials));
    }

    /* Update username */
    if let Some(new_username) = &body.new_username {
        let is_username_same: bool = new_username == &user.username;
        if is_username_same {
            return Err(HttpError::bad_request(ErrorMessage::NewUsernameIsTheSame));
        }
        app_state
            .dbclient
            .update_user(user_id, Some(new_username), None, None, None)
            .await?;
    }
    /* Update email */
    if let Some(new_email) = &body.new_email {
        let is_new_email_same: bool = new_email == &user.email;
        if is_new_email_same {
            return Err(HttpError::bad_request(ErrorMessage::NewEmailIsTheSame));
        }
        app_state
            .dbclient
            .update_user(user_id, None, None, Some(new_email), None)
            .await?;
    }
    /* Update password */
    if let Some(new_password) = &body.new_password {
        app_state
            .dbclient
            .update_user(user_id, None, Some(new_password), None, None)
            .await?;
    }

    let updated_user: ClientUser = app_state
        .dbclient
        .get_user(user_id)
        .await
        .ok_or(HttpError::server_error())?
        .into_client(
            &app_state.config.domain,
            app_state.websocket_server_handle.clone(),
        )
        .await;

    Ok(HttpResponse::Ok().json(UpdateUserResponse {
        message: SuccessMessage::UpdatedUser.into(),
        result: updated_user,
    }))
}

#[derive(Serialize, Deserialize)]
struct GetMeResponse {
    message: String,
    result: ClientUser,
}

async fn get_me(
    auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;

    let user: ClientUser = app_state
        .dbclient
        .get_user(user_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            &app_state.config.domain,
            app_state.websocket_server_handle.clone(),
        )
        .await;

    Ok(HttpResponse::Ok().json(GetMeResponse {
        message: SuccessMessage::GotUser.into(),
        result: user,
    }))
}

#[derive(Serialize, Deserialize)]
struct UpdateUserStatusBody {
    new_status: String,
}

#[derive(Serialize, Deserialize)]
struct UpdateUserStatusResponse {
    message: String,
    result: ClientUser,
}

async fn update_user_status(
    body: web::Json<UpdateUserStatusBody>,
    auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let body: UpdateUserStatusBody = body.into_inner();
    let user_id: usize = auth_token.id;

    /* Update online status */
    app_state
        .dbclient
        .update_user::<String>(
            user_id,
            None,
            None,
            None,
            Some(Status::new(body.new_status)),
        )
        .await?;

    let updated_user: ClientUser = app_state
        .dbclient
        .get_user(user_id)
        .await
        .ok_or(HttpError::server_error())?
        .into_client(
            &app_state.config.domain,
            app_state.websocket_server_handle.clone(),
        )
        .await;

    Ok(HttpResponse::Ok().json(UpdateUserStatusResponse {
        message: SuccessMessage::UpdatedUserStatus.into(),
        result: updated_user,
    }))
}

#[derive(Serialize, Deserialize)]
struct GetUserResponse {
    message: String,
    result: ClientUser,
}

async fn get_user(
    path: web::Path<usize>,
    _auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = path.into_inner();
    let user: ClientUser = app_state
        .dbclient
        .get_user(user_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            &app_state.config.domain,
            app_state.websocket_server_handle.clone(),
        )
        .await;
    Ok(HttpResponse::Ok().json(GetUserResponse {
        message: SuccessMessage::GotUser.into(),
        result: user,
    }))
}

#[derive(Serialize, Deserialize)]
struct UpdateAvatarResponse {
    message: String,
}

async fn update_avatar(
    app_state: web::Data<AppState>,
    payload: Multipart,
    req: HttpRequest,
    auth_token: AuthenticationToken,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;

    Uploader::new()
        .required(true)
        .max_size_in_mb(1)
        .max_count(1)
        .into_avatar_payload(payload, &req)
        .await?
        .write_to_disk(user_id, &app_state.config.upload_dir, Some((200, 200)))
        .await?;

    Ok(HttpResponse::Ok().json(UpdateAvatarResponse {
        message: SuccessMessage::ChangedAvatar.into(),
    }))
}

async fn get_avatar(app_state: web::Data<AppState>, path: web::Path<usize>) -> HttpResponse {
    let user_id: usize = path.into_inner();
    let user_img_path = Path::new(&app_state.config.upload_dir)
        .join("avatar")
        .join(format!("{}.jpeg", user_id));

    let img: Vec<u8> = fs::read(user_img_path)
        .await
        .unwrap_or(DEFAULT_AVATAR_IMG.to_vec());

    HttpResponse::Ok().content_type("image/jpeg").body(img)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::dbclient::{
        BlockedUserExt, DbClient, FriendExt, FriendrequestExt, MessageExt, PostExt, TokenExt,
    };
    use crate::models::{
        DbBlockedUser, DbFriend, DbFriendrequest, DbMessage, DbPost, DbPostComment, DbPostLike,
    };
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils::{get_test_token, init_test_users};
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, AppState};
    use actix_web::{body::MessageBody, http, http::header::ContentType, test, web, App};
    use mime;
    use serde_json;
    use sqlx::mysql::MySqlPool;

    #[sqlx::test]
    async fn test_get_users(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_users_response(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        let user: DbUser = dbclient.get_user(1).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![
                user.into_client(&config.domain, websocket_server_handle.clone())
                    .await,
            ],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(69, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_next_url(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("amarc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("bmarc", "amarc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("cmarc", "bmarc@mail.de", "1234asdf")
            .await
            .unwrap();
        let user: DbUser = dbclient.get_user(1).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 3,
            next: Some("http://localhost:4000/users/?page=2&page_size=1&query=marc".to_string()),
            previous: None,
            result: vec![
                user.into_client(&config.domain, websocket_server_handle.clone())
                    .await,
            ],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc&page_size=1&page=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(69, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_previous_url(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("amarc", "amarc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("bmarc", "bmarc@mail.de", "1234asdf")
            .await
            .unwrap();
        let user: DbUser = dbclient.get_user(2).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some(
                "http://localhost:4000/users/?page=1&page_size=1&query=marc".to_string(),
            ),
            result: vec![
                user.into_client(&config.domain, websocket_server_handle.clone())
                    .await,
            ],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc&page_size=1&page=2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(69, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_but_without_query(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let expected: &str = "{\"count\":0,\"next\":null,\"previous\":null,\"result\":[]}";
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_but_page_size_is_too_big(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc&page_size=99999")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_users_but_page_is_zero(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc&page=0")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_users_filters_me(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 0,
            next: None,
            previous: None,
            result: Vec::new(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc&filter_me=true")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_filters_friends(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 0,
            next: None,
            previous: None,
            result: Vec::new(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=leon&filter_friends=true")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_filters_pending_friendrequests(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friendrequest(1, 2).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 0,
            next: None,
            previous: None,
            result: Vec::new(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=leon&filter_pending_friendrequests=true")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_filters_blocked_users(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.block_user(1, 2).await.unwrap();

        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 0,
            next: None,
            previous: None,
            result: Vec::new(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=leon&filter_blocked_users=true")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_is_sorted_by_username(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("cmarc", "cmarc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("amarc", "amarc@mail.de", "1234asdf")
            .await
            .unwrap();
        let users: Vec<ClientUser> = vec![
            dbclient
                .get_user(2)
                .await
                .unwrap()
                .into_client(&config.domain, websocket_server_handle.clone())
                .await,
            dbclient
                .get_user(1)
                .await
                .unwrap()
                .into_client(&config.domain, websocket_server_handle.clone())
                .await,
        ];

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: users,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(69, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_users_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_users)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?query=marc")
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_register_user(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "marc".to_string(),
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::CREATED);
    }

    #[sqlx::test]
    async fn test_register_user_response(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle: websocket_server_handle.clone(),
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "marc".to_string(),
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;

        let user: DbUser = dbclient_clone.get_user(1).await.unwrap();
        let expected: String = serde_json::to_string(&RegisterUserResponse {
            message: SuccessMessage::Register.into(),
            result: user
                .into_client(&config.domain, websocket_server_handle)
                .await,
        })
        .unwrap();

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_register_user_but_username_contains_spaces(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: " ma rc ".to_string(),
                email: "marc@gmail.com".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_register_user_but_username_is_taken(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "marc".to_string(),
                email: "marc@gmail.com".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_register_user_but_email_is_taken(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "warc".to_string(),
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_register_user_but_username_is_too_short(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "ma".to_string(),
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_register_user_but_email_is_invalid(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "marc".to_string(),
                email: "mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_register_user_but_password_is_too_short(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(register_user)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(RegisterUserBody {
                username: "marc".to_string(),
                email: "marc@mail.de".to_string(),
                password: "1234".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_user(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        let expected: String = serde_json::to_string(&GetUserResponse {
            message: SuccessMessage::GotUser.into(),
            result: dbclient
                .get_user(1)
                .await
                .unwrap()
                .into_client(&config.domain, websocket_server_handle.clone())
                .await,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_user)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_user_response(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config: config.clone(),
                    websocket_server_handle: websocket_server_handle.clone(),
                }))
                .route("/{user_id}", web::get().to(get_user)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        let user: DbUser = dbclient.get_user(1).await.unwrap();
        let expected: String = serde_json::to_string(&GetUserResponse {
            message: SuccessMessage::GotUser.into(),
            result: user
                .into_client(&config.domain, websocket_server_handle)
                .await,
        })
        .unwrap();
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_user_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_user)),
        )
        .await;

        let req = test::TestRequest::default().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_user(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_delete_user_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_user_but_without_body(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_delete_user_but_with_wrong_credentials(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "asdf1234".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_refresh_tokens(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_refresh_token(1, "shhh!").await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let refresh_tokens: Vec<String> = dbclient.get_refresh_tokens(1).await;
        assert_eq!(refresh_tokens.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_friends(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let friends: Vec<DbFriend> = dbclient.get_friends(1).await;
        assert_eq!(friends.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_friendrequests(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("sven", "sven@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friendrequest(1, 2).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let friendrequests: Vec<DbFriendrequest> = dbclient.get_friendrequests(1).await;
        assert_eq!(friendrequests.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_blocked_users(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.block_user(1, 2).await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let blocked_users: Vec<DbBlockedUser> = dbclient.get_blocked_users(1).await;
        assert_eq!(blocked_users.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_messages(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_message(1, 2, "hi").await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let messages: Vec<DbMessage> = dbclient.get_messages(1).await;
        assert_eq!(messages.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_posts_with_likes_and_comments(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_post(1, "hiii", 0).await.unwrap();
        dbclient.like_post(1, 1).await.unwrap();
        dbclient.save_post_comment(1, 1, "hahah").await.unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        let posts: Vec<DbPost> = dbclient.get_posts(1).await;
        let post_comments: Vec<DbPostComment> = dbclient.get_post_comments(1).await;
        let post_likes: Vec<DbPostLike> = dbclient.get_post_likes(1).await;
        assert_eq!(posts.len(), 0);
        assert_eq!(post_comments.len(), 0);
        assert_eq!(post_likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_user_also_deletes_avatar(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());
        let upload_dir: String = config.upload_dir.clone();

        const BLACK_PIXEL_PNG: [u8; 72] = [
            137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 1, 0, 0, 0, 1, 8, 2,
            0, 0, 0, 144, 119, 83, 222, 0, 0, 0, 15, 73, 68, 65, 84, 8, 29, 1, 4, 0, 251, 255, 0, 0, 0,
            0, 0, 4, 0, 1, 47, 82, 180, 141, 0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130,
        ];

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        image::load_from_memory(&BLACK_PIXEL_PNG).unwrap().save(
            Path::new(&upload_dir).join("avatar").join("1.jpeg")
        ).unwrap();

        assert_eq!(Path::new(&upload_dir).join("avatar").join("1.jpeg").exists(), true);

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient: dbclient.clone(),
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::delete().to(delete_user)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/")
            .set_json(DeleteUserBody {
                email: "marc@mail.de".to_string(),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);

        assert_eq!(Path::new(&upload_dir).join("avatar").join("1.jpeg").exists(), false);
    }

    #[sqlx::test]
    async fn test_update_user(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserBody {
                new_username: Some("leon".to_string()),
                new_email: Some("leon@mail.de".to_string()),
                new_password: Some("asdf1234".to_string()),
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        let updated_user: DbUser = dbclient_clone.get_user(1).await.unwrap();

        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(updated_user.username, "leon");
        assert_eq!(updated_user.email, "leon@mail.de");
        assert_eq!(
            password_utils::compare("1234asdf", &updated_user.password),
            false
        );
    }

    #[sqlx::test]
    async fn test_update_user_response(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        let pre_user: DbUser = dbclient.get_user(1).await.unwrap();
        let post_user: ClientUser = ClientUser {
            id: 1,
            username: "leon".to_string(),
            register_date: pre_user.register_date,
            status: "off".to_string(),
            avatar: "http://localhost:4000/users/avatar/1".to_string(),
        };
        let expected: String = serde_json::to_string(&UpdateUserResponse {
            message: SuccessMessage::UpdatedUser.into(),
            result: post_user,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserBody {
                new_username: Some("leon".to_string()),
                new_email: None,
                new_password: None,
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_update_user_email_but_email_is_taken(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserBody {
                new_email: Some("leon@mail.de".to_string()),
                new_username: None,
                new_password: None,
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_user_email_but_new_is_same_as_old(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserBody {
                new_email: Some("marc@mail.de".to_string()),
                new_username: None,
                new_password: None,
                password: "1234asdf".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_user_with_wrong_password(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserBody {
                new_password: Some("12345asdfg".to_string()),
                new_username: None,
                new_email: None,
                password: "asdf1234".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_user_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserBody {
                new_username: None,
                new_email: None,
                new_password: None,
                password: "1234asdf".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_get_me(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_me)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_me_response(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        let me: ClientUser = dbclient
            .get_user(1)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_me)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp: Vec<u8> = test::call_service(&app, req)
            .await
            .into_body()
            .try_into_bytes()
            .unwrap()
            .to_vec();
        let body: GetMeResponse =
            serde_json::from_str(std::str::from_utf8(&resp).unwrap()).unwrap();

        assert_eq!(me.id, body.result.id);
        assert_eq!(me.username, body.result.username);
    }

    #[sqlx::test]
    async fn test_get_me_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_me)),
        )
        .await;

        let req = test::TestRequest::default().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_update_user_status(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user_status)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserStatusBody {
                new_status: "away".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_user_status_to_off(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user_status)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserStatusBody {
                new_status: Status::Off.to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        let new_status: String = dbclient_clone.get_user(1).await.unwrap().status.to_string();
        assert_eq!(new_status, Status::Off.to_string());
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_user_status_to_on(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user_status)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserStatusBody {
                new_status: Status::On.to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        let new_status: String = dbclient_clone.get_user(1).await.unwrap().status.to_string();
        assert_eq!(new_status, Status::On.to_string());
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_user_status_to_away(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user_status)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserStatusBody {
                new_status: Status::Away.to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        let new_status: String = dbclient_clone.get_user(1).await.unwrap().status.to_string();
        assert_eq!(new_status, Status::Away.to_string());
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_user_status_to_dnd(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user_status)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserStatusBody {
                new_status: Status::Dnd.to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        let new_status: String = dbclient_clone.get_user(1).await.unwrap().status.to_string();
        assert_eq!(new_status, Status::Dnd.to_string());
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_user_status_to_unknown(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user_status)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserStatusBody {
                new_status: "unknown".to_string(),
            })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        let new_status: String = dbclient_clone.get_user(1).await.unwrap().status.to_string();
        assert_eq!(new_status, Status::On.to_string());
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_update_user_status_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::patch().to(update_user_status)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/")
            .set_json(UpdateUserStatusBody {
                new_status: "unknown".to_string(),
            })
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_update_avatar(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let upload_dir: String = config.upload_dir.clone();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut payload: Vec<u8> = Vec::new();
        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"image\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          \x89PNG\r\n\x1a\n\0\0\0\rIHDR\0\0\0\x01\0\0\0\x01\x08\x02\0\0\0\x90wS\xde\0\0\0\x0fIDAT\x08\x1d\x01\x04\0\xfb\xff\0\0\0\0\0\x04\0\x01/R\xb4\x8d\0\0\0\0IEND\xaeB`\x82\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n".into_iter().for_each(|b: &u8| payload.push(*b));

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(update_avatar)),
        )
        .await;

        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(
            Path::new(&format!("{}/avatar/1.jpeg", upload_dir)).exists(),
            true
        );
    }

    #[sqlx::test]
    async fn test_update_avatar_but_without_picture(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(update_avatar)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header(ContentType(mime::MULTIPART_FORM_DATA))
            .append_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_update_avatar_but_with_garbage(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let upload_dir: String = config.upload_dir.clone();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let mut payload: Vec<u8> = Vec::new();
        b"testasdadsad\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
          Content-Disposition: form-data; name=\"image\"; filename=\"post.png\"\r\n\
          Content-Type: image/png\r\n\r\n\
          some_garbage\r\n\
          --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n"
            .into_iter()
            .for_each(|b: &u8| payload.push(*b));

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(update_avatar)),
        )
        .await;

        let req = test::TestRequest::post()
            .insert_header((
                http::header::CONTENT_TYPE,
                "multipart/form-data; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ))
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .set_payload(payload)
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
        assert_eq!(
            Path::new(&format!("{}/avatar/1.jpeg", upload_dir)).exists(),
            false,
        );
    }

    #[sqlx::test]
    async fn test_update_avatar_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(update_avatar)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .insert_header(ContentType(mime::MULTIPART_FORM_DATA))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_get_avatar(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_avatar)),
        )
        .await;

        let req = test::TestRequest::default().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }
}
