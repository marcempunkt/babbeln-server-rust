use crate::dbclient::{FriendExt, UserExt};
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{ClientUser, DbFriend, IntoClientUserExt};
use crate::success::SuccessMessage;
use crate::utils::paginate::Paginator;
use crate::AppState;
use actix_web::{web, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use validator::Validate;

pub fn friends_scope() -> Scope {
    web::scope("/friends")
        .route("/", web::get().to(get_friends))
        .route("/{user_id}", web::get().to(get_friend))
        .route("/{user_id}", web::delete().to(remove_friend))
}

#[derive(Serialize, Deserialize, Validate)]
pub struct QueryGetFriends {
    #[validate(range(min = 1))]
    page: Option<usize>,
    #[validate(range(min = 1, max = 50))]
    page_size: Option<usize>,
}

async fn get_friends(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    query: web::Query<QueryGetFriends>,
) -> HttpResponse {
    let query: QueryGetFriends = query.into_inner();
    let user_id: usize = auth_token.id;
    let friend_items: Vec<DbFriend> = app_state.dbclient.get_friends(user_id).await;
    let mut friends: Vec<ClientUser> = Vec::new();

    // TODO Pass this logic into the paginator with FnOnce
    // with that this logic, which is time intensive, can be done only on the
    // neccessary items of the data/result
    for friend_item in friend_items {
        let user_id_of_friend: usize = friend_item.get_user_id_of_friend(user_id).unwrap();

        if let Some(friend) = app_state.dbclient.get_user(user_id_of_friend).await {
            friends.push(
                friend
                    .into_client(
                        &app_state.config.domain,
                        app_state.websocket_server_handle.clone(),
                    )
                    .await,
            );
        }
    }
    friends.sort_by_key(|user: &ClientUser| user.username.to_lowercase());

    HttpResponse::Ok().json(
        Paginator::new()
            .page(query.page)
            .page_size(query.page_size)
            .url(&app_state.config.domain)
            .pathname("/friends")
            .data(friends)
            .into_paginated_response(),
    )
}

#[derive(Serialize, Deserialize)]
struct GetFriendResponse {
    message: String,
    result: ClientUser,
}

async fn get_friend(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let user_id_of_friend: usize = path.into_inner();

    // Check if they're friends
    app_state
        .dbclient
        .get_friend(user_id, user_id_of_friend)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoFriendFound))?;

    let friend: ClientUser = app_state
        .dbclient
        .get_user(user_id_of_friend)
        .await
        .ok_or(HttpError::server_error())?
        .into_client(
            &app_state.config.domain,
            app_state.websocket_server_handle.clone(),
        )
        .await;

    Ok(HttpResponse::Ok().json(GetFriendResponse {
        message: SuccessMessage::GotFriend.into(),
        result: friend,
    }))
}

#[derive(Serialize, Deserialize)]
struct RemoveFriendResponse {
    message: String,
}

async fn remove_friend(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let user_id_of_friend: usize = path.into_inner();
    /* Check if they're friends */
    app_state
        .dbclient
        .get_friend(user_id, user_id_of_friend)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoFriendFound))?;
    /* Try to remove the friend */
    app_state
        .dbclient
        .delete_friend(user_id, user_id_of_friend)
        .await?;

    Ok(HttpResponse::Ok().json(RemoveFriendResponse {
        message: SuccessMessage::RemovedFriend.into(),
    }))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::dbclient::UserExt;
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils::{get_test_token, init_test_friends, init_test_users};
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, dbclient::DbClient, AppState};
    use actix_web::{body::MessageBody, http, test, web, App};
    use sqlx::mysql::MySqlPool;

    #[sqlx::test]
    async fn test_get_friends(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friends)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_friends_paginated_response(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();
        let friend: ClientUser = dbclient
            .get_user(2)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![friend],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friends)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friends_next_url(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("amarc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("martin", "martin@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_friend(1, 3).await.unwrap();

        let user: ClientUser = dbclient
            .get_user(2)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: Some("http://localhost:4000/friends/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![user],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friends)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page_size=1&page=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friends_previous_url(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("amarc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("martin", "martin@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_friend(1, 3).await.unwrap();

        let user: ClientUser = dbclient
            .get_user(3)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some("http://localhost:4000/friends/?page=1&page_size=1".to_string()),
            result: vec![user],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friends)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page_size=1&page=2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friends_is_sorted_by_username(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("thomas", "thomas@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_friend(1, 3).await.unwrap();

        let friends: Vec<ClientUser> = vec![
            dbclient
                .get_user(3)
                .await
                .unwrap()
                .into_client(&config.domain, websocket_server_handle.clone())
                .await,
            dbclient
                .get_user(2)
                .await
                .unwrap()
                .into_client(&config.domain, websocket_server_handle.clone())
                .await,
        ];

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: friends,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friends)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friends_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friends)),
        )
        .await;

        let req = test::TestRequest::default().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_get_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_friend)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_friend_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&GetFriendResponse {
            message: SuccessMessage::GotFriend.into(),
            result: dbclient
                .get_user(2)
                .await
                .unwrap()
                .into_client(&config.domain, websocket_server_handle.clone())
                .await,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_friend)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friend_but_isnt_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_friend)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_get_friend_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::get().to(get_friend)),
        )
        .await;

        let req = test::TestRequest::default().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_remove_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(remove_friend)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_remove_friend_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&RemoveFriendResponse {
            message: SuccessMessage::RemovedFriend.into(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(remove_friend)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_remove_friend_who_isnt_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(remove_friend)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    async fn test_remove_friend_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{user_id}", web::delete().to(remove_friend)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }
}
