use crate::dbclient::{FriendExt, FriendrequestExt, UserExt};
use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{
    ClientFriendrequest, ClientUser, DbFriendrequest, IntoClientFriendrequestExt, IntoClientUserExt,
};
use crate::success::SuccessMessage;
use crate::utils::paginate::Paginator;
use crate::AppState;
use actix_web::{web, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use validator::Validate;

pub fn friendrequests_scope() -> Scope {
    web::scope("/friendrequests")
        .route("/", web::get().to(get_friendrequests))
        .route("/", web::post().to(send_friendrequest))
        .route("/{friendreq_id}", web::get().to(get_friendrequest))
        .route("/{friendreq_id}", web::delete().to(delete_friendrequest))
        .route(
            "/accept/{friendreq_id}",
            web::patch().to(accept_friendrequest),
        )
        .route(
            "/decline/{friendreq_id}",
            web::patch().to(decline_friendrequest),
        )
}

#[derive(Serialize, Deserialize, Validate)]
struct QueryGetFriendrequests {
    #[validate(range(min = 1))]
    page: Option<usize>,
    #[validate(range(min = 1, max = 50))]
    page_size: Option<usize>,
}

async fn get_friendrequests(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    query: web::Query<QueryGetFriendrequests>,
) -> HttpResponse {
    let query: QueryGetFriendrequests = query.into_inner();
    let user_id: usize = auth_token.id;
    let friendrequests: Vec<ClientFriendrequest> = app_state
        .dbclient
        .get_friendrequests(user_id)
        .await
        .into_iter()
        .map(|f: DbFriendrequest| f.into_client(&app_state.config.domain))
        .collect();

    HttpResponse::Ok().json(
        Paginator::<ClientFriendrequest>::new()
            .page(query.page)
            .page_size(query.page_size)
            .url(&app_state.config.domain)
            .pathname("/friendrequests")
            .data(friendrequests)
            .into_paginated_response(),
    )
}

#[derive(Serialize, Deserialize)]
struct SendFriendrequestBody {
    to_user: usize,
}

#[derive(Serialize, Deserialize)]
struct SendFriendrequestResponse {
    message: String,
    result: ClientFriendrequest,
}

async fn send_friendrequest(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    body: web::Json<SendFriendrequestBody>,
) -> Result<HttpResponse, HttpError> {
    let body: SendFriendrequestBody = body.into_inner();
    let user_id: usize = auth_token.id;
    let to_user_id: usize = body.to_user;
    /* Check if you try to send yourself a friendrequest */
    if body.to_user == user_id {
        return Err(HttpError::bad_request(
            ErrorMessage::CantSendYourselfFriendrequest,
        ));
    }
    /* Check if there is already a pending friendrequest */
    if app_state
        .dbclient
        .get_friendrequest(user_id, to_user_id)
        .await
        .is_some()
    {
        return Err(HttpError::bad_request(
            ErrorMessage::AlreadyPendingFriendrequest,
        ));
    }
    /* Check if the to_user_id exists */
    app_state
        .dbclient
        .get_user(to_user_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?;
    /* Save friendrequest */
    app_state
        .dbclient
        .save_friendrequest(user_id, to_user_id)
        .await?;

    let friendrequest: ClientFriendrequest = app_state
        .dbclient
        .get_friendrequest(user_id, to_user_id)
        .await
        .ok_or(HttpError::server_error())?
        .into_client(&app_state.config.domain);

    Ok(HttpResponse::Ok().json(SendFriendrequestResponse {
        message: SuccessMessage::SentFriendrequest.into(),
        result: friendrequest,
    }))
}

#[derive(Serialize, Deserialize)]
struct GetFriendrequestResult {
    message: String,
    result: ClientFriendrequest,
}

async fn get_friendrequest(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let friendrequest_id: usize = path.into_inner();

    let friendrequest: ClientFriendrequest = app_state
        .dbclient
        .get_friendrequest_by_id(friendrequest_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoFriendrequestFound))
        .and_then(|req: DbFriendrequest| {
            let is_users_friendrequest: bool =
                req.from_user as usize == user_id || req.to_user as usize == user_id;
            match is_users_friendrequest {
                true => Ok(req),
                false => Err(HttpError::bad_request(ErrorMessage::NotYourFriendrequest)),
            }
        })?
        .into_client(&app_state.config.domain);

    Ok(HttpResponse::Ok().json(GetFriendrequestResult {
        message: SuccessMessage::GotFriendrequest.to_string(),
        result: friendrequest,
    }))
}

#[derive(Serialize, Deserialize)]
struct DeleteFriendrequestResponse {
    message: String,
}

async fn delete_friendrequest(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let friendrequest_id: usize = path.into_inner();
    /* Check if its legal to delete the friendrequest */
    app_state
        .dbclient
        .get_friendrequest_by_id(friendrequest_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoFriendrequestFound))
        .and_then(|req: DbFriendrequest| {
            let can_user_delete: bool =
                req.from_user as usize == user_id && req.to_user as usize != user_id;
            match can_user_delete {
                true => Ok(req),
                false => Err(HttpError::bad_request(
                    ErrorMessage::CantDeleteFriendrequest,
                )),
            }
        })?;
    /* Delete the friendrequest */
    app_state
        .dbclient
        .delete_friendrequest(friendrequest_id)
        .await?;

    Ok(HttpResponse::Ok().json(DeleteFriendrequestResponse {
        message: SuccessMessage::DeletedFriendrequest.into(),
    }))
}

#[derive(Serialize, Deserialize)]
struct AcceptFriendrequestResponse {
    message: String,
    result: ClientUser,
}

async fn accept_friendrequest(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let friendrequest_id: usize = path.into_inner();
    /* Check if its legal to accept */
    let user_id_of_friend: usize = app_state
        .dbclient
        .get_friendrequest_by_id(friendrequest_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoFriendrequestFound))
        .and_then(|req: DbFriendrequest| {
            let can_user_accept: bool =
                req.from_user as usize != user_id && req.to_user as usize == user_id;
            match can_user_accept {
                true => Ok(req.from_user as usize),
                false => Err(HttpError::bad_request(
                    ErrorMessage::CantAcceptFriendrequest,
                )),
            }
        })?;
    /* Create new friend */
    app_state
        .dbclient
        .save_friend(user_id, user_id_of_friend)
        .await?;
    /* Remove obsolete friendrequest */
    app_state
        .dbclient
        .delete_friendrequest(friendrequest_id)
        .await?;
    /* Get the new friend */
    let friend: ClientUser = app_state
        .dbclient
        .get_user(user_id_of_friend)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoUserFound))?
        .into_client(
            &app_state.config.domain,
            app_state.websocket_server_handle.clone(),
        )
        .await;

    Ok(HttpResponse::Ok().json(AcceptFriendrequestResponse {
        message: SuccessMessage::AcceptedFriendrequest.into(),
        result: friend,
    }))
}

#[derive(Serialize, Deserialize)]
struct DeclineFriendrequestResponse {
    message: String,
}

async fn decline_friendrequest(
    app_state: web::Data<AppState>,
    auth_token: AuthenticationToken,
    path: web::Path<usize>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let friendrequest_id: usize = path.into_inner();
    /* Check if its legal to decline */
    app_state
        .dbclient
        .get_friendrequest_by_id(friendrequest_id)
        .await
        .ok_or(HttpError::bad_request(ErrorMessage::NoFriendrequestFound))
        .and_then(|req: DbFriendrequest| {
            let can_user_decline: bool =
                req.from_user as usize != user_id && req.to_user as usize == user_id;
            match can_user_decline {
                true => Ok(()),
                false => Err(HttpError::bad_request(
                    ErrorMessage::CantDeclineFriendrequest,
                )),
            }
        })?;
    /* Remove obsolete friendrequest */
    app_state
        .dbclient
        .delete_friendrequest(friendrequest_id)
        .await?;

    Ok(HttpResponse::Ok().json(DeclineFriendrequestResponse {
        message: SuccessMessage::DeclinedFriendrequest.into(),
    }))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::models::IntoClientUserExt;
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils::{
        get_test_token, init_test_friendrequests, init_test_friends, init_test_users,
    };
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, dbclient::DbClient, AppState};
    use actix_web::{body::MessageBody, http, test, web, App};
    use sqlx::mysql::MySqlPool;

    #[sqlx::test]
    fn test_get_friendrequests(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_get_friendrequests_paginated_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let friendreq: ClientFriendrequest = dbclient
            .get_friendrequest(1, 2)
            .await
            .unwrap()
            .into_client(&config.domain);
        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 1,
            next: None,
            previous: None,
            result: vec![friendreq],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friendrequests_previous_url(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("martin", "martin@mail.de", "1234asdf")
            .await
            .unwrap();

        dbclient.save_friendrequest(1, 2).await.unwrap();
        dbclient.save_friendrequest(1, 3).await.unwrap();
        let friendrequest: ClientFriendrequest = dbclient
            .get_friendrequest(1, 3)
            .await
            .unwrap()
            .into_client(&config.domain);

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: Some("http://localhost:4000/friendrequests/?page=1&page_size=1".to_string()),
            result: vec![friendrequest],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page_size=1&page=2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friendrequests_next_url(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("martin", "martin@mail.de", "1234asdf")
            .await
            .unwrap();

        dbclient.save_friendrequest(1, 2).await.unwrap();
        dbclient.save_friendrequest(1, 3).await.unwrap();
        let friendrequest: ClientFriendrequest = dbclient
            .get_friendrequest(1, 2)
            .await
            .unwrap()
            .into_client(&config.domain);

        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: Some("http://localhost:4000/friendrequests/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![friendrequest],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page_size=1&page=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_friendrequests_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("thomas", "thomas@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friendrequest(1, 2).await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient.save_friendrequest(1, 3).await.unwrap();
        let friendrequests: Vec<ClientFriendrequest> = vec![
            dbclient
                .get_friendrequest(1, 2)
                .await
                .unwrap()
                .into_client(&config.domain),
            dbclient
                .get_friendrequest(1, 3)
                .await
                .unwrap()
                .into_client(&config.domain),
        ];
        let expected: String = serde_json::to_string(&PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: friendrequests,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    fn test_get_friendrequests_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_friendrequests)),
        )
        .await;

        let req = test::TestRequest::default().uri("/").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    fn test_send_friendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_send_friendrequest_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&SendFriendrequestResponse {
            message: SuccessMessage::SentFriendrequest.into(),
            result: ClientFriendrequest {
                id: 1,
                from_user: "http://localhost:4000/users/1".to_string(),
                to_user: "http://localhost:4000/users/2".to_string(),
                accept: "http://localhost:4000/friendrequests/accept/1".to_string(),
                decline: "http://localhost:4000/friendrequests/decline/1".to_string(),
            },
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    fn test_send_friendrequest_to_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_send_friendrequest_but_already_pending(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_send_friendrequest_but_user_does_not_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_send_friendrequest_created_friendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 2 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(
            dbclient_clone.get_friendrequest_by_id(1).await.is_some(),
            true
        );
    }

    #[sqlx::test]
    fn test_send_friendrequest_to_yourself(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 1 })
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_send_friendrequest_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::post().to(send_friendrequest)),
        )
        .await;

        let req = test::TestRequest::post()
            .uri("/")
            .set_json(SendFriendrequestBody { to_user: 99 })
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    fn test_get_friendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::get().to(get_friendrequest)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_get_friendrequest_reponse(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&GetFriendrequestResult {
            message: SuccessMessage::GotFriendrequest.into(),
            result: ClientFriendrequest {
                id: 1,
                from_user: "http://localhost:4000/users/1".to_string(),
                to_user: "http://localhost:4000/users/2".to_string(),
                accept: "http://localhost:4000/friendrequests/accept/1".to_string(),
                decline: "http://localhost:4000/friendrequests/decline/1".to_string(),
            },
        })
        .unwrap();
        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::get().to(get_friendrequest)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    fn test_get_friendrequest_that_doesnt_exist(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::get().to(get_friendrequest)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_get_friendrequest_which_isnt_mine(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::get().to(get_friendrequest)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_get_friendrequest_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::get().to(get_friendrequest)),
        )
        .await;

        let req = test::TestRequest::default().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    fn test_delete_friendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_delete_friendrequest_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        let expected: String = serde_json::to_string(&DeleteFriendrequestResponse {
            message: SuccessMessage::DeletedFriendrequest.into(),
        })
        .unwrap();
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    fn test_delete_friendrequest_that_doesnt_exist(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_delete_friendrequest_which_i_didnt_initiated(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_delete_friendrequest_which_isnt_mine(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_delete_friendrequest_removed_friendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(
            dbclient_clone.get_friendrequest_by_id(1).await.is_none(),
            true
        );
    }

    #[sqlx::test]
    fn test_delete_friendrequest_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::delete().to(delete_friendrequest)),
        )
        .await;

        let req = test::TestRequest::delete().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    fn test_accept_friendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&AcceptFriendrequestResponse {
            message: SuccessMessage::AcceptedFriendrequest.into(),
            result: dbclient
                .get_user(1)
                .await
                .unwrap()
                .into_client(&config.domain, websocket_server_handle.clone())
                .await,
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_which_doesnt_exist(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_which_isnt_mine(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_which_i_sent(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_removed_friendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(
            dbclient_clone.get_friendrequest_by_id(1).await.is_none(),
            true
        );
    }

    #[sqlx::test]
    fn test_accept_friendrequest_created_new_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(dbclient_clone.get_friend(2, 1).await.is_some(), true);
    }

    #[sqlx::test]
    fn test_accept_friendrequest_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(accept_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    fn test_decline_friendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(decline_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    fn test_decline_friendrequest_response(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let expected: String = serde_json::to_string(&DeclineFriendrequestResponse {
            message: SuccessMessage::DeclinedFriendrequest.into(),
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(decline_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    fn test_decline_friendrequest_which_doesnt_exist(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(decline_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/9")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_decline_friendrequest_which_isnt_mine(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(decline_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(3, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_decline_friendrequest_i_sent(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(decline_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::BAD_REQUEST);
    }

    #[sqlx::test]
    fn test_decline_friendrequest_removed_friendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(decline_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(
            dbclient_clone.get_friendrequest_by_id(1).await.is_none(),
            true
        );
    }

    #[sqlx::test]
    fn test_decline_friendrequest_didnt_create_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let dbclient_clone = dbclient.clone();
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(decline_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch()
            .uri("/1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(2, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
        assert_eq!(dbclient_clone.get_friend(2, 1).await.is_none(), true);
    }

    #[sqlx::test]
    fn test_decline_friendrequest_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/{friendreq_id}", web::patch().to(decline_friendrequest)),
        )
        .await;

        let req = test::TestRequest::patch().uri("/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }
}
