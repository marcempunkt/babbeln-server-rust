use crate::dbclient::{MessageExt, UserExt};
use crate::error::HttpError;
use crate::extractors::authentication_token::AuthenticationToken;
use crate::models::{ClientUser, DbMessage, IntoClientUserExt};
use crate::utils::paginate::Paginator;
use crate::AppState;
use actix_web::{web, HttpResponse, Scope};
use serde::{Deserialize, Serialize};
use validator::Validate;

pub fn chat_partners_scope() -> Scope {
    web::scope("/chat-partners").route("/", web::get().to(get_chat_partners))
}

#[derive(Serialize, Deserialize, Validate)]
struct GetUsersQuery {
    #[validate(range(min = 1))]
    page: Option<usize>,
    #[validate(range(min = 1, max = 50))]
    page_size: Option<usize>,
}

async fn get_chat_partners(
    query: web::Query<GetUsersQuery>,
    auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let query_params: GetUsersQuery = query.into_inner();
    let user_id: usize = auth_token.id;

    query_params
        .validate()
        .map_err(|e| HttpError::bad_request(e.to_string()))?;

    let user_ids: Vec<usize> = app_state
        .dbclient
        .get_messages(user_id)
        .await
        .into_iter()
        .fold(
            Vec::new(),
            |mut user_ids: Vec<usize>, message: DbMessage| {
                let sender: usize = message.sender as usize;
                let receiver: usize = message.receiver as usize;

                let is_memo_msg: bool = sender == user_id && receiver == user_id;

                if is_memo_msg {
                    return user_ids;
                }

                if sender == user_id && !user_ids.contains(&receiver) {
                    user_ids.push(message.receiver as usize);
                } else if receiver == user_id && !user_ids.contains(&sender) {
                    user_ids.push(message.sender as usize);
                }

                user_ids
            },
        );

    let mut users: Vec<ClientUser> = Vec::new();
    for user_id in user_ids {
        if let Some(user) = app_state.dbclient.get_user(user_id).await {
            let user: ClientUser = user
                .into_client(
                    &app_state.config.domain,
                    app_state.websocket_server_handle.clone(),
                )
                .await;
            users.push(user);
        }
    }

    Ok(HttpResponse::Ok().json(
        Paginator::new()
            .page(query_params.page)
            .page_size(query_params.page_size)
            .url(&app_state.config.domain)
            .pathname("/chat_partners")
            .data(users)
            .into_paginated_response(),
    ))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::dbclient::{DbClient, FriendExt, MessageExt, UserExt};
    use crate::models::ClientUser;
    use crate::utils::paginate::PaginatedResponse;
    use crate::utils::test_utils::get_test_token;
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, AppState};
    use actix_web::{body::MessageBody, http, test, web, App};
    use sqlx::mysql::MySqlPool;

    #[sqlx::test]
    async fn test_get_chat_partners(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());
        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_chat_partners)),
        )
        .await;

        let req = test::TestRequest::default()
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_get_chat_partners_response(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());
        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_chat_partners)),
        )
        .await;

        let req = test::TestRequest::default()
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 0,
            next: None,
            previous: None,
            result: Vec::new(),
        })
        .unwrap();
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_partners_next_url(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("thomas", "thomas@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_friend(1, 3).await.unwrap();
        dbclient.save_message(1, 2, "hi").await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient.save_message(1, 3, "hi").await.unwrap();
        let user: ClientUser = dbclient
            .get_user(3)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 2,
            next: Some("http://localhost:4000/chat_partners/?page=2&page_size=1".to_string()),
            previous: None,
            result: vec![user],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_chat_partners)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page_size=1&page=1")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_partners_previous_url(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("thomas", "thomas@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_friend(1, 3).await.unwrap();
        dbclient.save_message(1, 2, "hi").await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient.save_message(1, 3, "hi").await.unwrap();
        let user: ClientUser = dbclient
            .get_user(2)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 2,
            next: None,
            previous: Some("http://localhost:4000/chat_partners/?page=1&page_size=1".to_string()),
            result: vec![user],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_chat_partners)),
        )
        .await;

        let req = test::TestRequest::default()
            .uri("/?page_size=1&page=2")
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_partners_doesnt_include_yourself(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_message(1, 2, "hi").await.unwrap();
        dbclient
            .save_message(1, 1, "message to myself")
            .await
            .unwrap();
        let user: ClientUser = dbclient
            .get_user(2)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 1,
            next: None,
            previous: None,
            result: vec![user],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_chat_partners)),
        )
        .await;

        let req = test::TestRequest::default()
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_partners_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("thomas", "thomas@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_friend(1, 3).await.unwrap();
        dbclient.save_message(1, 2, "hi").await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient.save_message(1, 3, "hi").await.unwrap();
        let new_chat_partner: ClientUser = dbclient
            .get_user(3)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;
        let old_chat_partner: ClientUser = dbclient
            .get_user(2)
            .await
            .unwrap()
            .into_client(&config.domain, websocket_server_handle.clone())
            .await;

        let expected: String = serde_json::to_string(&PaginatedResponse::<ClientUser> {
            count: 2,
            next: None,
            previous: None,
            result: vec![new_chat_partner, old_chat_partner],
        })
        .unwrap();

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_chat_partners)),
        )
        .await;

        let req = test::TestRequest::default()
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.into_body().try_into_bytes().unwrap(), expected);
    }

    #[sqlx::test]
    async fn test_get_chat_partners_is_protected(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());
        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .route("/", web::get().to(get_chat_partners)),
        )
        .await;

        let req = test::TestRequest::default().to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }
}
