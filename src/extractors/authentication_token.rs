use crate::error::{ErrorMessage, HttpError};
use crate::AppState;
use actix_web::{cookie::Cookie, dev::Payload, http::header, web, FromRequest, HttpRequest};
use chrono::{Duration, Utc};
use jsonwebtoken::{
    decode, encode, errors::Error as JwtError, Algorithm, DecodingKey, EncodingKey, Header,
    TokenData, Validation,
};
use serde::{Deserialize, Serialize};
use std::future::{ready, Ready};

/// Payload of the jsonwebtoken
#[derive(Serialize, Deserialize)]
pub struct Claims {
    pub id: usize,
    pub exp: usize,
}

impl Claims {
    /// Creates a new instance of Claims aka. payload of jsonwebtoken with the expiry date set to one days.
    pub fn access(id: usize) -> Self {
        Self {
            id,
            exp: (Utc::now() + Duration::minutes(60)).timestamp() as usize,
        }
    }

    /// Creates a new instance of Claims aka. payload of jsonwebtoken with the expiry date set to 1 year.
    pub fn refresh(id: usize) -> Self {
        Self {
            id,
            exp: (Utc::now() + Duration::days(365)).timestamp() as usize,
        }
    }

    /// Only for testing use cases!  
    /// Creates a new instance of Claims that already expired.
    pub fn expired(id: usize) -> Self {
        Self {
            id,
            // It is expired by 61 seconds,
            // because 60 seconds is the default clockskew
            exp: (Utc::now() - Duration::seconds(61)).timestamp() as usize,
        }
    }
}

/// AuthenticationToken extractor:  
/// Retrieves the bearer token out of the req.headers
/// to decode it and create an AuthenticationToken object
/// with the user id stored inside it.
///
/// ```rust
/// async fn some_route(auth_token: AuthenticationToken) -> HttpResponse {
///   let user_id: usize = auth_token.id;
/// }
/// ```
#[derive(Debug)]
pub struct AuthenticationToken {
    pub id: usize,
}

impl AuthenticationToken {
    fn get_bearer_from_header(req: HttpRequest) -> Option<String> {
        let auth_header: &header::HeaderValue = req.headers().get(header::AUTHORIZATION)?;
        let auth_header: String = auth_header.to_str().unwrap_or("").to_string(); // "Bearer ...token"
        let auth_header: Vec<&str> = auth_header.split(" ").collect();
        // Get the token that is after "Bearer"
        let auth_token: String = auth_header
            .clone()
            .get(
                auth_header
                    .into_iter()
                    .position(|x| x == "Bearer")
                    .unwrap_or(1)
                    + 1,
            )?
            .to_string();

        if auth_token.is_empty() {
            return None;
        }

        Some(auth_token)
    }

    fn get_bearer_from_cookies(req: HttpRequest) -> Option<String> {
        let bearer_cookie: Cookie = req.cookie("Bearer")?;
        let auth_token: String = bearer_cookie.value().to_string();

        if auth_token.is_empty() {
            return None;
        }

        Some(auth_token)
    }

    fn decode(auth_token: &str, secret: &str) -> Result<AuthenticationToken, HttpError> {
        let token_result: Result<TokenData<Claims>, JwtError> = decode::<Claims>(
            &auth_token,
            &DecodingKey::from_secret(secret.as_ref()),
            &Validation::new(Algorithm::HS256),
        );

        match token_result {
            Ok(token) => Ok(AuthenticationToken {
                id: token.claims.id,
            }),
            Err(_e) => Err(HttpError::unauthorized(ErrorMessage::ExpiredAuthToken)),
        }
    }

    pub fn create_access_token(user_id: usize, secret: impl Into<String>) -> String {
        let token: String = encode(
            &Header::default(),
            &Claims::access(user_id),
            &EncodingKey::from_secret(secret.into().as_ref()),
        )
        .unwrap();

        token
    }

    pub fn create_refresh_token(user_id: usize, secret: impl Into<String>) -> String {
        let token: String = encode(
            &Header::default(),
            &Claims::refresh(user_id),
            &EncodingKey::from_secret(secret.into().as_ref()),
        )
        .unwrap();

        token
    }

    pub fn decode_token<T: Into<String>>(token: T, secret: T) -> Result<usize, HttpError> {
        let decoded: Result<TokenData<Claims>, JwtError> = decode(
            &token.into(),
            &DecodingKey::from_secret(&secret.into().as_ref()),
            &Validation::new(Algorithm::HS256),
        );

        match decoded {
            Ok(token) => Ok(token.claims.id),
            Err(_) => Err(HttpError::new(ErrorMessage::InvalidToken.to_string(), 401)),
        }
    }
}

impl FromRequest for AuthenticationToken {
    type Error = HttpError;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(req: &HttpRequest, _payload: &mut Payload) -> Self::Future {
        let secret: &str = &req
            .app_data::<web::Data<AppState>>()
            .unwrap()
            .config
            .secret_token;

        let auth_token_header: Option<String> =
            AuthenticationToken::get_bearer_from_header(req.clone());

        if let Some(auth_token) = auth_token_header {
            return ready(AuthenticationToken::decode(&auth_token, secret));
        }

        let auth_token_cookie: Option<String> =
            AuthenticationToken::get_bearer_from_cookies(req.clone());

        if let Some(auth_token) = auth_token_cookie {
            return ready(AuthenticationToken::decode(&auth_token, secret));
        }

        ready(Err(HttpError::unauthorized(ErrorMessage::MissingAuthToken)))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::utils::test_utils::{get_expired_test_token, get_test_token};
    use crate::ws::server::WebSocketServer;
    use crate::{config::Config, dbclient::DbClient, AppState};
    use actix_web::{get, http, test, web, App, HttpResponse};
    use sqlx::mysql::MySqlPool;

    #[get("/")]
    async fn handler(_: AuthenticationToken) -> HttpResponse {
        HttpResponse::Ok().into()
    }

    #[sqlx::test]
    async fn test_authentication_token(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .service(handler),
        )
        .await;

        let req = test::TestRequest::default()
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }

    #[sqlx::test]
    async fn test_authentication_token_but_it_expired(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .service(handler),
        )
        .await;

        let req = test::TestRequest::default()
            .insert_header((
                http::header::AUTHORIZATION,
                format!("Bearer {}", get_expired_test_token(1, "shhh!")),
            ))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_no_authentication_token(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .service(handler),
        )
        .await;

        let req = test::TestRequest::default().to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::UNAUTHORIZED);
    }

    #[sqlx::test]
    async fn test_authentication_token_as_cookie(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        let (_, websocket_server_handle) = WebSocketServer::new(dbclient.clone());

        let app = test::init_service(
            App::new()
                .app_data(web::Data::new(AppState {
                    dbclient,
                    config,
                    websocket_server_handle,
                }))
                .service(handler),
        )
        .await;

        let req = test::TestRequest::default()
            .cookie(Cookie::new("Bearer", get_test_token(1, "shhh!")))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), http::StatusCode::OK);
    }
}
