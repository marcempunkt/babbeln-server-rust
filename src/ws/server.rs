use crate::dbclient::{DbClient, FriendExt, PostExt};
use crate::models::DbFriend;
use crate::ws::events::{Command, EventWrapper, SocketEvent};
use actix_ws::{Message, MessageStream, Session};
use futures_util::StreamExt;
use std::cmp::{Eq, PartialEq};
use std::collections::{HashMap, HashSet};
use std::hash::{Hash, Hasher};
use tokio::sync::mpsc::{self, UnboundedReceiver, UnboundedSender};
use tokio::sync::oneshot;
use tokio::task::{spawn, spawn_local};
use tokio::time::{sleep, Duration};
use uuid::Uuid;

// ----------------------------------------------- Server Instance
pub struct WebSocketServer {
    connections: HashMap<usize, HashSet<WebSocketConnection>>,
    dbclient: DbClient,
    receiver: UnboundedReceiver<Command>,
}

impl WebSocketServer {
    pub fn new(dbclient: DbClient) -> (Self, WebSocketServerHandle) {
        let (sender, receiver): (UnboundedSender<Command>, UnboundedReceiver<Command>) =
            mpsc::unbounded_channel();
        let connections: HashMap<usize, HashSet<WebSocketConnection>> = HashMap::new();

        (
            Self {
                connections,
                dbclient,
                receiver,
            },
            WebSocketServerHandle { sender },
        )
    }

    pub fn run(websocket_server: Self) {
        spawn(websocket_server.receive_loop());
    }

    async fn receive_loop(mut self) {
        // TODO log every command especially message or inside the handler???
        // In here comes all the logic to send a socket event
        // to a specific user
        while let Some(command) = self.receiver.recv().await {
            match command {
                Command::Connect { user_id, new_conn } => {
                    println!("Command::Connect");
                    self.connect(user_id, new_conn).await;
                }
                Command::Disconnect { conn } => {
                    println!("Command::Disconnect");
                    self.disconnect(conn);
                }
                Command::Message { socket_event, conn } => {
                    let _ = self.handle_message(socket_event, conn.clone()).await;
                }
                Command::GetIsConnected { user_id, sender } => {
                    self.is_user_online(user_id, sender);
                }
            }
        }
    }

    async fn connect(&mut self, user_id: usize, mut new_conn: WebSocketConnection) {
        let _ = new_conn.session.ping(&[]).await;

        self.connections
            .entry(user_id)
            .and_modify(|set: &mut HashSet<WebSocketConnection>| {
                // add new socket session to existing hashset
                set.insert(new_conn.clone());
            })
            .or_insert_with(|| {
                // Create new entry with new hashset
                let mut new_hashset: HashSet<WebSocketConnection> = HashSet::new();
                new_hashset.insert(new_conn.clone());
                return new_hashset;
            });

        self.debug();
    }

    fn disconnect(&mut self, conn: WebSocketConnection) {
        // // Remove sockets connections from set
        self.connections.entry(conn.user_id).and_modify(
            |set: &mut HashSet<WebSocketConnection>| {
                set.remove(&conn);
            },
        );

        // Remove user_id from map when empty
        if let Some(connections_set) = self.connections.get(&conn.user_id) {
            if connections_set.is_empty() {
                self.connections.remove_entry(&conn.user_id);
            }
        }
    }

    async fn handle_message(
        &mut self,
        socket_event: SocketEvent,
        conn: WebSocketConnection,
    ) -> Result<(), ()> {
        match socket_event.into_event_wrapper() {
            EventWrapper::Normal(socket_event) => {
                // Only execute when
                // from_user_id, to_user_id & reference_object_id
                // is present
                let from_user_id: usize = socket_event.payload.from_user_id.ok_or(())?;
                let to_user_id: usize = socket_event.payload.to_user_id.ok_or(())?;

                let all_to_user_clients: Vec<WebSocketConnection> =
                    self.get_connections_of_user(to_user_id, None);
                let all_from_user_clients: Vec<WebSocketConnection> =
                    self.get_connections_of_user(from_user_id, Some(conn));

                let mut all_clients: Vec<WebSocketConnection> = Vec::new();
                all_clients.extend(all_to_user_clients);
                all_clients.extend(all_from_user_clients);

                self.emit_to(all_clients, socket_event).await;
            }
            EventWrapper::Private(socket_event) => {
                // Only execute when
                // from_user_id & reference_object_id
                // is present
                let from_user_id: usize = socket_event.payload.from_user_id.ok_or(())?;
                let _reference_object_id = socket_event.payload.reference_object_id.ok_or(())?;

                let all_from_user_clients: Vec<WebSocketConnection> =
                    self.get_connections_of_user(from_user_id, Some(conn));

                self.emit_to(all_from_user_clients, socket_event).await;
            }
            EventWrapper::Public(socket_event) => {
                // Only execute when
                // from_user_id & reference_object_id
                // is present
                let from_user_id: usize = socket_event.payload.from_user_id.ok_or(())?;
                let post_id: usize = socket_event.payload.reference_object_id.ok_or(())?;
                let post_author: usize =
                    self.dbclient.get_post(post_id).await.ok_or(())?.author as usize;
                let friends_of_author: Vec<usize> = self
                    .dbclient
                    .get_friends(post_author)
                    .await
                    .into_iter()
                    .map(|f: DbFriend| f.get_user_id_of_friend(post_author))
                    .filter(|maybe_id: &Option<usize>| maybe_id.is_some())
                    .map(|id: Option<usize>| id.unwrap())
                    .collect();

                let from_user_conns: Vec<WebSocketConnection> =
                    self.get_connections_of_user(from_user_id, Some(conn));
                let mut all_friends_of_author_conns: Vec<WebSocketConnection> = Vec::new();

                for user_id in friends_of_author {
                    let conns: Vec<WebSocketConnection> =
                        self.get_connections_of_user(user_id, None);
                    all_friends_of_author_conns.extend(conns);
                }

                let mut all_conns: Vec<WebSocketConnection> = Vec::new();
                all_conns.extend(from_user_conns);
                all_conns.extend(all_friends_of_author_conns);

                self.emit_to(all_conns, socket_event).await;
            }
            EventWrapper::FriendCircle(socket_event) => {
                // Get all the from_user friends
                // Get all their connections
                let from_user_id: usize = socket_event.payload.from_user_id.ok_or(())?;
                let from_users_friends: Vec<usize> = self
                    .dbclient
                    .get_friends(from_user_id)
                    .await
                    .into_iter()
                    .map(|f: DbFriend| f.get_user_id_of_friend(from_user_id))
                    .filter(|maybe_id: &Option<usize>| maybe_id.is_some())
                    .map(|id: Option<usize>| id.unwrap())
                    .collect();

                let from_user_conns: Vec<WebSocketConnection> =
                    self.get_connections_of_user(from_user_id, Some(conn));
                let mut friends_conns: Vec<WebSocketConnection> = Vec::new();

                for user_id in from_users_friends {
                    let conns: Vec<WebSocketConnection> =
                        self.get_connections_of_user(user_id, None);
                    friends_conns.extend(conns);
                }

                let mut all_conns: Vec<WebSocketConnection> = Vec::new();
                all_conns.extend(from_user_conns);
                all_conns.extend(friends_conns);

                self.emit_to(all_conns, socket_event).await;
            }
        };

        Ok(())
    }

    async fn emit_to(&self, conns: Vec<WebSocketConnection>, socket_event: SocketEvent) {
        for mut conn in conns {
            let _ = conn
                .session
                .text(serde_json::to_string(&socket_event).unwrap())
                .await;
        }
    }

    fn get_connections_of_user(
        &self,
        user_id: usize,
        exclude_conn: Option<WebSocketConnection>,
    ) -> Vec<WebSocketConnection> {
        let connections: Option<&HashSet<WebSocketConnection>> = self.connections.get(&user_id);

        if connections.is_none() {
            return Vec::new();
        }

        let connections: Vec<WebSocketConnection> = connections
            .unwrap()
            .into_iter()
            .map(|conn: &WebSocketConnection| conn.clone())
            .filter(|conn: &WebSocketConnection| {
                if exclude_conn.is_some() {
                    // remove the conn to be excluded
                    return conn.clone() != exclude_conn.clone().unwrap();
                } else {
                    return true;
                }
            })
            .collect();

        connections
    }

    /// used by DbUser.into_client
    fn is_user_online(&self, user_id: usize, sender: oneshot::Sender<bool>) {
        let _ = sender.send(self.connections.contains_key(&user_id));
    }

    fn debug(&self) {
        // Print all user_ids with its socket_ids
        for conn in &self.connections {
            println!("user_id: {}", conn.0);
            for socket_conn in conn.1 {
                println!("socket_ids: {:#?}", socket_conn.socket_id);
            }
        }
    }
}

// ----------------------------------------------- Sender to WebSocket Server Instance
#[derive(Clone)]
pub struct WebSocketServerHandle {
    pub sender: UnboundedSender<Command>,
}

impl WebSocketServerHandle {
    pub async fn connect(
        self,
        user_id: usize,
        new_conn: WebSocketConnection,
        msg_stream: MessageStream,
    ) {
        // Save new connection in the server
        let _ = self.sender.send(Command::Connect {
            user_id,
            new_conn: new_conn.clone(),
        });

        self.socket_event_loop(new_conn, msg_stream).await;
    }

    async fn disconnect(&self, conn: WebSocketConnection) {
        let _ = conn.session.clone().close(None).await;
        let _ = self.sender.send(Command::Disconnect { conn });
    }

    async fn socket_event_loop(self, mut conn: WebSocketConnection, mut msg_stream: MessageStream) {
        spawn_local(async move {
            while let Some(Ok(msg)) = msg_stream.next().await {
                match msg {
                    Message::Pong(bytes) => {
                        // TODO close after 10,20sec if no ping
                        sleep(Duration::from_secs(5)).await;
                        let _ = conn.session.ping(&bytes).await;
                    }
                    Message::Text(s) => self.handle_text_message(s, conn.clone()).await,
                    _ => break,
                }
            }

            self.disconnect(conn).await;
        });
    }

    async fn handle_text_message(&self, message: impl Into<String>, conn: WebSocketConnection) {
        let message: String = message.into();
        dbg!(&message);
        if let Ok(socket_event) = serde_json::from_str::<SocketEvent>(&message) {
            let _ = self.sender.send(Command::Message { socket_event, conn });
        }
    }

    /// used by DbUser.into_client
    pub async fn is_user_connected(&self, user_id: usize) -> bool {
        let (sender, receiver) = oneshot::channel();

        if let Err(_) = self
            .sender
            .send(Command::GetIsConnected { user_id, sender })
        {
            return false;
        }

        match receiver.await {
            Ok(is_connected) => is_connected,
            Err(_) => false,
        }
    }
}

// ----------------------------------------------- Single Connection
#[derive(Clone)]
pub struct WebSocketConnection {
    pub user_id: usize,
    pub socket_id: Uuid,
    pub session: Session,
}

impl WebSocketConnection {
    pub fn new(user_id: usize, session: Session) -> Self {
        Self {
            user_id,
            socket_id: Uuid::new_v4(),
            session,
        }
    }
}

impl PartialEq for WebSocketConnection {
    fn eq(&self, other: &Self) -> bool {
        let socket_id_eq: bool = self.socket_id == other.socket_id;
        let user_id_eq: bool = self.user_id == other.user_id;
        socket_id_eq && user_id_eq
    }
}

impl Eq for WebSocketConnection {}

impl Hash for WebSocketConnection {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.socket_id.hash(state);
        self.user_id.hash(state);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::utils::test_utils::get_test_token;
    use crate::ws::events::{SocketAction, SocketEvent, SocketPayload};
    use futures_util::stream::{SplitSink, SplitStream};
    use futures_util::SinkExt;
    use sqlx::MySqlPool;
    use tokio::net::TcpStream;
    use tokio_tungstenite::tungstenite::http::{header, Request};
    use tokio_tungstenite::tungstenite::protocol::Message;
    use tokio_tungstenite::{connect_async, MaybeTlsStream, WebSocketStream};

    async fn create_test_client(
        user_id: usize,
    ) -> (
        SplitSink<WebSocketStream<MaybeTlsStream<TcpStream>>, Message>,
        SplitStream<WebSocketStream<MaybeTlsStream<TcpStream>>>,
    ) {
        let request = Request::builder()
            .uri("ws://127.0.0.1:4000/ws/")
            .header(header::HOST, "127.0.0.1:4000")
            .header(
                header::AUTHORIZATION,
                &format!("Bearer {}", get_test_token(user_id, "shhh!")),
            )
            .header(header::UPGRADE, "websocket")
            .header(header::CONNECTION, "Upgrade")
            .header(header::SEC_WEBSOCKET_KEY, "pDTDUR/eEFpXHb847ddH1g==")
            .header(header::SEC_WEBSOCKET_VERSION, "13")
            .body(())
            .unwrap();

        // Connect to the WebSocket server
        let (ws_stream, _) = connect_async(request).await.expect("Failed to connect");
        let (write, read) = ws_stream.split();

        (write, read)
    }

    fn generic_send_message() -> String {
        serde_json::to_string(&SocketEvent {
            action: SocketAction::SendMessage,
            payload: SocketPayload {
                from_user_id: Some(1),
                to_user_id: Some(2),
                reference_object_id: Some(420),
            },
        })
        .unwrap()
    }

    #[sqlx::test]
    async fn test_websocketserver_create_and_run(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let (server, _) = WebSocketServer::new(dbclient);
        WebSocketServer::run(server);
    }

    #[tokio::test]
    async fn test_websocketserver_connect() {
        let (mut write, _) = create_test_client(1).await;
        let _ = write.close().await;
    }

    #[tokio::test]
    async fn test_websocketserver_disconnect() {
        let (mut write, _) = create_test_client(1).await;
        write.close().await.unwrap();
    }

    #[tokio::test]
    async fn test_websocketserver_emit() {
        let (mut write, _) = create_test_client(1).await;
        let msg: String = generic_send_message();
        write.send(Message::text(msg)).await.unwrap();
        let _ = write.close().await;
    }

    #[tokio::test]
    async fn test_websocketserver_emit_to_user() {
        let (mut one_sender, _) = create_test_client(1).await;
        let (mut two_sender, two_receiver) = create_test_client(2).await;

        let msg: String = generic_send_message();

        one_sender.send(Message::text(&msg)).await.unwrap();

        let received: String = match two_receiver.into_future().await.0.unwrap().unwrap() {
            Message::Text(msg) => msg,
            _ => panic!("No text was send"),
        };

        let _ = one_sender.close().await;
        let _ = two_sender.close().await;
        assert_eq!(received, msg);
    }

    #[tokio::test]
    async fn test_websocket_server_emit_to_many_users() {
        let (mut one_sender, _) = create_test_client(1).await;
        let (mut two_sender, two_receiver) = create_test_client(2).await;
        let (mut three_sender, three_receiver) = create_test_client(2).await;

        let msg: String = generic_send_message();

        one_sender.send(Message::text(&msg)).await.unwrap();

        let received_two: String = match two_receiver.into_future().await.0.unwrap().unwrap() {
            Message::Text(msg) => msg,
            _ => panic!("No text was send"),
        };

        let received_three: String = match three_receiver.into_future().await.0.unwrap().unwrap() {
            Message::Text(msg) => msg,
            _ => panic!("No text was send"),
        };

        let _ = one_sender.close().await;
        let _ = two_sender.close().await;
        let _ = three_sender.close().await;

        assert_eq!(received_two, msg);
        assert_eq!(received_three, msg);
    }

    #[tokio::test]
    async fn test_websocket_server_emit_sends_also_to_other_from_users() {
        let (mut one_sender, _) = create_test_client(1).await;
        let (mut two_sender, two_receiver) = create_test_client(1).await;
        let (mut three_sender, three_receiver) = create_test_client(1).await;

        let msg: String = generic_send_message();

        one_sender.send(Message::text(&msg)).await.unwrap();

        let received_two: String = match two_receiver.into_future().await.0.unwrap().unwrap() {
            Message::Text(msg) => msg,
            _ => panic!("No text was send"),
        };

        let received_three: String = match three_receiver.into_future().await.0.unwrap().unwrap() {
            Message::Text(msg) => msg,
            _ => panic!("No text was send"),
        };

        let _ = one_sender.close().await;
        let _ = two_sender.close().await;
        let _ = three_sender.close().await;

        assert_eq!(received_two, msg);
        assert_eq!(received_three, msg);
    }

    #[tokio::test]
    async fn test_websocket_server_emit_doesnt_send_back_to_client() {
        let (mut sender, receiver) = create_test_client(348454589).await;
        let msg: String = generic_send_message();
        sender.send(Message::text(&msg)).await.unwrap();

        // Listen to receiver of client one for x secs
        if let Ok(x) =
            tokio::time::timeout(std::time::Duration::from_secs(2), receiver.into_future()).await
        {
            sender.close().await.unwrap();
            panic!("Got something {}", x.0.unwrap().unwrap());
        }

        sender.close().await.unwrap();
    }

    #[tokio::test]
    async fn test_websocket_server_emit_normal_event() {
        let (mut sender_one, _) = create_test_client(1).await;
        let (mut sender_two, receiver_two) = create_test_client(1).await;
        let (mut sender_three, receiver_three) = create_test_client(2).await;

        let msg: String = serde_json::to_string(&SocketEvent {
            action: SocketAction::SendMessage,
            payload: SocketPayload {
                from_user_id: Some(1),
                to_user_id: Some(2),
                reference_object_id: Some(420),
            },
        })
        .unwrap();

        sender_one.send(Message::text(&msg)).await.unwrap();

        let received_two: String = match receiver_two.into_future().await.0.unwrap().unwrap() {
            Message::Text(msg) => msg,
            _ => panic!("No text was send"),
        };
        let received_three: String = match receiver_three.into_future().await.0.unwrap().unwrap() {
            Message::Text(msg) => msg,
            _ => panic!("No text was send"),
        };

        sender_one.close().await.unwrap();
        sender_two.close().await.unwrap();
        sender_three.close().await.unwrap();

        assert_eq!(received_two, msg);
        assert_eq!(received_three, msg);
    }

    #[tokio::test]
    async fn test_websocket_server_emit_normal_event_sends_to_all_from_user_clients() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_normal_event_doesnt_emit_to_third_party_users() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_normal_event_with_bad_payload() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_private_event() {
        let (mut sender_one, _) = create_test_client(3425334543).await;
        let (mut sender_two, receiver_two) = create_test_client(3425334543).await;

        let msg: String = serde_json::to_string(&SocketEvent {
            action: SocketAction::BlockUser,
            payload: SocketPayload {
                from_user_id: Some(3425334543),
                reference_object_id: Some(420),
                ..Default::default()
            },
        })
        .unwrap();

        sender_one.send(Message::text(&msg)).await.unwrap();

        let received_two: String = match receiver_two.into_future().await.0.unwrap().unwrap() {
            Message::Text(msg) => msg,
            _ => panic!("No text was send"),
        };

        sender_one.close().await.unwrap();
        sender_two.close().await.unwrap();

        assert_eq!(received_two, msg);
    }

    #[tokio::test]
    async fn test_websocket_server_emit_private_event_doesnt_emit_to_other_users() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_private_event_with_bad_payload() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_public_event() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_public_event_with_bad_payload() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_friend_circle_event() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_friend_circle_event_with_bad_payload() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_delete_user() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_update_user_status() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_update_user_name() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_update_user_avatar() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_remove_friend() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_block_user() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_unblock_user() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_send_friendrequest() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_remove_friendrequest() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_accept_friendrequest() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_decline_friendrequest() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_create_post() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_remove_post() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_like_post() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_unlike_post() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_comment_post() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_remove_comment_post() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_send_message() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_update_message() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_unsend_message() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_remove_message() {
        todo!()
    }

    #[tokio::test]
    async fn test_websocket_server_emit_delete_chat() {
        todo!()
    }
}
