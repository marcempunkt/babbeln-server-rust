use crate::error::{ErrorMessage, HttpError};
use crate::extractors::authentication_token::AuthenticationToken;
use crate::ws::server::{WebSocketConnection, WebSocketServerHandle};
use crate::AppState;
use actix_web::{web, HttpRequest, HttpResponse, Scope};
use tokio::task::spawn_local;

pub fn socket_scope() -> Scope {
    web::scope("/ws").route("/", web::get().to(ws))
}

async fn ws(
    req: HttpRequest,
    body: web::Payload,
    auth_token: AuthenticationToken,
    app_state: web::Data<AppState>,
) -> Result<HttpResponse, HttpError> {
    let user_id: usize = auth_token.id;
    let websocket_server_handle: WebSocketServerHandle = app_state.websocket_server_handle.clone();
    let (response, session, msg_stream) = actix_ws::handle(&req, body)
        .map_err(|_| HttpError::bad_request(ErrorMessage::CouldntConnectToSocketServer))?;

    let new_conn = WebSocketConnection::new(user_id, session);

    spawn_local(websocket_server_handle.connect(user_id, new_conn, msg_stream));

    Ok(response)
}
