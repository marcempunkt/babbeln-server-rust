use crate::ws::server::WebSocketConnection;
use serde::{Deserialize, Serialize};
use tokio::sync::oneshot;

/// Command (or SocketEvent) from Handle to Server
pub enum Command {
    Connect {
        user_id: usize,
        new_conn: WebSocketConnection,
    },
    Disconnect {
        conn: WebSocketConnection,
    },
    Message {
        socket_event: SocketEvent,
        conn: WebSocketConnection,
    },
    GetIsConnected {
        user_id: usize,
        sender: oneshot::Sender<bool>,
    },
}

/// Socket Event from Client to Server
/// Definition of how a socket event emitted as text from client to server should look like.
/// As json object will be something like this:
/// {
///    "action": "SendMessage",
///    "payload": { from_user_id, to_user_id, reference_object_id }
/// }
///
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct SocketEvent {
    pub action: SocketAction,
    pub payload: SocketPayload,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Copy, Clone)]
pub enum SocketAction {
    // Users
    DeleteUser,
    UpdateUserStatus,
    UpdateUserName,
    UpdateUserAvatar,
    // Users - Friends
    RemoveFriend,
    // Users - BlockedUsers
    BlockUser,
    UnblockUser,
    // Friendrequests
    SendFriendrequest,
    RemoveFriendrequest,
    AcceptFriendrequest,
    DeclineFriendrequest,
    // Posts
    CreatePost,
    RemovePost,
    LikePost,
    UnlikePost,
    CommentPost,
    RemoveCommentPost,
    // Message
    SendMessage,
    UpdateMessage,
    UnsendMessage,
    RemoveMessage,
    DeleteChat,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Default)]
pub struct SocketPayload {
    pub from_user_id: Option<usize>,
    pub to_user_id: Option<usize>,
    pub reference_object_id: Option<usize>,
}

impl SocketEvent {
    pub fn into_event_wrapper(self) -> EventWrapper {
        match self.action {
            // Normal Event
            SocketAction::RemoveFriend => EventWrapper::Normal(self),
            SocketAction::SendFriendrequest => EventWrapper::Normal(self),
            SocketAction::RemoveFriendrequest => EventWrapper::Normal(self),
            SocketAction::AcceptFriendrequest => EventWrapper::Normal(self),
            SocketAction::DeclineFriendrequest => EventWrapper::Normal(self),
            SocketAction::SendMessage => EventWrapper::Normal(self),
            SocketAction::UpdateMessage => EventWrapper::Normal(self),
            SocketAction::UnsendMessage => EventWrapper::Normal(self),
            // Private Event
            SocketAction::BlockUser => EventWrapper::Private(self),
            SocketAction::UnblockUser => EventWrapper::Private(self),
            SocketAction::DeleteChat => EventWrapper::Private(self),
            SocketAction::RemoveMessage => EventWrapper::Private(self),
            // Public Event
            SocketAction::LikePost => EventWrapper::Public(self),
            SocketAction::UnlikePost => EventWrapper::Public(self),
            SocketAction::CommentPost => EventWrapper::Public(self),
            SocketAction::RemoveCommentPost => EventWrapper::Public(self),
            // Friend Circle Event
            SocketAction::DeleteUser => EventWrapper::FriendCircle(self),
            SocketAction::UpdateUserStatus => EventWrapper::FriendCircle(self),
            SocketAction::UpdateUserName => EventWrapper::FriendCircle(self),
            SocketAction::UpdateUserAvatar => EventWrapper::FriendCircle(self),
            SocketAction::CreatePost => EventWrapper::FriendCircle(self),
            SocketAction::RemovePost => EventWrapper::FriendCircle(self),
        }
    }
}

/// Event wrapper to determine to who the event should be emitted
/// There are 4 Types of SocketEvents:
/// - to all same clients (except sender) and friends   (friend circle event)
/// - from one user to another                          (normal event)
/// - to all same clients                               (private event)
/// - to all users that can see the reference_object_id (public event)
///
/// normal event:        { from_user_id, to_user_id, reference_object_id }
/// private event:       { from_user_id, reference_object_id }
/// public event:        { from_user_id, reference_object_id }
/// friend circle event: { from_user_id, reference_object_id }
pub enum EventWrapper {
    Normal(SocketEvent),
    Private(SocketEvent),
    Public(SocketEvent),
    FriendCircle(SocketEvent),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_deserialize_socket_event() {
        let socket_event = SocketEvent {
            action: SocketAction::SendMessage,
            payload: SocketPayload {
                from_user_id: Some(1),
                to_user_id: Some(2),
                reference_object_id: Some(3),
            },
        };

        let expected: &str =
            "{\"action\":\"SendMessage\",\"payload\":{\"from_user_id\":1,\"to_user_id\":2,\"reference_object_id\":3}}";

        assert_eq!(serde_json::to_string(&socket_event).unwrap(), expected);
    }

    #[test]
    fn test_serialize_socket_event() {
        let socket_event: &str =
            "{\"action\":\"SendMessage\",\"payload\":{\"from_user_id\":1,\"to_user_id\":2,\"reference_object_id\":3}}";

        let expected = SocketEvent {
            action: SocketAction::SendMessage,
            payload: SocketPayload {
                from_user_id: Some(1),
                to_user_id: Some(2),
                reference_object_id: Some(3),
            },
        };

        assert_eq!(
            serde_json::from_str::<SocketEvent>(socket_event).unwrap(),
            expected
        );
    }
}
