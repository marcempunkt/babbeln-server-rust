pub enum SuccessMessage {
    // Users
    Login,
    Register,
    DeletedUser,
    ChangedUsername,
    ChangedEmail,
    ChangedPassword,
    ChangedStatus,
    ChangedAvatar,
    UpdatedUser,
    UpdatedUserStatus,
    GotUser,
    // Tokens
    // Friends
    GotFriend,
    RemovedFriend,
    // Friendrequests
    SentFriendrequest,
    GotFriendrequest,
    AcceptedFriendrequest,
    DeclinedFriendrequest,
    DeletedFriendrequest,
    // Blocked Users
    GotBlockedUser,
    BlockedUser,
    UnblockedUser,
    // Messages
    GotMessages,
    SentMessage,
    GotMessage,
    UpdatedMessage,
    DeletedMessage,
    UnsentMessage,
    DeletedChat,
    // Posts
    SavedPost,
    GotPost,
    DeletedPost,
    LikedPost,
    UnlikedPost,
    CommentPost,
    DeletedPostComment,
}

impl ToString for SuccessMessage {
    fn to_string(&self) -> String {
        self.to_str().to_string()
    }
}

impl Into<String> for SuccessMessage {
    fn into(self) -> String {
        self.to_string()
    }
}

impl SuccessMessage {
    pub fn to_str(&self) -> &str {
        match self {
            // Users
            SuccessMessage::Login => "Successfully logged in.",
            SuccessMessage::Register => "Successfully created a new account",
            SuccessMessage::DeletedUser => "Your account has been deleted.",
            SuccessMessage::ChangedUsername => "Successfully changed your username.",
            SuccessMessage::ChangedEmail => "Successfully changed your email.",
            SuccessMessage::ChangedPassword => "Successfully changed your password.",
            SuccessMessage::ChangedStatus => "Changed online status.",
            SuccessMessage::ChangedAvatar => "Uploaded new profile picture.",
            SuccessMessage::UpdatedUser => "Your account has been updated.",
            SuccessMessage::UpdatedUserStatus => "Your online status has been updated.",
            SuccessMessage::GotUser => "Got user.",
            // Tokens
            // Friends
            SuccessMessage::GotFriend => "Got your friend.",
            SuccessMessage::RemovedFriend => "User has been removed from your friend list.",
            // Friendrequests
            SuccessMessage::SentFriendrequest => "Friendrequest has been sent.",
            SuccessMessage::GotFriendrequest => "Got friendrequest.",
            SuccessMessage::AcceptedFriendrequest => "You've accepeted the friendrequest.",
            SuccessMessage::DeclinedFriendrequest => "You've declined the friendrequest.",
            SuccessMessage::DeletedFriendrequest => "You've deleted the friendrequest.",
            // Blocked Users
            SuccessMessage::GotBlockedUser => "Got blocked user.",
            SuccessMessage::BlockedUser => "User has been blocked.",
            SuccessMessage::UnblockedUser => "User has been unblocked.",
            // Messages
            SuccessMessage::GotMessages => "Got all of your messages.",
            SuccessMessage::SentMessage => "Successfully sent a message.",
            SuccessMessage::GotMessage => "Got the message.",
            SuccessMessage::UpdatedMessage => "You've edited this message.",
            SuccessMessage::DeletedMessage => "You've deleted this message.",
            SuccessMessage::UnsentMessage => "You've unsended this message.",
            SuccessMessage::DeletedChat => "You've deleted the whole chat.",
            // Posts
            SuccessMessage::SavedPost => "Successfully posted a new post.",
            SuccessMessage::GotPost => "Got your post.",
            SuccessMessage::DeletedPost => "You've deleted this post.",
            SuccessMessage::LikedPost => "You've liked this post.",
            SuccessMessage::UnlikedPost => "You've unliked this post.",
            SuccessMessage::CommentPost => "You've commented this post.",
            SuccessMessage::DeletedPostComment => "You've deleted this comment.",
        }
    }
}
