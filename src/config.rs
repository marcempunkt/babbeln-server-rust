use crate::APPNAME;
use log::{debug, info, warn};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::fs;
use std::path::{Path, PathBuf};
use toml;
use uuid::Uuid;

#[derive(Serialize, Deserialize, Debug)]
pub struct ConfigToml {
    general: Option<ConfigTomlGeneral>,
    database: Option<ConfigTomlDatabase>,
    jwt: Option<ConfigTomlJwt>,
}

impl Default for ConfigToml {
    fn default() -> Self {
        Self {
            general: None,
            database: None,
            jwt: None,
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct ConfigTomlGeneral {
    domain: Option<String>,
    port: Option<usize>,
    upload_dir: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
struct ConfigTomlDatabase {
    host: Option<String>,
    port: Option<String>,
    user: Option<String>,
    password: Option<String>,
    database: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
struct ConfigTomlJwt {
    secret_token: Option<String>,
}

#[derive(Debug, Clone)]
pub struct Config {
    pub domain: String,
    pub port: usize,
    pub upload_dir: String,
    pub database_url: String,
    pub secret_token: String,
}

impl Config {
    /// Create a new instance of Config.
    /// It tries to read the config file that can be stored at the following places
    /// - "./langunion.toml"
    /// - "./Langunion.toml"
    /// - "~/.config/langunion/config.toml"
    /// - "~/.config/langunion/Config.toml"
    ///
    /// If there is no config file or the config file is not complete,
    /// 'sane' default values will be used instead respectively:
    ///
    /// ```rust
    /// Config {
    ///   database_url: "mysql://unknown:unknown@127.0.0.1:3306/unknown",
    ///   secret_token: "randomly_generated_uuid4",
    ///   is_dev: true,
    /// }
    /// ```
    ///
    /// Example of a config file:
    ///
    /// ```toml
    /// [general]
    /// domain="https://server.langunion.org"
    /// port=443
    /// upload_dir="~/.langunion/files/"
    ///
    /// [database]
    /// db_host="127.17.0.2"
    /// db_port="3306"
    /// db_user="langunion"
    /// db_password="langunionpassword"
    /// db_database="languniondb"
    ///
    /// [jwt]
    /// secret_token="abc"
    /// ```
    pub fn new() -> Self {
        let config_content: Option<String> = Self::get_config();
        let config_toml: ConfigToml = Self::convert_to_toml(config_content);
        let default: Config = Self::default();

        let (domain, port, upload_dir): (String, usize, String) = match config_toml.general {
            Some(general) => {
                let domain: String = general.domain.unwrap_or(default.domain);
                let port: usize = general.port.unwrap_or(default.port);
                let upload_dir: String = general.upload_dir.unwrap_or(default.upload_dir);
                (domain, port, upload_dir)
            }
            None => {
                warn!("Missing table 'general'");
                (default.domain, default.port, default.upload_dir)
            }
        };

        let database_url: String = match config_toml.database {
            Some(database) => {
                let host: String = database.host.unwrap_or("127.0.0.1".to_string());
                let port: String = database.port.unwrap_or("3306".to_string());
                let user: String = database.user.unwrap_or("unknown".to_string());
                let password: String = database.password.unwrap_or("unknown".to_owned());
                let db: String = database.database.unwrap_or("unknown".to_owned());
                format!("mysql://{user}:{password}@{host}:{port}/{db}")
            }
            None => {
                warn!("Missing table 'database'");
                default.database_url
            }
        };

        let secret_token: String = match config_toml.jwt {
            Some(jwt) => jwt.secret_token.unwrap_or(default.secret_token),
            None => {
                warn!("Missing table 'jwt'.");
                default.secret_token
            }
        };

        let config = Config {
            domain,
            port,
            upload_dir,
            database_url,
            secret_token,
        };

        config.create_dirs();

        config
    }

    /// Create a test config and the test upload directory
    #[cfg(test)]
    pub fn new_test() -> Self {
        let config = Self {
            // "./.tmp_langunion/files/"
            upload_dir: Path::new(".")
                .join(".test_langunion")
                // generate a random sub folder so the test won't interfere with each other
                .join(Uuid::new_v4().to_string())
                .join("files")
                .to_str()
                .unwrap()
                .to_string(),
            secret_token: "shhh!".to_string(),
            ..Default::default()
        };

        config.create_dirs();

        config
    }

    /// Read the config file in one of the many directories it can be stored
    fn get_config() -> Option<String> {
        let paths: [PathBuf; 3] = [
            // "./{APPNAME}.toml"
            PathBuf::from(Path::new(".").join(&format!("{APPNAME}.toml"))),
            // "~/.{APPNAME}/config.toml"
            PathBuf::from(
                Path::new(&dirs::home_dir().unwrap())
                    .join(&format!(".{APPNAME}"))
                    .join("config.toml"),
            ),
            // "~/.config/{APPNAME}/config.toml"
            PathBuf::from(
                Path::new(&dirs::home_dir().unwrap())
                    .join(".config")
                    .join(APPNAME)
                    .join("config.toml"),
            ),
        ];

        for path in paths {
            if let Ok(content) = fs::read_to_string(&path) {
                debug!("Config file read from {:?}", path);
                let content: String = Self::untildify(&content);
                return Some(content);
            }
        }

        warn!("No config file was found");
        None
    }

    /// Convert the read (raw) string of the config file and convert it into ConfigToml
    fn convert_to_toml(content: Option<String>) -> ConfigToml {
        let content: String = content.unwrap_or(String::new());
        toml::from_str(&content).unwrap_or(ConfigToml::default())
    }

    /// Replace ~/ with the path of the home dir
    fn untildify(content: impl Into<String>) -> String {
        let content: String = content.into();
        let regex = Regex::new("=(\\s*)?(\"|')~\\/").unwrap();
        let home: String = dirs::home_dir().unwrap().to_str().unwrap().to_string();
        let new: String = format!("=\"{}/", &home);
        regex.replace_all(&content, new).to_owned().to_string()
    }

    /// Create the directory structure of the upload directory
    /// otherwise trying to store files will panic!
    fn create_dirs(&self) {
        let avatar_path = Path::new(&self.upload_dir).join("avatar");
        let posts_path = Path::new(&self.upload_dir).join("posts");

        if !Path::new(&self.upload_dir).exists() {
            fs::create_dir_all(&self.upload_dir).expect("Couldn't create upload directory");
            info!("Upload directory created");
            fs::create_dir_all(&avatar_path)
                .expect("Couldn't create avatar directory inside 'upload'");
            info!("Avatar directory inside upload created");
            fs::create_dir_all(&posts_path)
                .expect("Couldn't create posts directory inside 'upload'");
            info!("Posts directory inside upload created");
        }
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            domain: "http://localhost:4000".to_string(),
            port: 4000,
            // "~/.langungion/files/"
            upload_dir: Path::new(&dirs::home_dir().unwrap())
                .join(".langunion")
                .join("files")
                .to_str()
                .unwrap()
                .to_string(),
            database_url: "mysql://unknown:unknown@127.0.0.1:3306/unknown".to_string(),
            secret_token: Uuid::new_v4().to_string(),
        }
    }
}
