//! # Langunion-server
//! > Backend server for the whole langunion project
//! > Licensed under `GNU GPLv3`
//! Made with ♥ by me (Marc Mäurer).
//!
//! The goal is to create a service in which everyone can learn languages
//! and connect with other people
use actix_cors::Cors;
use actix_web::{get, middleware::Logger, web, App, HttpResponse, HttpServer};
use log::info;
use serde::{Deserialize, Serialize};
use sqlx::mysql::MySqlPoolOptions;

pub mod config;
pub mod dbclient;
pub mod error;
pub mod extractors;
pub mod models;
pub mod scopes;
pub mod success;
pub mod utils;
pub mod ws;

use crate::config::Config;
use crate::dbclient::DbClient;
use crate::error::HttpError;
use crate::ws::server::{WebSocketServer, WebSocketServerHandle};

pub static APPNAME: &str = "langunion";

/// Struct of the globally available app state
pub struct AppState {
    pub config: Config,
    pub dbclient: DbClient,
    pub websocket_server_handle: WebSocketServerHandle,
}

/// Start the langunion server
#[actix_web::main]
async fn main() {
    env_logger::Builder::from_env(
        env_logger::Env::default().filter_or("LANGUNION_LOG_LEVEL", "debug"),
    )
    .init();

    let config: Config = Config::new();
    let port: u16 = config.port as u16;
    let domain: &str = &config.domain;

    info!("{}-server is running on {} ♥", APPNAME, domain);

    // TODO insert debug into db
    let dbclient: DbClient = DbClient::new(
        MySqlPoolOptions::new()
            .max_connections(10)
            .connect(&config.database_url)
            .await
            .unwrap(),
    );
    let (websocket_server, websocket_server_handle): (WebSocketServer, WebSocketServerHandle) =
        WebSocketServer::new(dbclient.clone());

    WebSocketServer::run(websocket_server);

    let app_state: web::Data<AppState> = web::Data::new(AppState {
        config,
        dbclient,
        websocket_server_handle,
    });

    HttpServer::new(move || {
        App::new()
            .wrap(Cors::permissive())
            .wrap(Logger::default())
            .app_data(app_state.clone())
            .service(ws::scope::socket_scope()) // TODO asyncapi spec
            .service(scopes::users::users_scope())
            .service(scopes::token::token_scope())
            .service(scopes::friendrequests::friendrequests_scope())
            .service(scopes::messages::messages_scope())
            .service(scopes::posts::posts_scope())
            .service(root)
            .default_service(web::to(not_found))
    })
    .bind(("127.0.0.1", port))
    .unwrap()
    .run()
    .await
    .unwrap();
}

#[derive(Serialize, Deserialize)]
struct RootResponse {
    message: String,
}

#[get("/")]
async fn root() -> HttpResponse {
    HttpResponse::Ok().json(RootResponse {
        message: format!("{} Server is up and running ♥", APPNAME),
    })
}

async fn not_found() -> HttpResponse {
    HttpResponse::NotFound().json(HttpError::not_found())
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{body::MessageBody, http, test, web::Bytes, App};

    #[actix_web::test]
    async fn test_start_server() {
        let app = test::init_service(App::new().service(root)).await;
        let req = test::TestRequest::default().to_request();
        let resp = test::call_service(&app, req).await;
        assert!(resp.status().is_success());
    }

    #[actix_web::test]
    async fn test_root_returns_json() {
        let app = test::init_service(App::new().service(root)).await;
        let req = test::TestRequest::default().to_request();
        let resp = test::call_service(&app, req).await;
        let body: Bytes = resp.into_body().try_into_bytes().unwrap();
        let expected_json: String = serde_json::to_string(&RootResponse {
            message: format!("{} Server is up and running ♥", APPNAME),
        })
        .unwrap();
        assert_eq!(body, expected_json);
    }

    #[actix_web::test]
    async fn test_not_found_returns_json() {
        let app = test::init_service(App::new().default_service(web::to(not_found))).await;
        let req = test::TestRequest::default().uri("/asdf1234").to_request();
        let resp = test::call_service(&app, req).await;

        assert_eq!(resp.status(), http::StatusCode::NOT_FOUND);

        let body: Bytes = resp.into_body().try_into_bytes().unwrap();
        let expected_json: String = serde_json::to_string(&HttpError::not_found()).unwrap();

        assert_eq!(body, expected_json);
    }
}
