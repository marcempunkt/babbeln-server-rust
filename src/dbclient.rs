use crate::error::{ErrorMessage, HttpError};
use crate::models::{
    DbBlockedUser, DbFriend, DbFriendrequest, DbMessage, DbPost, DbPostComment, DbPostLike,
    DbToken, DbUser, Status,
};
use crate::utils::password_utils;
use async_trait::async_trait;
use chrono::Utc;
use log::{debug, error};
use serde::{Deserialize, Serialize};
use sqlx::mysql::MySqlPool;
use std::marker::Send;
use validator::Validate;

#[derive(Debug, Clone)]
pub struct DbClient {
    pool: MySqlPool,
}

impl DbClient {
    pub fn new(pool: MySqlPool) -> Self {
        debug!("Created new dbclient");
        DbClient { pool }
    }
}

#[async_trait]
pub trait UserExt {
    async fn get_user(&self, user_id: usize) -> Option<DbUser>;
    async fn get_user_by_username(&self, username: impl Into<String> + Send) -> Option<DbUser>;
    async fn get_user_by_email(&self, username: impl Into<String> + Send) -> Option<DbUser>;
    async fn get_users(&self, query: impl Into<String> + Send) -> Vec<DbUser>;
    async fn save_user<T: Into<String> + Send>(
        &self,
        username: T,
        email: T,
        password: T,
    ) -> Result<(), HttpError>;
    async fn delete_user(&self, user_id: usize) -> Result<(), HttpError>;
    async fn update_user<T: Into<String> + Send>(
        &self,
        user_id: usize,
        username: Option<T>,
        password: Option<T>,
        email: Option<T>,
        status: Option<Status>,
    ) -> Result<(), HttpError>;
}

#[async_trait]
pub trait TokenExt {
    async fn get_refresh_tokens(&self, user_id: usize) -> Vec<String>;
    async fn get_refresh_token(&self, token: impl Into<String> + Send) -> Option<String>;
    async fn save_refresh_token(
        &self,
        user_id: usize,
        token: impl Into<String> + Send,
    ) -> Result<(), HttpError>;
    async fn delete_refresh_tokens(&self, user_id: usize) -> Result<(), HttpError>;
    async fn delete_refresh_token(&self, token: impl Into<String> + Send) -> Result<(), HttpError>;
}

#[async_trait]
pub trait FriendExt {
    async fn get_friend(&self, user_id_one: usize, user_id_two: usize) -> Option<DbFriend>;
    async fn get_friends(&self, user_id: usize) -> Vec<DbFriend>;
    async fn save_friend(&self, user_id_one: usize, user_id_two: usize) -> Result<(), HttpError>;
    async fn delete_friend(&self, user_id_one: usize, user_id_two: usize) -> Result<(), HttpError>;
    async fn delete_friends(&self, user_id: usize) -> Result<(), HttpError>;
}

#[async_trait]
pub trait FriendrequestExt {
    async fn get_friendrequest(&self, user_id: usize, friend_id: usize) -> Option<DbFriendrequest>;
    async fn get_friendrequests(&self, user_id: usize) -> Vec<DbFriendrequest>;
    async fn get_friendrequest_by_id(&self, req_id: usize) -> Option<DbFriendrequest>;
    async fn save_friendrequest(&self, user_id: usize, friend_id: usize) -> Result<(), HttpError>;
    async fn delete_friendrequest(&self, req_id: usize) -> Result<(), HttpError>;
    async fn delete_friendrequests(&self, user_id: usize) -> Result<(), HttpError>;
}

#[async_trait]
pub trait BlockedUserExt {
    async fn get_blocked_user(&self, user_id: usize, friend_id: usize) -> Option<DbBlockedUser>;
    async fn get_blocked_users(&self, user_id: usize) -> Vec<DbBlockedUser>;
    async fn block_user(&self, user_id: usize, friend_id: usize) -> Result<(), HttpError>;
    async fn unblock_user(&self, user_id: usize, friend_id: usize) -> Result<(), HttpError>;
    async fn unblock_users(&self, user_id: usize) -> Result<(), HttpError>;
}

#[async_trait]
pub trait PostExt {
    async fn get_post(&self, post_id: usize) -> Option<DbPost>;
    async fn get_posts(&self, user_id: usize) -> Vec<DbPost>;
    async fn save_post(
        &self,
        author: usize,
        content: impl Into<String> + Send,
        image_count: usize,
    ) -> Result<DbPost, HttpError>;
    async fn delete_post(&self, post_id: usize) -> Result<(), HttpError>;
    async fn delete_posts(&self, user_id: usize) -> Result<(), HttpError>;
    async fn get_post_likes(&self, post_id: usize) -> Vec<DbPostLike>;
    async fn delete_post_likes(&self, post_id: usize) -> Result<(), HttpError>;
    async fn like_post(&self, post_id: usize, user_id: usize) -> Result<(), HttpError>;
    async fn unlike_post(&self, post_id: usize, user_id: usize) -> Result<(), HttpError>;
    async fn get_post_comment(&self, comment_id: usize) -> Option<DbPostComment>;
    async fn get_post_comments(&self, post_id: usize) -> Vec<DbPostComment>;
    async fn save_post_comment(
        &self,
        post_id: usize,
        user_id: usize,
        content: impl Into<String> + Send,
    ) -> Result<(), HttpError>;
    async fn delete_post_comment(&self, comment_id: usize) -> Result<(), HttpError>;
    async fn delete_post_comments(&self, post_id: usize) -> Result<(), HttpError>;
}

#[async_trait]
pub trait MessageExt {
    async fn get_message(&self, message_id: usize) -> Option<DbMessage>;
    async fn get_messages(&self, user_id: usize) -> Vec<DbMessage>;
    async fn save_message(
        &self,
        from_user: usize,
        to_user: usize,
        content: impl Into<String> + Send,
    ) -> Result<DbMessage, HttpError>;
    async fn update_message(
        &self,
        message_id: usize,
        new_content: impl Into<String> + Send,
    ) -> Result<(), HttpError>;
    async fn delete_message_for_sender(&self, message_id: usize) -> Result<(), HttpError>;
    async fn delete_message_for_receiver(&self, message_id: usize) -> Result<(), HttpError>;
    async fn delete_message(&self, message_id: usize) -> Result<(), HttpError>;
    async fn delete_messages(&self, user_id: usize) -> Result<(), HttpError>;
    async fn get_chat(&self, user_id: usize, user_id_of_friend: usize) -> Vec<DbMessage>;
    async fn delete_chat(&self, user_id: usize, user_id_of_friend: usize) -> Result<(), HttpError>;
}

#[async_trait]
impl UserExt for DbClient {
    async fn get_user(&self, user_id: usize) -> Option<DbUser> {
        debug!("Get user with the id: {user_id}");
        sqlx::query_as!(DbUser, "SELECT * FROM users WHERE id = ?", user_id as u64,)
            .fetch_optional(&self.pool)
            .await
            .unwrap_or(None)
    }

    async fn get_user_by_username(&self, username: impl Into<String> + Send) -> Option<DbUser> {
        let username: String = username.into();
        debug!("Get user with the username: {username}");
        sqlx::query_as!(DbUser, "SELECT * FROM users WHERE username = ?", username,)
            .fetch_optional(&self.pool)
            .await
            .unwrap_or(None)
    }

    async fn get_user_by_email(&self, email: impl Into<String> + Send) -> Option<DbUser> {
        let email: String = email.into();
        debug!("Get user with the email: {email}");
        sqlx::query_as!(DbUser, "SELECT * FROM users WHERE email = ?", email,)
            .fetch_optional(&self.pool)
            .await
            .unwrap_or(None)
    }

    async fn get_users(&self, query: impl Into<String> + Send) -> Vec<DbUser> {
        let query: String = query.into();
        debug!("Get all the users that match the query: {query}");
        let query_fuzzy: String = format!("%{}%", &query);
        sqlx::query_as!(
            DbUser,
            "SELECT * FROM users WHERE (username LIKE ?) OR (email = ?) \
             ORDER BY username ASC",
            &query_fuzzy,
            &query,
        )
        .fetch_all(&self.pool)
        .await
        .unwrap_or(Vec::new())
    }

    async fn save_user<T: Into<String> + Send>(
        &self,
        username: T,
        email: T,
        password: T,
    ) -> Result<(), HttpError> {
        #[derive(Serialize, Deserialize, Validate)]
        struct NewUser {
            #[validate(length(min = 4, max = 255))]
            username: String,
            #[validate(email)]
            email: String,
            #[validate(length(min = 8, max = 255))]
            password: String,
        }

        let new_user: NewUser = NewUser {
            username: username.into(),
            email: email.into(),
            password: password.into(),
        };

        debug!(
            "Insert a new user with the username: {} and email: {}",
            new_user.username, new_user.email
        );

        new_user
            .validate()
            .map_err(|e| HttpError::bad_request(e.to_string()))?;

        let is_email_taken: bool = self.get_user_by_email(&new_user.email).await.is_some();
        let is_username_taken: bool = self
            .get_user_by_username(&new_user.username)
            .await
            .is_some();
        if is_email_taken {
            return Err(HttpError::bad_request(ErrorMessage::EmailExist));
        }
        if is_username_taken {
            return Err(HttpError::bad_request(ErrorMessage::UsernameExist));
        }

        let hashed_password: String = password_utils::hash(&new_user.password);

        sqlx::query!(
            "INSERT INTO users(username, email, password) VALUES (?, ?, ?)",
            new_user.username,
            new_user.email,
            hashed_password,
        )
        .execute(&self.pool)
        .await
        .map_err(|_| {
            error!("Could not create a new user");
            HttpError::server_error()
        })?;

        Ok(())
    }

    async fn delete_user(&self, user_id: usize) -> Result<(), HttpError> {
        debug!("Delete user with the id: {user_id}");
        match sqlx::query!("DELETE FROM users WHERE id = ?", user_id as u64,)
            .execute(&self.pool)
            .await
        {
            Ok(_) => Ok(()),
            Err(_) => {
                error!("Could not delete the user with the id {user_id}");
                Err(HttpError::server_error())
            }
        }
    }

    async fn update_user<T: Into<String> + Send>(
        &self,
        user_id: usize,
        username: Option<T>,
        password: Option<T>,
        email: Option<T>,
        status: Option<Status>,
    ) -> Result<(), HttpError> {
        /* Update username */
        if let Some(username) = username {
            debug!("Update username of user with the id: {user_id}");
            let username: String = username.into();
            let is_username_taken: bool = self.get_user_by_username(&username).await.is_some();
            if is_username_taken {
                return Err(HttpError::bad_request(ErrorMessage::UsernameExist));
            }

            if let Err(_) = sqlx::query!(
                "UPDATE users SET username = ? WHERE id = ?",
                username,
                user_id as u64,
            )
            .execute(&self.pool)
            .await
            {
                error!("Could not update the username of the user with the id: {user_id}");
                return Err(HttpError::server_error());
            }
        }
        /* Update password */
        if let Some(password) = password {
            debug!("Update password of user with the id: {user_id}");
            let hashed: String = password_utils::hash(password);
            if let Err(_) = sqlx::query!(
                "UPDATE users SET password = ? WHERE id = ?",
                hashed,
                user_id as u64,
            )
            .execute(&self.pool)
            .await
            {
                error!("Could not update the password of the user with the id: {user_id}");
                return Err(HttpError::server_error());
            }
        }
        /* Update email */
        if let Some(email) = email {
            debug!("Update email of user with the id: {user_id}");
            let email: String = email.into();
            let is_email_taken: bool = self.get_user_by_email(&email).await.is_some();
            if is_email_taken {
                return Err(HttpError::bad_request(ErrorMessage::EmailExist));
            }

            if let Err(_) = sqlx::query!(
                "UPDATE users SET email = ? WHERE id = ?",
                email,
                user_id as u64,
            )
            .execute(&self.pool)
            .await
            {
                error!("Could not update the email of the user with the id: {user_id}");
                return Err(HttpError::server_error());
            }
        }
        /* Update online status */
        if let Some(status) = status {
            debug!("Update online status of user with the id: {user_id}");
            let new_status: String = status.to_string();
            if let Err(_) = sqlx::query!(
                "UPDATE users SET status = ? WHERE id = ?",
                &new_status,
                user_id as u64,
            )
            .execute(&self.pool)
            .await
            {
                error!("Could not update the online status of the user with the id: {user_id}");
                return Err(HttpError::server_error());
            }
        }

        Ok(())
    }
}

#[async_trait]
impl TokenExt for DbClient {
    async fn get_refresh_tokens(&self, user_id: usize) -> Vec<String> {
        debug!("Get refresh tokens of user with the id: {user_id}");
        let tokens: sqlx::Result<Vec<DbToken>> = sqlx::query_as!(
            DbToken,
            "SELECT * FROM tokens WHERE user_id = ?",
            user_id as u64,
        )
        .fetch_all(&self.pool)
        .await;

        match tokens {
            Ok(tokens) => tokens
                .into_iter()
                .map(|t: DbToken| t.refresh)
                .collect::<Vec<String>>(),
            Err(_) => Vec::new(),
        }
    }

    async fn get_refresh_token(&self, token: impl Into<String> + Send) -> Option<String> {
        debug!("Get refresh token");
        let token: Option<DbToken> = sqlx::query_as!(
            DbToken,
            "SELECT * FROM tokens WHERE refresh = ?",
            token.into(),
        )
        .fetch_optional(&self.pool)
        .await
        .unwrap_or(None);

        match token {
            Some(token) => Some(token.refresh),
            None => None,
        }
    }

    async fn save_refresh_token(
        &self,
        user_id: usize,
        token: impl Into<String> + Send,
    ) -> Result<(), HttpError> {
        debug!("Save a refresh token for user with the id: {user_id}");
        match sqlx::query!(
            "INSERT INTO tokens (user_id, refresh) VALUE (?, ?)",
            user_id as u64,
            token.into(),
        )
        .execute(&self.pool)
        .await
        {
            Ok(_) => Ok(()),
            Err(_) => {
                error!("Could not save the refresh token for the user with the id: {user_id}");
                Err(HttpError::server_error())
            }
        }
    }

    async fn delete_refresh_tokens(&self, user_id: usize) -> Result<(), HttpError> {
        debug!("Delete refresh tokens of user with the id: {user_id}");
        match sqlx::query!("DELETE FROM tokens WHERE user_id = ?", user_id as u64,)
            .execute(&self.pool)
            .await
        {
            Ok(_) => Ok(()),
            Err(_) => {
                error!("Could not delete the refresh token for the user with the id: {user_id}");
                Err(HttpError::server_error())
            }
        }
    }

    async fn delete_refresh_token(&self, token: impl Into<String> + Send) -> Result<(), HttpError> {
        debug!("Delete a refresh token");
        match sqlx::query!("DELETE FROM tokens WHERE refresh = ?", token.into(),)
            .execute(&self.pool)
            .await
        {
            Ok(_) => Ok(()),
            Err(_) => {
                debug!("Could not delete the refresh token");
                Err(HttpError::server_error())
            }
        }
    }
}

#[async_trait]
impl FriendExt for DbClient {
    async fn get_friend(&self, user_id_one: usize, user_id_two: usize) -> Option<DbFriend> {
        debug!("Get friend with the first user: {user_id_one} and the second user: {user_id_two}");
        let friend: Option<DbFriend> = sqlx::query_as!(
            DbFriend,
            "SELECT * FROM friends \
             WHERE (user_one = ? AND user_two = ?) \
             OR (user_one = ? AND user_two = ?)",
            user_id_one as u64,
            user_id_two as u64,
            user_id_two as u64,
            user_id_one as u64,
        )
        .fetch_optional(&self.pool)
        .await
        .unwrap_or(None);

        friend
    }

    async fn get_friends(&self, user_id: usize) -> Vec<DbFriend> {
        debug!("Get friends of the user with the id: {user_id}");
        let friends: Vec<DbFriend> = sqlx::query_as!(
            DbFriend,
            "SELECT * FROM friends \
             WHERE user_one = ? OR user_two = ?",
            user_id as u64,
            user_id as u64,
        )
        .fetch_all(&self.pool)
        .await
        .unwrap_or(Vec::new());

        friends
    }

    async fn save_friend(&self, user_id_one: usize, user_id_two: usize) -> Result<(), HttpError> {
        debug!(
            "Insert friend with the first user: {user_id_one} and the second user: {user_id_two}"
        );
        if let Err(_) = sqlx::query!(
            "INSERT INTO friends (user_one, user_two) VALUES (?, ?)",
            user_id_one as u64,
            user_id_two as u64,
        )
        .execute(&self.pool)
        .await
        {
            error!("Could not save the friend with the first user: {user_id_one} and the second user: {user_id_two}");
            return Err(HttpError::server_error());
        }

        Ok(())
    }

    async fn delete_friend(&self, user_id_one: usize, user_id_two: usize) -> Result<(), HttpError> {
        debug!(
            "Delete friend with the first user: {user_id_one} and the second user: {user_id_two}"
        );
        if let Err(_) = sqlx::query!(
            "DELETE FROM friends \
             WHERE (user_one = ? AND user_two = ?) \
             OR (user_one = ? AND user_two = ?)",
            user_id_one as u64,
            user_id_two as u64,
            user_id_two as u64,
            user_id_one as u64,
        )
        .execute(&self.pool)
        .await
        {
            error!("Could not delete the friend with the first user: {user_id_one} and the second user: {user_id_two}");
            return Err(HttpError::server_error());
        }

        Ok(())
    }

    async fn delete_friends(&self, user_id: usize) -> Result<(), HttpError> {
        debug!("Delete all friends of user with the id: {user_id}");
        if let Err(_) = sqlx::query!(
            "DELETE FROM friends WHERE user_one = ? OR user_two = ?",
            user_id as u64,
            user_id as u64,
        )
        .execute(&self.pool)
        .await
        {
            error!("Could not delete friends of the user with the id: {user_id}");
            return Err(HttpError::server_error());
        }

        Ok(())
    }
}

#[async_trait]
impl FriendrequestExt for DbClient {
    async fn get_friendrequest(&self, user_id: usize, friend_id: usize) -> Option<DbFriendrequest> {
        debug!("Get friendrequest with user_id: {user_id} and the friend_id: {friend_id}");
        let friendrequest: Option<DbFriendrequest> = sqlx::query_as!(
            DbFriendrequest,
            "SELECT * FROM friendrequests \
             WHERE (from_user = ? AND to_user = ?) \
             OR (from_user = ? AND to_user = ?)",
            user_id as u64,
            friend_id as u64,
            friend_id as u64,
            user_id as u64,
        )
        .fetch_optional(&self.pool)
        .await
        .unwrap_or(None);

        friendrequest
    }

    async fn get_friendrequests(&self, user_id: usize) -> Vec<DbFriendrequest> {
        debug!("Get all the friendrequests of the user with the id: {user_id}");
        let friendrequests: Vec<DbFriendrequest> = sqlx::query_as!(
            DbFriendrequest,
            "SELECT * FROM friendrequests \
             WHERE (from_user = ? OR to_user = ?) \
             ORDER BY id ASC",
            user_id as u64,
            user_id as u64,
        )
        .fetch_all(&self.pool)
        .await
        .unwrap_or(Vec::new());

        friendrequests
    }

    async fn get_friendrequest_by_id(&self, req_id: usize) -> Option<DbFriendrequest> {
        debug!("Get friendrequest with the id: {req_id}");
        let friendrequest: Option<DbFriendrequest> = sqlx::query_as!(
            DbFriendrequest,
            "SELECT * FROM friendrequests WHERE id = ?",
            req_id as u64,
        )
        .fetch_optional(&self.pool)
        .await
        .unwrap_or(None);

        friendrequest
    }

    async fn save_friendrequest(&self, user_id: usize, friend_id: usize) -> Result<(), HttpError> {
        debug!("Save friendrequest of user with the id: {user_id} and the friend with the id: {friend_id}");
        // check if the users exist
        let user: Option<DbUser> = self.get_user(user_id).await;
        let friend: Option<DbUser> = self.get_user(friend_id).await;
        if user.is_none() || friend.is_none() {
            return Err(HttpError::bad_request(ErrorMessage::NoUserFound));
        }
        // Check if they're not already friends in save_friendrequest
        let friend: Option<DbFriend> = self.get_friend(user_id, friend_id).await;
        if friend.is_some() {
            return Err(HttpError::bad_request(ErrorMessage::AlreadyFriends));
        }
        // check if there is already a pending friendrequest
        let friendrequest: Option<DbFriendrequest> =
            self.get_friendrequest(user_id, friend_id).await;
        if friendrequest.is_some() {
            return Err(HttpError::bad_request(
                ErrorMessage::AlreadyPendingFriendrequest,
            ));
        }
        if let Err(_) = sqlx::query!(
            "INSERT INTO friendrequests (from_user, to_user) VALUES(?, ?)",
            user_id as u64,
            friend_id as u64,
        )
        .execute(&self.pool)
        .await
        {
            error!("Could not save friendrequest of user with the id: {user_id} and the other user with the id: {friend_id}");
            return Err(HttpError::server_error());
        }

        Ok(())
    }

    async fn delete_friendrequest(&self, req_id: usize) -> Result<(), HttpError> {
        debug!("Delete friendrequest with the id: {req_id}");
        if let Err(_) = sqlx::query!("DELETE FROM friendrequests WHERE id = ?", req_id as u64,)
            .execute(&self.pool)
            .await
        {
            error!("Could not delete friendrequest with the id: {req_id}");
            return Err(HttpError::server_error());
        }

        Ok(())
    }

    async fn delete_friendrequests(&self, user_id: usize) -> Result<(), HttpError> {
        debug!("Delete all the friendrequests of the user with the id: {user_id}");
        if let Err(_) = sqlx::query!(
            "DELETE FROM friendrequests WHERE from_user = ? OR to_user = ?",
            user_id as u64,
            user_id as u64,
        )
        .execute(&self.pool)
        .await
        {
            error!("Could not delete all the friendrequests of the user with the id: {user_id}");
            return Err(HttpError::server_error());
        }

        Ok(())
    }
}

#[async_trait]
impl BlockedUserExt for DbClient {
    async fn get_blocked_user(&self, user_id: usize, friend_id: usize) -> Option<DbBlockedUser> {
        debug!("Get blocked user with the user_id: {user_id} and the friend_id: {friend_id}");
        let blocked_user: Option<DbBlockedUser> = sqlx::query_as!(
            DbBlockedUser,
            "SELECT * FROM blocked_users \
             WHERE (user_id = ? AND blocked_user_id = ?)",
            user_id as u64,
            friend_id as u64,
        )
        .fetch_optional(&self.pool)
        .await
        .unwrap_or(None);

        blocked_user
    }

    async fn get_blocked_users(&self, user_id: usize) -> Vec<DbBlockedUser> {
        debug!("Get all the blocked users of the user with the id: {user_id}");
        let blocked_users: Vec<DbBlockedUser> = sqlx::query_as!(
            DbBlockedUser,
            "SELECT * FROM blocked_users \
             WHERE (user_id = ?)",
            user_id as u64,
        )
        .fetch_all(&self.pool)
        .await
        .unwrap_or(Vec::new());

        blocked_users
    }

    async fn block_user(&self, user_id: usize, friend_id: usize) -> Result<(), HttpError> {
        debug!("Block user: {friend_id} by user: {user_id}");
        // Check if the user is already blocked
        if let Ok(_) = sqlx::query_as!(
            DbBlockedUser,
            "SELECT * FROM blocked_users WHERE (user_id = ? AND blocked_user_id = ?)",
            user_id as u64,
            friend_id as u64,
        )
        .fetch_one(&self.pool)
        .await
        {
            return Err(HttpError::bad_request(ErrorMessage::UserAlreadyBlocked));
        }

        // Block the user
        sqlx::query!(
            "INSERT INTO blocked_users (user_id, blocked_user_id) VALUES (?, ?)",
            user_id as u64,
            friend_id as u64,
        )
        .execute(&self.pool)
        .await
        .map_err(|_| {
            error!("Could not block user: {friend_id} by user: {user_id}");
            HttpError::server_error()
        })?;

        Ok(())
    }

    async fn unblock_user(&self, user_id: usize, friend_id: usize) -> Result<(), HttpError> {
        debug!("Unblock user: {friend_id} by user: {user_id}");
        // Check if the user is blocked
        let blocked_user: DbBlockedUser = sqlx::query_as!(
            DbBlockedUser,
            "SELECT * FROM blocked_users WHERE (user_id = ? AND blocked_user_id = ?)",
            user_id as u64,
            friend_id as u64,
        )
        .fetch_one(&self.pool)
        .await
        .map_err(|_| HttpError::bad_request(ErrorMessage::UserIsntBlocked))?;

        sqlx::query!("DELETE FROM blocked_users WHERE (id = ?)", blocked_user.id,)
            .execute(&self.pool)
            .await
            .map_err(|_| {
                error!("Could not block user: {friend_id} by user: {user_id}");
                HttpError::server_error()
            })?;

        Ok(())
    }

    async fn unblock_users(&self, user_id: usize) -> Result<(), HttpError> {
        // TODO make this also transactional
        debug!("Unblock all users of user with the id: {user_id}");
        let blocked_users: Vec<DbBlockedUser> = self.get_blocked_users(user_id).await;
        for blocked_user in blocked_users {
            self.unblock_user(user_id, blocked_user.blocked_user_id as usize)
                .await?;
        }
        Ok(())
    }
}

#[async_trait]
impl PostExt for DbClient {
    async fn get_post(&self, post_id: usize) -> Option<DbPost> {
        debug!("Get post with the id: {post_id}");
        sqlx::query_as!(DbPost, "SELECT * FROM posts WHERE id = ?", post_id as u64,)
            .fetch_optional(&self.pool)
            .await
            .unwrap_or(None)
    }

    async fn get_posts(&self, user_id: usize) -> Vec<DbPost> {
        debug!("Get all the posts of the user with the id: {user_id}");
        // Get user ids of friends and user_id
        let mut user_ids: Vec<String> = self
            .get_friends(user_id)
            .await
            .into_iter()
            .map(|friend_item: DbFriend| {
                friend_item
                    .get_user_id_of_friend(user_id)
                    .unwrap()
                    .to_string()
            })
            .collect();
        user_ids.push(user_id.to_string());

        let query: String = format!(
            "SELECT * FROM posts WHERE author in ({}) \
             ORDER BY created_at DESC",
            user_ids.join(", ")
        );
        // FIXME when sqlx has added support for vec as bind
        // sqlx::query_as!(DbPost, "SELECT * FROM posts WHERE author in (?)", query)
        // but not high-priority, because the binding in this case is not user input
        // so we can be sure that the query is safe to execute
        sqlx::query_as(&query)
            .fetch_all(&self.pool)
            .await
            .unwrap_or(Vec::new())
    }

    async fn save_post(
        &self,
        user_id: usize,
        content: impl Into<String> + Send,
        image_count: usize,
    ) -> Result<DbPost, HttpError> {
        debug!("Insert post by user with the id: {user_id}");
        let content: String = content.into();

        if content.len() > 255 {
            return Err(HttpError::bad_request(ErrorMessage::PostTooLong));
        }

        let post_id: u64 = sqlx::query!(
            "INSERT INTO posts (author, content, image_count) VALUES (?, ?, ?)",
            user_id as u64,
            content,
            image_count as u64,
        )
        .execute(&self.pool)
        .await
        .map_err(|_| {
            error!("Could not insert post by user with the id: {user_id}");
            HttpError::server_error()
        })?
        .last_insert_id();

        let post: DbPost = sqlx::query_as!(DbPost, "SELECT * FROM posts WHERE id = ?", post_id)
            .fetch_one(&self.pool)
            .await
            .map_err(|_| {
                error!("Could not get the post that has been recently saved by user with the id: {user_id}");
                HttpError::server_error()
            })?;

        Ok(post)
    }

    async fn delete_post(&self, post_id: usize) -> Result<(), HttpError> {
        debug!("Delete post with the id: {post_id}");
        self.delete_post_likes(post_id).await?;
        self.delete_post_comments(post_id).await?;

        if sqlx::query!("DELETE FROM posts WHERE id = ?", post_id as u64)
            .execute(&self.pool)
            .await
            .map_err(|_| HttpError::server_error())?
            .rows_affected()
            == 0
        {
            return Err(HttpError::bad_request(ErrorMessage::NoPostFound));
        }

        Ok(())
    }

    async fn delete_posts(&self, user_id: usize) -> Result<(), HttpError> {
        debug!("Delete all the posts of the user with the id: {user_id}");
        // Delete my posts
        sqlx::query!("DELETE FROM posts WHERE author = ?", user_id as u64)
            .execute(&self.pool)
            .await
            .map_err(|_| {
                error!("Could not delete all the posts of the user with the id: {user_id}");
                HttpError::server_error()
            })?;

        // Delete my likes
        sqlx::query!("DELETE FROM post_likes WHERE user_id = ?", user_id as u64)
            .execute(&self.pool)
            .await
            .map_err(|_| {
                error!("Could not delete all the post_likes of the user with the id: {user_id}");
                HttpError::server_error()
            })?;

        // delete my comments
        sqlx::query!("DELETE FROM post_comments WHERE author = ?", user_id as u64)
            .execute(&self.pool)
            .await
            .map_err(|_| {
                error!("Could not delete all the post_comments of the user with the id: {user_id}");
                HttpError::server_error()
            })?;

        Ok(())
    }

    async fn get_post_likes(&self, post_id: usize) -> Vec<DbPostLike> {
        debug!("Get all the post likes of the post with the id: {post_id}");
        sqlx::query_as!(
            DbPostLike,
            "SELECT * FROM post_likes WHERE post_id = ?",
            post_id as u64
        )
        .fetch_all(&self.pool)
        .await
        .unwrap_or(Vec::new())
    }

    async fn delete_post_likes(&self, post_id: usize) -> Result<(), HttpError> {
        debug!("Delete all the post likes of the post with the id: {post_id}");
        sqlx::query!("DELETE FROM post_likes WHERE post_id = ?", post_id as u64)
            .execute(&self.pool)
            .await
            .map_err(|_| {
                error!("Could not delete all the post of the post with the id: {post_id}");
                HttpError::server_error()
            })?;
        Ok(())
    }

    async fn like_post(&self, post_id: usize, user_id: usize) -> Result<(), HttpError> {
        debug!("The user with the id: {user_id} likes a post with the id: {post_id}");
        self.get_post(post_id)
            .await
            .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))?;

        let liked: bool = self
            .get_post_likes(post_id)
            .await
            .into_iter()
            .map(|post_like: DbPostLike| post_like.user_id as usize)
            .collect::<Vec<usize>>()
            .contains(&user_id);

        if liked {
            return Err(HttpError::bad_request(ErrorMessage::CannotLikePostTwice));
        }

        sqlx::query!(
            "INSERT INTO post_likes (post_id, user_id) VALUES (?, ?)",
            post_id as u64,
            user_id as u64
        )
        .execute(&self.pool)
        .await
        .map_err(|_| {
            error!("The user with the id: {user_id} couldn't like the post with the id: {post_id}");
            HttpError::server_error()
        })?;
        Ok(())
    }

    async fn unlike_post(&self, post_id: usize, user_id: usize) -> Result<(), HttpError> {
        debug!("The user with the id: {user_id} unlikes a post with the id: {post_id}");
        self.get_post(post_id)
            .await
            .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))?;

        match sqlx::query!(
            "DELETE FROM post_likes WHERE post_id = ? and user_id = ?",
            post_id as u64,
            user_id as u64
        )
        .execute(&self.pool)
        .await
        .map_err(|_| HttpError::server_error())?
        .rows_affected()
        {
            0 => Err(HttpError::bad_request(ErrorMessage::CannotUnlikePostTwice)),
            1 | _ => Ok(()),
        }
    }

    async fn get_post_comment(&self, comment_id: usize) -> Option<DbPostComment> {
        debug!("Get a post_comment with the id: {comment_id}");
        sqlx::query_as!(
            DbPostComment,
            "SELECT * FROM post_comments WHERE id = ?",
            comment_id as u64,
        )
        .fetch_optional(&self.pool)
        .await
        .unwrap_or(None)
    }

    async fn get_post_comments(&self, post_id: usize) -> Vec<DbPostComment> {
        debug!("Get all the post_comments of the post with the id: {post_id}");
        sqlx::query_as!(
            DbPostComment,
            "SELECT * FROM post_comments WHERE post_id = ? \
             ORDER BY created_at DESC",
            post_id as u64
        )
        .fetch_all(&self.pool)
        .await
        .unwrap_or(Vec::new())
    }

    async fn save_post_comment(
        &self,
        post_id: usize,
        user_id: usize,
        content: impl Into<String> + Send,
    ) -> Result<(), HttpError> {
        debug!("Insert a new post_comment by the user: {user_id} to the post: {post_id}");
        let content: String = content.into();
        if content.len() > 255 {
            return Err(HttpError::bad_request(ErrorMessage::PostTooLong));
        }

        // check if post exists
        self.get_post(post_id)
            .await
            .ok_or(HttpError::bad_request(ErrorMessage::NoPostFound))?;

        sqlx::query!(
            "INSERT INTO post_comments (post_id, author, content) VALUES (?, ?, ?)",
            post_id as u64,
            user_id as u64,
            content,
        )
        .execute(&self.pool)
        .await
        .map_err(|_| {
            error!("Could not insert a new post comment to the post: {post_id}");
            HttpError::server_error()
        })?;
        Ok(())
    }

    async fn delete_post_comment(&self, comment_id: usize) -> Result<(), HttpError> {
        debug!("Delete post_comment with the id: {comment_id}");
        match sqlx::query!("DELETE FROM post_comments WHERE id = ?", comment_id as u64)
            .execute(&self.pool)
            .await
            .map_err(|_| HttpError::server_error())?
            .rows_affected()
        {
            0 => Err(HttpError::bad_request(ErrorMessage::NoPostCommentFound)),
            1 | _ => Ok(()),
        }
    }

    async fn delete_post_comments(&self, post_id: usize) -> Result<(), HttpError> {
        debug!("Delete all the post_comments of the post with the id: {post_id}");
        sqlx::query!(
            "DELETE FROM post_comments WHERE post_id = ?",
            post_id as u64
        )
        .execute(&self.pool)
        .await
        .map_err(|_| {
            error!("Could not delete all the post_comments of the post with the id: {post_id}");
            HttpError::server_error()
        })?;
        Ok(())
    }
}

#[async_trait]
impl MessageExt for DbClient {
    async fn get_message(&self, message_id: usize) -> Option<DbMessage> {
        debug!("Get a message with the id: {message_id}");
        let message: sqlx::Result<Option<DbMessage>> = sqlx::query_as!(
            DbMessage,
            "SELECT * FROM messages WHERE id = ?",
            message_id as u64,
        )
        .fetch_optional(&self.pool)
        .await;

        match message {
            Ok(message) => message,
            Err(_) => None,
        }
    }

    async fn get_messages(&self, user_id: usize) -> Vec<DbMessage> {
        debug!("Get all the messages of the user with the id: {user_id}");
        let messages: sqlx::Result<Vec<DbMessage>> = sqlx::query_as!(
            DbMessage,
            "SELECT * FROM messages \
             WHERE (sender = ? OR receiver = ?) \
             ORDER BY send_at DESC",
            user_id as u64,
            user_id as u64,
        )
        .fetch_all(&self.pool)
        .await;

        match messages {
            Ok(messages) => messages,
            Err(_) => Vec::new(),
        }
    }

    async fn save_message(
        &self,
        sender: usize,
        receiver: usize,
        content: impl Into<String> + Send,
    ) -> Result<DbMessage, HttpError> {
        debug!("Insert a message by the user with the id: {sender}");
        let content: String = content.into();
        if content.is_empty() {
            return Err(HttpError::bad_request(ErrorMessage::MessageHasNoContent));
        }

        let is_receiver_blocked: bool = self.get_blocked_user(sender, receiver).await.is_some();
        if is_receiver_blocked {
            return Err(HttpError::bad_request(ErrorMessage::YouHaveBlockedThisUser));
        }

        let are_friends: bool = self.get_friend(sender, receiver).await.is_some()
            || self.get_friend(receiver, sender).await.is_some()
            || sender == receiver;

        if !are_friends {
            return Err(HttpError::bad_request(ErrorMessage::NoFriendFound));
        }

        let message_id: u64 = sqlx::query!(
            "INSERT INTO messages \
             (sender, receiver, content) \
             VALUES (?, ?, ?)",
            sender as u64,
            receiver as u64,
            &content,
        )
        .execute(&self.pool)
        .await
        .map_err(|_| {
            error!("Could not insert a message by the user with the id: {sender}");
            HttpError::server_error()
        })?
        .last_insert_id();

        let message: DbMessage =
            sqlx::query_as!(DbMessage, "SELECT * FROM messages where id = ?", message_id)
                .fetch_one(&self.pool)
                .await
            .map_err(|_| {
                error!("Could not get a message that has been just inserted by the user with the id: {sender}");
                HttpError::server_error()
            })?;

        Ok(message)
    }

    async fn update_message(
        &self,
        message_id: usize,
        new_content: impl Into<String> + Send,
    ) -> Result<(), HttpError> {
        debug!("Update message with the id: {message_id}");
        let new_content: String = new_content.into();

        if new_content.is_empty() {
            return Err(HttpError::bad_request(ErrorMessage::MessageHasNoContent));
        }

        sqlx::query!("SELECT * FROM messages WHERE id = ?", message_id as u64)
            .fetch_one(&self.pool)
            .await
            .map_err(|_| HttpError::bad_request(ErrorMessage::NoMessageFound))?;

        sqlx::query!(
            "UPDATE messages SET content = ?, edited_at = ? WHERE id = ?",
            new_content,
            Utc::now(),
            message_id as u64,
        )
        .execute(&self.pool)
        .await
        .map_err(|_| {
            error!("Could not update the message with the id: {message_id}");
            HttpError::server_error()
        })?;

        Ok(())
    }

    async fn delete_message_for_sender(&self, message_id: usize) -> Result<(), HttpError> {
        debug!("Delete message with the id: {message_id} only for the sender");
        sqlx::query!(
            "UPDATE messages SET is_deleted_by_sender = true \
             WHERE id = ?",
            message_id as u64,
        )
        .execute(&self.pool)
        .await
        .map_err(|_| {
            error!("Could not delete the message with the id: {message_id} only for the sender");
            HttpError::server_error()
        })?;

        Ok(())
    }

    async fn delete_message_for_receiver(&self, message_id: usize) -> Result<(), HttpError> {
        debug!("Delete message with the id: {message_id} only for the receiver");
        sqlx::query!(
            "UPDATE messages SET is_deleted_by_receiver = true \
             WHERE id = ?",
            message_id as u64,
        )
        .execute(&self.pool)
        .await
        .map_err(|_| {
            error!("Could not delete the message with the id: {message_id} only for the receiver");
            HttpError::server_error()
        })?;

        Ok(())
    }

    async fn delete_message(&self, message_id: usize) -> Result<(), HttpError> {
        debug!("Delete the message with the message_id: {message_id}");
        sqlx::query!("DELETE FROM messages WHERE id = ?", message_id as u64,)
            .execute(&self.pool)
            .await
            .map_err(|_| {
                error!("Could not delete the message with the id: {message_id}");
                HttpError::server_error()
            })?;

        Ok(())
    }

    async fn delete_messages(&self, user_id: usize) -> Result<(), HttpError> {
        debug!("Delete all the message of the user with the id: {user_id}");
        let messages: Vec<DbMessage> = self.get_messages(user_id).await;
        for message in messages {
            self.delete_message(message.id as usize).await?;
        }

        Ok(())
    }

    async fn get_chat(&self, user_id: usize, user_id_of_friend: usize) -> Vec<DbMessage> {
        debug!("Get a whole chat of messages of the user: {user_id} with the user: {user_id_of_friend}");
        sqlx::query_as!(
            DbMessage,
            "SELECT * FROM messages \
             WHERE ((sender = ? AND receiver = ?) \
             OR (receiver= ? AND sender = ?)) \
             ORDER BY send_at DESC",
            user_id as u64,
            user_id_of_friend as u64,
            user_id as u64,
            user_id_of_friend as u64,
        )
        .fetch_all(&self.pool)
        .await
        .unwrap_or(Vec::new())
    }

    async fn delete_chat(&self, user_id: usize, user_id_of_friend: usize) -> Result<(), HttpError> {
        debug!("Delete a whole chat of messages of the user: {user_id} with the user: {user_id_of_friend}");
        let chat: Vec<DbMessage> = self.get_chat(user_id, user_id_of_friend).await;

        for message in chat {
            let is_user_sender: bool = user_id == message.sender as usize;
            let is_user_receiver: bool = user_id == message.receiver as usize;

            if is_user_sender {
                self.delete_message_for_sender(message.id as usize).await?;
            } else if is_user_receiver {
                self.delete_message_for_receiver(message.id as usize)
                    .await?;
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::config::Config;
    use crate::models::{ClientPost, IntoClientPostExt};
    use crate::utils::test_utils::{
        init_test_blocked_users, init_test_friendrequests, init_test_friends, init_test_messages,
        init_test_post_comments, init_test_post_likes, init_test_posts, init_test_users,
    };
    use chrono::{DateTime, Utc};
    use sqlx::mysql::MySqlPool;

    // ------------------------------------------------------------------------- Users
    #[sqlx::test]
    async fn test_get_user_by_id(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        let user: DbUser = dbclient.get_user(1).await.unwrap();
        assert_eq!(user.id, 1);
    }

    #[sqlx::test]
    async fn test_get_user_by_username(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        let user: DbUser = dbclient.get_user_by_username("marc").await.unwrap();
        assert_eq!(user.username, "marc");
    }

    #[sqlx::test]
    async fn test_get_user_by_email(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        let user: DbUser = dbclient.get_user_by_email("marc@mail.de").await.unwrap();
        assert_eq!(user.email, "marc@mail.de");
    }

    #[sqlx::test]
    async fn test_get_users(pool: MySqlPool) {
        let dbclient: DbClient = DbClient::new(pool);
        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("amarc", "amarc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("bmarc", "bmarc@mail.de", "1234asdf")
            .await
            .unwrap();
        let users: Vec<DbUser> = dbclient.get_users("marc").await;
        assert_eq!(users.len(), 3);
    }

    #[sqlx::test]
    async fn test_get_users_is_sorted_by_username_alphabetically(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient
            .save_user("a_user", "a@mail", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("b_user", "b@mail", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("c_user", "c@mail", "1234asdf")
            .await
            .unwrap();
        let users: Vec<DbUser> = dbclient.get_users("user").await;
        assert_eq!(users[0].username, "a_user");
        assert_eq!(users[1].username, "b_user");
        assert_eq!(users[2].username, "c_user");
    }

    #[sqlx::test]
    async fn test_get_users_email_has_to_exact_to_work(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        assert_eq!(dbclient.get_users("marc@web.de").await.len(), 0);
        assert_eq!(dbclient.get_users("marc@mail.de").await.len(), 1);
    }

    #[sqlx::test]
    async fn test_save_user(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        dbclient
            .save_user("test", "test@mail.de", "1234asdf")
            .await
            .unwrap();
        let user: DbUser = dbclient.get_user_by_username("test").await.unwrap();
        assert_eq!(user.username, "test");
        assert_eq!(user.email, "test@mail.de");
    }

    #[sqlx::test]
    async fn test_save_user_hashed_password(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        dbclient
            .save_user("test", "test@mail.de", "1234asdf")
            .await
            .unwrap();
        let user: DbUser = dbclient.get_user_by_username("test").await.unwrap();
        assert_ne!(user.password, "1234asdf");
    }

    #[sqlx::test]
    async fn test_save_user_but_username_is_taken(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        let save_failed: bool = dbclient
            .save_user("marc", "marc_two@mail.de", "1234asdf")
            .await
            .is_err();
        assert_eq!(save_failed, true);
    }

    #[sqlx::test]
    async fn test_save_user_but_email_is_taken(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        let save_failed: bool = dbclient
            .save_user("marc_two", "marc@mail.de", "1234asdf")
            .await
            .is_err();
        assert_eq!(save_failed, true);
    }

    #[sqlx::test]
    async fn test_save_user_username_validation(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        let save_failed: bool = dbclient
            .save_user("Mi", "mi@mail.de", "1234asdf")
            .await
            .is_err();
        assert_eq!(save_failed, true);
    }

    #[sqlx::test]
    async fn test_save_user_email_validation(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        let save_failed: bool = dbclient
            .save_user("test", "test.de", "1234asdf")
            .await
            .is_err();
        assert_eq!(save_failed, true);
    }

    #[sqlx::test]
    async fn test_save_user_password_validation(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        let too_short_pw: &str = "1234";
        let save_failed: bool = dbclient
            .save_user("test", "test@mail.de", too_short_pw)
            .await
            .is_err();
        assert_eq!(save_failed, true);
    }

    #[sqlx::test]
    async fn test_delete_user(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        dbclient.delete_user(1).await.unwrap();
        let user: Option<DbUser> = dbclient.get_user(1).await;
        assert_eq!(user.is_some(), false);
    }

    #[sqlx::test]
    async fn test_update_user(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        let password_before: String = dbclient.get_user(1).await.unwrap().password;
        dbclient
            .update_user(
                1,
                Some("leon"),
                Some("asdf1234"),
                Some("leon@mail.de"),
                Some(Status::Away),
            )
            .await
            .unwrap();
        let user: DbUser = dbclient.get_user(1).await.unwrap();
        assert_eq!(user.username, "leon");
        assert_ne!(password_before, user.password);
        assert_eq!(user.email, "leon@mail.de");
        assert_eq!(user.status, "away");
    }

    #[sqlx::test]
    async fn test_update_user_but_username_is_taken(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        let update_failed: bool = dbclient
            .update_user(1, Some("michelle"), None, None, None)
            .await
            .is_err();
        assert_eq!(update_failed, true);
    }

    #[sqlx::test]
    async fn test_update_user_but_email_is_taken(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient: DbClient = DbClient::new(pool);
        let update_failed: bool = dbclient
            .update_user(1, None, None, Some("michelle@mail.de"), None)
            .await
            .is_err();
        assert_eq!(update_failed, true);
    }

    // ------------------------------------------------------------------------- Tokens
    #[sqlx::test]
    async fn test_get_refresh_tokens(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_refresh_token(1, "1234asdf").await.unwrap();
        dbclient.save_refresh_token(1, "asdf1234").await.unwrap();
        let tokens: Vec<String> = dbclient.get_refresh_tokens(1).await;
        assert_eq!(tokens.len(), 2);
    }

    #[sqlx::test]
    async fn test_get_refresh_token(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_refresh_token(1, "1234asdf").await.unwrap();
        let is_token: bool = dbclient.get_refresh_token("1234asdf").await.is_some();
        assert_eq!(is_token, true);
    }

    #[sqlx::test]
    async fn test_save_refresh_token(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let is_saved: bool = dbclient.save_refresh_token(1, "1234asdf").await.is_ok();
        assert_eq!(is_saved, true);
    }

    #[sqlx::test]
    async fn test_delete_refresh_tokens(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_refresh_token(1, "1234asdf").await.unwrap();
        dbclient.save_refresh_token(1, "1234asdf").await.unwrap();
        dbclient.save_refresh_token(1, "1234asdf").await.unwrap();
        dbclient.delete_refresh_tokens(1).await.unwrap();
        let refresh_tokens: Vec<String> = dbclient.get_refresh_tokens(1).await;
        assert_eq!(refresh_tokens.len(), 0);
    }

    #[sqlx::test]
    async fn delete_refresh_token(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_refresh_token(1, "1234asdf").await.unwrap();
        dbclient.delete_refresh_token("1234asdf").await.unwrap();
        let is_token: bool = dbclient.get_refresh_token("1234asdf").await.is_some();
        assert_eq!(is_token, false);
    }

    // ------------------------------------------------------------------------- Friends
    #[sqlx::test]
    async fn test_get_friend(pool: MySqlPool) {
        let dbclient: DbClient = DbClient::new(pool);
        dbclient.save_friend(1, 2).await.unwrap();
        let friend: Option<DbFriend> = dbclient.get_friend(1, 2).await;
        assert_eq!(friend.is_some(), true);
    }

    #[sqlx::test]
    async fn test_get_friend_with_swapped_ids(pool: MySqlPool) {
        let dbclient: DbClient = DbClient::new(pool);
        dbclient.save_friend(1, 2).await.unwrap();
        let friend: Option<DbFriend> = dbclient.get_friend(2, 1).await;
        assert_eq!(friend.is_some(), true);
    }

    #[sqlx::test]
    async fn test_get_friends(pool: MySqlPool) {
        let dbclient: DbClient = DbClient::new(pool);
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_friend(1, 3).await.unwrap();
        dbclient.save_friend(2, 3).await.unwrap();
        let friends: Vec<DbFriend> = dbclient.get_friends(1).await;
        assert_eq!(friends.len(), 2);
    }

    #[sqlx::test]
    async fn test_save_friend(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_friend(1, 2).await.unwrap();
        let friends: Vec<DbFriend> = dbclient.get_friends(1).await;
        assert_eq!(friends.len(), 1);
    }

    #[sqlx::test]
    async fn test_delete_friend(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_friend(1, 3).await.unwrap();
        let friends_before: Vec<DbFriend> = dbclient.get_friends(1).await;
        assert_eq!(friends_before.len(), 2);
        dbclient.delete_friend(1, 2).await.unwrap();
        let friends_after: Vec<DbFriend> = dbclient.get_friends(1).await;
        assert_eq!(friends_after.len(), 1);
    }

    #[sqlx::test]
    async fn test_delete_friend_deletes_correct_friend(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_friend(2, 3).await.unwrap();
        dbclient.save_friend(1, 3).await.unwrap();
        dbclient.delete_friend(1, 2).await.unwrap();
        assert!(dbclient.get_friend(1, 2).await.is_none());
        assert!(dbclient.get_friend(2, 3).await.is_some());
        assert!(dbclient.get_friend(1, 3).await.is_some());
    }

    #[sqlx::test]
    async fn test_delete_friends(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_friend(1, 3).await.unwrap();
        dbclient.delete_friends(1).await.unwrap();
        let deleted_friends: Vec<DbFriend> = dbclient.get_friends(1).await;
        assert_eq!(deleted_friends.is_empty(), true);
    }

    #[sqlx::test]
    async fn test_delete_friends_deletes_correct_friends(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_friend(2, 3).await.unwrap();
        dbclient.save_friend(1, 3).await.unwrap();
        dbclient.delete_friends(1).await.unwrap();
        assert!(dbclient.get_friend(1, 2).await.is_none());
        assert!(dbclient.get_friend(2, 3).await.is_some());
        assert!(dbclient.get_friend(1, 3).await.is_none());
    }

    // ------------------------------------------------------------------------- Friendrequests
    #[sqlx::test]
    async fn test_get_friendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.get_friendrequest(1, 2).await.unwrap();
    }

    #[sqlx::test]
    async fn test_get_friendrequests(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        let friendreqs: Vec<DbFriendrequest> = dbclient.get_friendrequests(1).await;
        assert_eq!(friendreqs.is_empty(), false);
    }

    #[sqlx::test]
    async fn test_get_friendrequests_is_sorted_by_id(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient
            .save_user("marc", "marc@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("thomas", "thomas@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient
            .save_user("michelle", "michelle@mail.de", "1234asdf")
            .await
            .unwrap();
        dbclient.save_friendrequest(1, 2).await.unwrap();
        dbclient.save_friendrequest(1, 3).await.unwrap();
        dbclient.save_friendrequest(1, 4).await.unwrap();
        let friendrequests: Vec<DbFriendrequest> = dbclient.get_friendrequests(1).await;
        assert_eq!(friendrequests[0].id, 1);
        assert_eq!(friendrequests[1].id, 2);
        assert_eq!(friendrequests[2].id, 3);
    }

    #[sqlx::test]
    async fn test_get_friendrequest_by_id(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.get_friendrequest_by_id(1).await.unwrap();
    }

    #[sqlx::test]
    async fn test_save_friendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.save_friendrequest(1, 2).await.unwrap();
    }

    #[sqlx::test]
    async fn test_delete_friendrequest(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_friendrequest(1).await.unwrap();
        let deleted_friendreq: Option<DbFriendrequest> = dbclient.get_friendrequest_by_id(1).await;
        assert_eq!(deleted_friendreq.is_none(), true);
    }

    #[sqlx::test]
    async fn test_delete_friendrequests(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friendrequests(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_friendrequests(1).await.unwrap();
        let deleted_friendrequests: Vec<DbFriendrequest> = dbclient.get_friendrequests(1).await;
        assert_eq!(deleted_friendrequests.is_empty(), true);
    }
    // ------------------------------------------------------------------------- Blocked Users
    #[sqlx::test]
    async fn test_get_blocked_user(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.block_user(1, 2).await.unwrap();
        let blocked_user: Option<DbBlockedUser> = dbclient.get_blocked_user(1, 2).await;
        assert!(blocked_user.is_some());
    }

    #[sqlx::test]
    async fn test_get_blocked_users(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.block_user(1, 2).await.unwrap();
        dbclient.block_user(1, 3).await.unwrap();
        dbclient.block_user(2, 1).await.unwrap();
        let blocked_users: Vec<DbBlockedUser> = dbclient.get_blocked_users(1).await;
        assert_eq!(blocked_users.len(), 2);
    }

    #[sqlx::test]
    async fn test_block_user(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.block_user(1, 2).await.unwrap();
        let blocked_user: Option<DbBlockedUser> = dbclient.get_blocked_user(1, 2).await;
        assert!(blocked_user.is_some());
    }

    #[sqlx::test]
    async fn test_block_user_who_is_already_blocked(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let fst_try = dbclient.block_user(1, 2).await;
        let snd_try = dbclient.block_user(1, 2).await;
        assert!(fst_try.is_ok());
        assert!(snd_try.is_err());
    }

    #[sqlx::test]
    async fn test_unblock_user(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.block_user(1, 2).await.unwrap();
        let unblocked = dbclient.unblock_user(1, 2).await;
        assert!(unblocked.is_ok());
    }

    #[sqlx::test]
    async fn test_unblock_user_whow_wasnt_blocked(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let unblocked = dbclient.unblock_user(1, 2).await;
        assert!(unblocked.is_err());
    }

    #[sqlx::test]
    async fn test_unblock_users(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.block_user(1, 2).await.unwrap();
        dbclient.block_user(1, 3).await.unwrap();
        let unblocked_all: bool = dbclient.unblock_users(1).await.is_ok();
        let blocked_users: Vec<DbBlockedUser> = dbclient.get_blocked_users(1).await;
        assert!(unblocked_all);
        assert_eq!(blocked_users.len(), 0);
    }

    // ------------------------------------------------------------------------- Posts
    #[sqlx::test]
    async fn test_get_post(pool: MySqlPool) {
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        let post: DbPost = dbclient.get_post(1).await.unwrap();
        assert_eq!(post.id, 1);
        assert_eq!(post.content, "First post!");
    }

    #[sqlx::test]
    async fn test_get_posts(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        let posts: Vec<DbPost> = dbclient.get_posts(1).await;
        assert_eq!(posts.len(), 3);
    }

    #[sqlx::test]
    async fn test_get_posts_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_post(1, "this is a post", 0).await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient.save_post(1, "this is a post", 0).await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient.save_post(1, "this is a post", 0).await.unwrap();
        let posts: Vec<DbPost> = dbclient.get_posts(1).await;
        assert_eq!(posts[0].id, 3);
        assert_eq!(posts[1].id, 2);
        assert_eq!(posts[2].id, 1);
    }

    #[sqlx::test]
    async fn test_get_posts_of_user_with_no_posts(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let posts: Vec<DbPost> = dbclient.get_posts(999).await;
        assert_eq!(posts.len(), 0);
    }

    #[sqlx::test]
    async fn test_save_post(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_post(1, "new post", 0).await.unwrap();
    }

    #[sqlx::test]
    async fn test_save_post_returns_saved_post(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let saved_post: DbPost = dbclient.save_post(1, "new post", 0).await.unwrap();
        let getted_post: DbPost = dbclient.get_post(1).await.unwrap();
        assert_eq!(saved_post.id, getted_post.id);
        assert_eq!(saved_post.author, getted_post.author);
        assert_eq!(saved_post.content, getted_post.content);
        assert_eq!(saved_post.created_at, getted_post.created_at);
    }

    #[sqlx::test]
    async fn test_save_post_but_content_is_too_long(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let mut content = String::new();
        for _ in 0..=255 {
            content.push_str("a");
        }
        let failed: bool = dbclient.save_post(1, content, 0).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_delete_post(pool: MySqlPool) {
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_post(1).await.unwrap();
        let deleted: bool = dbclient.get_post(1).await.is_none();
        assert_eq!(deleted, true);
    }

    #[sqlx::test]
    async fn test_delete_post_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let failed: bool = dbclient.delete_post(1).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_delete_post_deletes_all_likes(pool: MySqlPool) {
        init_test_posts(&pool).await;
        init_test_post_likes(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_post(1).await.unwrap();
        let likes: Vec<DbPostLike> = dbclient.get_post_likes(1).await;
        assert_eq!(likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_post_deletes_all_comments(pool: MySqlPool) {
        init_test_posts(&pool).await;
        init_test_post_comments(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_post(1).await.unwrap();
        let comments: Vec<DbPostComment> = dbclient.get_post_comments(1).await;
        assert_eq!(comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_posts(pool: MySqlPool) {
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_posts(1).await.unwrap();
        let posts: Vec<DbPost> = dbclient.get_posts(1).await;
        assert_eq!(posts.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_posts_deletes_also_likes(pool: MySqlPool) {
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_posts(1).await.unwrap();
        let fst_deleted_post_likes: Vec<DbPostLike> = dbclient.get_post_likes(1).await;
        let snd_deleted_post_likes: Vec<DbPostLike> = dbclient.get_post_likes(3).await;
        assert_eq!(fst_deleted_post_likes.len(), 0);
        assert_eq!(snd_deleted_post_likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_posts_deletes_also_comments(pool: MySqlPool) {
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_posts(1).await.unwrap();
        let fst_deleted_post_comments: Vec<DbPostComment> = dbclient.get_post_comments(1).await;
        let snd_deleted_post_comments: Vec<DbPostComment> = dbclient.get_post_comments(3).await;
        assert_eq!(fst_deleted_post_comments.len(), 0);
        assert_eq!(snd_deleted_post_comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_get_post_likes(pool: MySqlPool) {
        init_test_posts(&pool).await;
        init_test_post_likes(&pool).await;
        let dbclient = DbClient::new(pool);
        let likes: Vec<DbPostLike> = dbclient.get_post_likes(1).await;
        assert_eq!(likes.len(), 2);
    }

    #[sqlx::test]
    async fn test_get_post_likes_of_post_with_no_likes(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let likes: Vec<DbPostLike> = dbclient.get_post_likes(1).await;
        assert_eq!(likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_post_likes(pool: MySqlPool) {
        init_test_posts(&pool).await;
        init_test_post_likes(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_post_likes(1).await.unwrap();
        let likes: Vec<DbPostLike> = dbclient.get_post_likes(1).await;
        assert_eq!(likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_like_post(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        dbclient.like_post(1, 2).await.unwrap();
        let liked_post: ClientPost = dbclient
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient, &config.domain)
            .await;
        assert_eq!(liked_post.likes.len(), 1);
        assert_eq!(liked_post.likes[0], "http://localhost:4000/users/2");
    }

    #[sqlx::test]
    async fn test_like_post_twice(pool: MySqlPool) {
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.like_post(1, 2).await.unwrap();
        let failed: bool = dbclient.like_post(1, 2).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_like_post_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let failed: bool = dbclient.like_post(1, 1).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_unlike_post(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        dbclient.like_post(1, 2).await.unwrap();
        dbclient.unlike_post(1, 2).await.unwrap();
        let unliked_post: ClientPost = dbclient
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient, &config.upload_dir)
            .await;
        assert_eq!(unliked_post.likes.len(), 0);
    }

    #[sqlx::test]
    async fn test_unlike_post_that_i_didnt_like(pool: MySqlPool) {
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        let failed: bool = dbclient.unlike_post(1, 2).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_unlike_post_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let error: HttpError = dbclient.unlike_post(1, 1).await.unwrap_err();
        assert_eq!(error.message, ErrorMessage::NoPostFound.to_string());
    }

    #[sqlx::test]
    async fn test_get_post_comment(pool: MySqlPool) {
        init_test_posts(&pool).await;
        init_test_post_comments(&pool).await;
        let dbclient = DbClient::new(pool);
        let comment: DbPostComment = dbclient.get_post_comment(1).await.unwrap();
        assert_eq!(comment.content, "lol");
    }

    #[sqlx::test]
    async fn test_get_post_comment_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let none: bool = dbclient.get_post_comment(1).await.is_none();
        assert_eq!(none, true);
    }

    #[sqlx::test]
    async fn test_get_post_comments(pool: MySqlPool) {
        init_test_posts(&pool).await;
        init_test_post_comments(&pool).await;
        let dbclient = DbClient::new(pool);
        let comments: Vec<DbPostComment> = dbclient.get_post_comments(1).await;
        assert_eq!(comments.len(), 3);
    }

    #[sqlx::test]
    async fn test_get_post_comments_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient.save_post(1, "this is a post", 0).await.unwrap();
        dbclient
            .save_post_comment(1, 1, "this is a comment")
            .await
            .unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient
            .save_post_comment(1, 1, "this is a comment")
            .await
            .unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient
            .save_post_comment(1, 1, "this is a comment")
            .await
            .unwrap();
        let post_comments: Vec<DbPostComment> = dbclient.get_post_comments(1).await;
        assert_eq!(post_comments[0].id, 3);
        assert_eq!(post_comments[1].id, 2);
        assert_eq!(post_comments[2].id, 1);
    }

    #[sqlx::test]
    async fn test_get_post_comments_of_post_that_has_no_comments(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let comments: Vec<DbPostComment> = dbclient.get_post_comments(1).await;
        assert_eq!(comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_save_post_comment(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        let config = Config::new_test();
        dbclient.save_post_comment(1, 1, "lol").await.unwrap();
        let post: ClientPost = dbclient
            .get_post(1)
            .await
            .unwrap()
            .into_client(&dbclient, &config.domain)
            .await;
        assert_eq!(post.comments[0].id, 1);
        assert_eq!(post.comments[0].author, "http://localhost:4000/users/1");
        assert_eq!(post.comments[0].content, "lol");
        assert_eq!(post.comments.len(), 1);
    }

    #[sqlx::test]
    async fn test_save_post_comment_to_post_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let failed: bool = dbclient.save_post_comment(1, 1, "lol").await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_save_post_comment_that_is_too_long(pool: MySqlPool) {
        init_test_posts(&pool).await;
        let dbclient = DbClient::new(pool);
        let mut content = String::new();
        for _ in 0..=255 {
            content.push_str("a");
        }
        let failed: bool = dbclient.save_post_comment(1, 1, content).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_delete_post_comment(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.save_post(1, "test post", 0).await.unwrap();
        dbclient
            .save_post_comment(1, 1, "test post comment")
            .await
            .unwrap();
        dbclient.delete_post_comment(1).await.unwrap();
        let comments: Vec<DbPostComment> = dbclient.get_post_comments(1).await;
        assert_eq!(comments.len(), 0);
    }

    #[sqlx::test]
    async fn test_delete_post_comment_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let failed: bool = dbclient.delete_post_comment(1).await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_delete_post_comments(pool: MySqlPool) {
        init_test_posts(&pool).await;
        init_test_post_comments(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_post_comments(1).await.unwrap();
        let comments: Vec<DbPostComment> = dbclient.get_post_comments(1).await;
        assert_eq!(comments.len(), 0);
    }

    // ------------------------------------------------------------------------- Messages
    #[sqlx::test]
    async fn test_get_message(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.get_message(1).await.unwrap();
    }

    #[sqlx::test]
    async fn test_get_message_which_doesnt_exist(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let message: Option<DbMessage> = dbclient.get_message(999).await;
        assert_eq!(message.is_none(), true);
    }

    #[sqlx::test]
    async fn test_get_message_that_is_soft_deleted(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_message(1).await.unwrap();
        let failed: bool = dbclient.get_message(1).await.is_none();
        assert!(failed);
    }

    #[sqlx::test]
    async fn test_get_messages(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let messages: Vec<DbMessage> = dbclient.get_messages(1).await;
        assert_eq!(messages.len(), 3);
    }

    #[sqlx::test]
    async fn test_get_messages_has_no_soft_deleted_message(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_message(1).await.unwrap();
        let messages: Vec<DbMessage> = dbclient.get_messages(1).await;
        assert_eq!(messages.len(), 2);
    }

    #[sqlx::test]
    async fn test_get_messages_of_user_that_doesnt_exist(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let messages: Vec<DbMessage> = dbclient.get_messages(999).await;
        assert_eq!(messages.len(), 0);
    }

    #[sqlx::test]
    async fn test_get_messages_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let first: DbMessage = dbclient.save_message(1, 2, "hiii").await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        let second: DbMessage = dbclient.save_message(1, 2, "haha").await.unwrap();
        let messages: Vec<DbMessage> = dbclient.get_messages(1).await;
        assert_eq!(first.id, messages[1].id);
        assert_eq!(second.id, messages[0].id);
    }

    #[sqlx::test]
    async fn test_save_message(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.save_message(1, 2, "Oh hi Marc").await.unwrap();
    }

    #[sqlx::test]
    async fn test_save_message_returns_saved_message(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let saved_message: DbMessage = dbclient.save_message(1, 2, "Oh hi Marc").await.unwrap();

        assert_eq!(saved_message.id, 1);
        assert_eq!(saved_message.sender, 1);
        assert_eq!(saved_message.receiver, 2);
        assert_eq!(saved_message.content, "Oh hi Marc");
        assert_eq!(saved_message.edited_at.is_none(), true);
    }

    #[sqlx::test]
    async fn test_save_message_with_no_content(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let failed: bool = dbclient.save_message(1, 2, "").await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_save_message_to_user_who_isnt_friend(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let failed: bool = dbclient.save_message(1, 999, "hi").await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_save_message_to_yourself(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let message: DbMessage = dbclient.save_message(1, 1, "hi").await.unwrap();
        assert_eq!(message.sender, message.receiver);
    }

    #[sqlx::test]
    async fn test_save_message_to_user_who_is_blocked(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_blocked_users(&pool).await;
        let dbclient = DbClient::new(pool);
        let failed: bool = dbclient.save_message(1, 2, "hiii").await.is_err();
        assert!(failed);
    }

    #[sqlx::test]
    async fn test_update_message(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.update_message(1, "test").await.unwrap();
    }

    #[sqlx::test]
    async fn test_update_message_updates_content(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.update_message(1, "test").await.unwrap();
        let updated_content: String = dbclient.get_message(1).await.unwrap().content;
        assert_eq!(updated_content, "test");
    }

    #[sqlx::test]
    async fn test_update_message_updates_edited_at(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let edited_at: Option<DateTime<Utc>> = dbclient.get_message(1).await.unwrap().edited_at;
        dbclient.update_message(1, "test").await.unwrap();
        let updated_edited_at: Option<DateTime<Utc>> =
            dbclient.get_message(1).await.unwrap().edited_at;
        assert_ne!(edited_at, updated_edited_at);
    }

    #[sqlx::test]
    async fn test_update_message_with_no_new_content(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let failed: bool = dbclient.update_message(1, "").await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_update_message_that_doesnt_exist(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        let failed: bool = dbclient.update_message(1, "test").await.is_err();
        assert_eq!(failed, true);
    }

    #[sqlx::test]
    async fn test_delete_message_for_sender(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_message_for_sender(1).await.unwrap();
        let message: DbMessage = dbclient.get_message(1).await.unwrap();
        assert_eq!(message.is_deleted_by_sender, 1);
    }

    #[sqlx::test]
    async fn test_delete_message_for_receiver(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_message_for_receiver(1).await.unwrap();
        let message: DbMessage = dbclient.get_message(1).await.unwrap();
        assert_eq!(message.is_deleted_by_receiver, 1);
    }

    #[sqlx::test]
    async fn test_delete_message_that_is_i_sent_to_myself(pool: MySqlPool) {
        init_test_users(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient
            .save_message(1, 1, "this is a message for just myself")
            .await
            .unwrap();
        dbclient.delete_message(1).await.unwrap();
        let is_deleted: bool = dbclient.get_message(1).await.is_none();
        assert!(is_deleted);
    }

    #[sqlx::test]
    async fn test_delete_message(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_message(1).await.unwrap();
        let is_deleted: bool = dbclient.get_message(1).await.is_none();
        assert!(is_deleted);
    }

    #[sqlx::test]
    async fn test_delete_messages(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_messages(1).await.unwrap();
        let messages: Vec<DbMessage> = dbclient.get_messages(1).await;
        assert_eq!(messages.len(), 0);
    }

    #[sqlx::test]
    async fn test_get_chat(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let chat: Vec<DbMessage> = dbclient.get_chat(1, 2).await;
        assert_eq!(chat.len(), 3);
    }

    #[sqlx::test]
    async fn test_get_chat_is_sorted_from_newest_to_oldest(pool: MySqlPool) {
        let dbclient = DbClient::new(pool);
        dbclient
            .save_user("marc", "marc@mail.de", "1234adsf")
            .await
            .unwrap();
        dbclient
            .save_user("leon", "leon@mail.de", "1234adsf")
            .await
            .unwrap();
        dbclient.save_friend(1, 2).await.unwrap();
        dbclient.save_message(1, 2, "hi").await.unwrap();
        tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        dbclient.save_message(1, 2, "hi").await.unwrap();
        let chat: Vec<DbMessage> = dbclient.get_chat(1, 2).await;
        assert_eq!(chat[0].id, 2);
        assert_eq!(chat[1].id, 1);
    }

    #[sqlx::test]
    async fn tet_get_chat_with_whom_there_are_no_messages(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        let dbclient = DbClient::new(pool);
        let chat: Vec<DbMessage> = dbclient.get_chat(1, 2).await;
        assert_eq!(chat.len(), 0);
    }

    #[sqlx::test]
    async fn test_get_chat_but_isnt_friend_anymore(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        let chat: Vec<DbMessage> = dbclient.get_chat(1, 2).await;
        assert_eq!(chat.len(), 3);
    }

    #[sqlx::test]
    async fn test_delete_chat(pool: MySqlPool) {
        init_test_users(&pool).await;
        init_test_friends(&pool).await;
        init_test_messages(&pool).await;
        let dbclient = DbClient::new(pool);
        dbclient.delete_chat(1, 2).await.unwrap();
        let chat: Vec<DbMessage> = dbclient.get_chat(1, 2).await;

        for message in chat {
            let is_deleted: bool =
                message.is_deleted_by_sender == 0 || message.is_deleted_by_receiver == 0;
            assert!(is_deleted);
        }
    }
}
