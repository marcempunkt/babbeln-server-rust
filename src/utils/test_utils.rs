use crate::dbclient::{
    BlockedUserExt, DbClient, FriendExt, FriendrequestExt, MessageExt, PostExt, TokenExt, UserExt,
};
use crate::extractors::authentication_token::Claims;
use jsonwebtoken::{encode, errors::Error as JwtError, EncodingKey, Header};
use sqlx::mysql::MySqlPool;

pub fn get_test_token(user_id: usize, secret: &str) -> String {
    let token: Result<String, JwtError> = encode(
        &Header::default(),
        &Claims::access(user_id),
        &EncodingKey::from_secret(secret.as_ref()),
    );
    token.unwrap()
}

pub fn get_expired_test_token(user_id: usize, secret: &str) -> String {
    let token: Result<String, JwtError> = encode(
        &Header::default(),
        &Claims::expired(user_id),
        &EncodingKey::from_secret(secret.as_ref()),
    );
    token.unwrap()
}

pub async fn init_test_users(pool: &MySqlPool) {
    let dbclient = DbClient::new(pool.clone());
    dbclient
        .save_user("marc", "marc@mail.de", "1234asdf")
        .await
        .unwrap();
    dbclient
        .save_user("nico", "nico@mail.de", "1234asdf")
        .await
        .unwrap();
    dbclient
        .save_user("michelle", "michelle@mail.de", "1234asdf")
        .await
        .unwrap();
}

pub async fn init_test_friends(pool: &MySqlPool) {
    let dbclient = DbClient::new(pool.clone());
    dbclient.save_friend(1, 2).await.unwrap();
}

pub async fn init_test_friendrequests(pool: &MySqlPool) {
    let dbclient = DbClient::new(pool.clone());
    dbclient.save_friendrequest(1, 2).await.unwrap();
}

pub async fn init_test_refresh_tokens(pool: &MySqlPool) {
    let dbclient = DbClient::new(pool.clone());
    dbclient
        .save_refresh_token(1, "super_secret_refresh_token")
        .await
        .unwrap();
}

pub async fn init_test_messages(pool: &MySqlPool) {
    let dbclient = DbClient::new(pool.clone());
    dbclient.save_message(1, 2, "Hi what's up?").await.unwrap();
    dbclient
        .save_message(2, 1, "Nothing, how about you?")
        .await
        .unwrap();
    dbclient
        .save_message(1, 2, "I'm trapped in a db!")
        .await
        .unwrap();
}

pub async fn init_test_posts(pool: &MySqlPool) {
    let dbclient = DbClient::new(pool.clone());
    dbclient.save_post(1, "First post!", 0).await.unwrap();
    dbclient.save_post(2, "Second post!", 0).await.unwrap();
    dbclient.save_post(1, "Third post!", 0).await.unwrap();
    dbclient.save_post(3, "Fourth post!", 0).await.unwrap();
}

pub async fn init_test_post_comments(pool: &MySqlPool) {
    let dbclient = DbClient::new(pool.clone());
    dbclient.save_post_comment(1, 2, "lol").await.unwrap();
    dbclient.save_post_comment(1, 1, "what?").await.unwrap();
    dbclient
        .save_post_comment(1, 2, "nice pics of your cat")
        .await
        .unwrap();
    dbclient.save_post_comment(2, 1, "hahah").await.unwrap();
}

pub async fn init_test_post_likes(pool: &MySqlPool) {
    let dbclient = DbClient::new(pool.clone());
    dbclient.like_post(1, 1).await.unwrap();
    dbclient.like_post(1, 2).await.unwrap();
    dbclient.like_post(2, 1).await.unwrap();
    dbclient.like_post(2, 2).await.unwrap();
}

pub async fn init_test_blocked_users(pool: &MySqlPool) {
    let dbclient = DbClient::new(pool.clone());
    dbclient.block_user(1, 2).await.unwrap();
}
