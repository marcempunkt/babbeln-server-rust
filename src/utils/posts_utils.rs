use crate::dbclient::{DbClient, FriendExt, PostExt};
use crate::models::{DbFriend, DbPost};

pub async fn is_my_or_friends_post(user_id: usize, post_id: usize, dbclient: &DbClient) -> bool {
    let post: Option<DbPost> = dbclient.get_post(post_id).await;
    if post.is_none() {
        return false;
    }
    let post: DbPost = post.unwrap();
    let friends: Vec<DbFriend> = dbclient.get_friends(user_id).await;
    let is_my_post: bool = user_id == post.author as usize;
    let is_friends_post: bool = friends
        .into_iter()
        .filter(|friend: &DbFriend| {
            friend.user_one == post.author || friend.user_two == post.author
        })
        .collect::<Vec<DbFriend>>()
        .get(0)
        .is_some();
    if !is_my_post && !is_friends_post {
        return false;
    }
    true
}

pub async fn is_my_post(user_id: usize, post_id: usize, dbclient: &DbClient) -> bool {
    let post: Option<DbPost> = dbclient.get_post(post_id).await;
    if post.is_none() {
        return false;
    }
    let post: DbPost = post.unwrap();
    let is_my_post: bool = user_id == post.author as usize;
    is_my_post
}
