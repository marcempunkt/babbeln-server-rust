use crate::error::{ErrorMessage, HttpError};
use actix_multipart::Multipart;
use actix_web::{http::header::CONTENT_LENGTH, web, HttpRequest};
use async_trait::async_trait;
use futures_util::TryStreamExt;
use image::{imageops::FilterType, DynamicImage};
use log::{debug, error, info};
use mime::{Mime, IMAGE_GIF, IMAGE_JPEG, IMAGE_PNG};
use std::marker::Send;
use std::path::{Path, PathBuf};
use tokio::{fs, fs::File, io::AsyncWriteExt};
use uuid::Uuid;

pub struct PostPayload {
    pub content: String,
    pub files: Vec<UploadedFile>,
}

pub struct AvatarPayload {
    pub image: UploadedFile,
}

#[async_trait]
pub trait WriteToDiskExt {
    async fn write_to_disk(
        self,
        id: usize,
        upload_dir: impl Into<String> + Send + Copy,
        dimensions: Option<(usize, usize)>,
    ) -> Result<(), HttpError>;
}

#[async_trait]
impl WriteToDiskExt for PostPayload {
    async fn write_to_disk(
        self,
        post_id: usize,
        upload_dir: impl Into<String> + Send + Copy,
        dimensions: Option<(usize, usize)>,
    ) -> Result<(), HttpError> {
        for (index, file) in self.files.into_iter().enumerate() {
            let upload_dir: String = upload_dir.into();
            let destination: PathBuf = PathBuf::from(Path::new(&upload_dir).join("posts").join(
                &format!("{}.{}", Uuid::new_v4(), Uploader::get_ext(&file.mime)),
            ));

            let mut saved_file: File = File::create(&destination).await.unwrap();
            saved_file.write_all(&file.raw).await.unwrap();

            info!("Temporarily stored post image to: {:?}", &destination);

            web::block(move || async move {
                let uploaded_img: DynamicImage = match image::open(&destination) {
                    Ok(img) => Ok(img),
                    Err(err) => {
                        fs::remove_file(&destination).await.unwrap();
                        error!(
                            "Could not open the temporarily stored post image in: {:?}",
                            &destination
                        );
                        error!("{err}");
                        Err(HttpError::bad_request(ErrorMessage::UploadError))
                    }
                }?;

                let new_destination: PathBuf = PathBuf::from(
                    Path::new(&upload_dir)
                        .join("posts")
                        .join(&format!("{post_id}_{index}.jpeg")),
                );

                fs::remove_file(&destination).await.map_err(|_| {
                    error!(
                        "Could not delete the temporarily stored post image in: {:?}",
                        &destination
                    );
                    HttpError::server_error()
                })?;

                if let Some((x, y)) = dimensions {
                    uploaded_img.resize_exact(x as u32, y as u32, FilterType::Nearest);
                }

                uploaded_img.save(&new_destination).map_err(|_| {
                    error!(
                        "Could not save the formatted post image to from: {:?} to {:?}",
                        &destination, &new_destination
                    );
                    HttpError::server_error()
                })?;

                Ok::<(), HttpError>(())
            })
            .await
            .unwrap()
            .await?;
        }
        Ok(())
    }
}

#[async_trait]
impl WriteToDiskExt for AvatarPayload {
    async fn write_to_disk(
        self,
        user_id: usize,
        upload_dir: impl Into<String> + Send + Copy,
        dimensions: Option<(usize, usize)>,
    ) -> Result<(), HttpError> {
        let image: UploadedFile = self.image;

        let upload_dir: String = upload_dir.into();
        let destination: PathBuf = PathBuf::from(Path::new(&upload_dir).join("avatar").join(
            &format!("{}.{}", Uuid::new_v4(), Uploader::get_ext(&image.mime)),
        ));

        let mut saved_file: File = File::create(&destination).await.unwrap();
        saved_file.write_all(&image.raw).await.unwrap();

        info!("Temporarily stored avatar picture to: {:?}", &destination);

        web::block(move || async move {
            let mut uploaded_img: DynamicImage = match image::open(&destination) {
                Ok(img) => Ok(img),
                Err(err) => {
                    fs::remove_file(&destination).await.unwrap();
                    error!(
                        "Could not open the temporarily stored avatar picture in: {:?}",
                        &destination
                    );
                    error!("{err}");
                    Err(HttpError::bad_request(ErrorMessage::UploadError))
                }
            }?;

            let new_destination: PathBuf = PathBuf::from(
                Path::new(&upload_dir)
                    .join("avatar")
                    .join(&format!("{user_id}.jpeg")),
            );

            fs::remove_file(&destination).await.map_err(|_| {
                error!(
                    "Could not delete the temporarily stored avatar picture in: {:?}",
                    &destination
                );
                HttpError::server_error()
            })?;

            if let Some((x, y)) = dimensions {
                uploaded_img = uploaded_img.resize_exact(x as u32, y as u32, FilterType::Nearest);
            }

            uploaded_img.save(&new_destination).map_err(|_| {
                error!(
                    "Could not save the formatted avatar picture from: {:?} to {:?}",
                    &destination, &new_destination
                );
                HttpError::server_error()
            })?;

            Ok::<(), HttpError>(())
        })
        .await
        .unwrap()
        .await
    }
}

pub struct UploadedFile {
    pub mime: Mime,
    pub raw: Vec<u8>,
}

#[derive(Clone)]
pub struct Uploader {
    max_size: usize, // in mb
    max_count: usize,
    mime_types: Vec<Mime>,
    required: bool,
}

impl Uploader {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }

    pub fn max_size_in_mb(mut self, max_size: usize) -> Self {
        self.max_size = max_size * 800_000;
        self
    }

    pub fn max_size(mut self, max_size: usize) -> Self {
        self.max_size = max_size;
        self
    }

    pub fn max_count(mut self, max_count: usize) -> Self {
        self.max_count = max_count;
        self
    }

    pub fn mime_types(mut self, mime_types: Vec<Mime>) -> Self {
        self.mime_types = mime_types;
        self
    }

    pub fn required(mut self, required: bool) -> Self {
        self.required = required;
        self
    }

    pub fn is_req_too_large(&self, req: &HttpRequest) -> bool {
        let content_length: usize = match req.headers().get(CONTENT_LENGTH) {
            Some(header_value) => header_value.to_str().unwrap_or("0").parse().unwrap_or(0),
            None => "0".parse().unwrap(),
        };

        content_length > self.max_size
    }

    fn get_ext(mime: &Mime) -> String {
        match mime.to_string().as_str() {
            "image/gif" => "gif".to_string(),
            "image/jpeg" => "jpeg".to_string(),
            "image/png" => "png".to_string(),
            _ => "".to_string(),
        }
    }
}

#[async_trait(?Send)]
pub trait IntoPayloadExt {
    async fn into_post_payload(
        self,
        mut payload: Multipart,
        req: &HttpRequest,
    ) -> Result<PostPayload, HttpError>;
    async fn into_avatar_payload(
        self,
        mut payload: Multipart,
        req: &HttpRequest,
    ) -> Result<AvatarPayload, HttpError>;
}

#[async_trait(?Send)]
impl IntoPayloadExt for Uploader {
    async fn into_post_payload(
        self,
        mut payload: Multipart,
        req: &HttpRequest,
    ) -> Result<PostPayload, HttpError> {
        let mut content: String = String::new();
        let mut files: Vec<UploadedFile> = Vec::new();

        if self.is_req_too_large(req) {
            return Err(HttpError::new(
                ErrorMessage::PayloadTooLarge.to_string(),
                413,
            ));
        }

        loop {
            if let Ok(Some(mut field)) = payload.try_next().await {
                let mime: Mime = field.content_type().clone();
                let mut raw: Vec<u8> = Vec::new();

                match field.name() {
                    // store all the content, which is the text of a post
                    "content" => {
                        while let Ok(Some(chunk)) = field.try_next().await {
                            content.push_str(std::str::from_utf8(&chunk).unwrap_or(""));
                        }
                    }
                    "image" => {
                        if !self.mime_types.contains(&mime) {
                            debug!("Illegal MIME type {mime}");
                            continue;
                        }

                        if files.len() >= self.max_count {
                            debug!("Too many files. Skip 'image' field!");
                            continue;
                        }

                        while let Ok(Some(chunk)) = field.try_next().await {
                            raw.extend_from_slice(&chunk);
                        }
                        files.push(UploadedFile { mime, raw });
                    }
                    _ => (),
                };
            } else {
                break; // payload is emptied
            }
        }

        if self.required && files.len() == 0 {
            return Err(HttpError::bad_request(ErrorMessage::MissingPictures));
        }
        Ok(PostPayload { content, files })
    }

    async fn into_avatar_payload(
        self,
        mut payload: Multipart,
        req: &HttpRequest,
    ) -> Result<AvatarPayload, HttpError> {
        let mut file: Option<UploadedFile> = None;

        if self.is_req_too_large(req) {
            return Err(HttpError::new(
                ErrorMessage::PayloadTooLarge.to_string(),
                413,
            ));
        }

        loop {
            if let Ok(Some(mut field)) = payload.try_next().await {
                let mime: Mime = field.content_type().clone();
                let mut raw: Vec<u8> = Vec::new();

                match field.name() {
                    "image" => {
                        if !self.mime_types.contains(&mime) {
                            debug!("Illegal MIME type {mime}");
                            return Err(HttpError::bad_request(ErrorMessage::WrongFileType));
                        }
                        while let Ok(Some(chunk)) = field.try_next().await {
                            raw.extend_from_slice(&chunk);
                        }
                        file = Some(UploadedFile { mime, raw });
                    }
                    _ => (),
                };
            } else {
                break; // payload is emptied
            }
        }

        if self.required && file.is_none() {
            return Err(HttpError::bad_request(ErrorMessage::MissingPicture));
        }
        Ok(AvatarPayload {
            image: file.unwrap(),
        })
    }
}

impl Default for Uploader {
    fn default() -> Self {
        Self {
            max_size: 1 * 800_000, // 1 mb
            max_count: 1,
            mime_types: vec![IMAGE_PNG, IMAGE_JPEG, IMAGE_GIF],
            required: true,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::config::Config;

    const BLACK_PIXEL_PNG: [u8; 72] = [
        137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 1, 0, 0, 0, 1, 8, 2,
        0, 0, 0, 144, 119, 83, 222, 0, 0, 0, 15, 73, 68, 65, 84, 8, 29, 1, 4, 0, 251, 255, 0, 0, 0,
        0, 0, 4, 0, 1, 47, 82, 180, 141, 0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130,
    ];

    const CORRUPTED_BLACK_PIXEL_PNG: [u8; 58] = [
        0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 1, 0, 0, 0, 1, 8, 2, 0, 0, 0, 144, 119, 83, 222, 0, 0,
        0, 15, 73, 68, 65, 84, 8, 29, 1, 4, 0, 251, 255, 0, 0, 0, 0, 0, 4, 0, 1, 47, 82, 180, 141,
        0, 0, 0, 0, 73, 69, 130,
    ];

    #[tokio::test]
    async fn test_write_to_disk_post() {
        let config = Config::new_test();
        let file = UploadedFile {
            mime: IMAGE_PNG,
            raw: BLACK_PIXEL_PNG.to_vec(),
        };
        let post_payload = PostPayload {
            content: String::new(),
            files: vec![file],
        };

        post_payload.write_to_disk(1, &config.upload_dir, None).await.unwrap();

        let stored_file = Path::new(&config.upload_dir).join("posts").join("1_0.jpeg");
        let is_file_stored: bool = stored_file.exists();
        let is_file_ok: bool = image::open(stored_file).is_ok();

        assert!(is_file_stored);
        assert!(is_file_ok);
    }

    #[tokio::test]
    async fn test_write_to_disk_post_with_garbage_data() {
        let config = Config::new_test();
        let file = UploadedFile {
            mime: IMAGE_PNG,
            raw: CORRUPTED_BLACK_PIXEL_PNG.to_vec(),
        };
        let post_payload = PostPayload {
            content: String::new(),
            files: vec![file],
        };

        let did_write_to_disk: bool = post_payload.write_to_disk(1, &config.upload_dir, None).await.is_ok();
        let is_posts_dir_empty: bool = Path::new(&config.upload_dir).join("posts").read_dir().unwrap().nth(0).is_none();

        assert_eq!(did_write_to_disk, false);
        assert!(is_posts_dir_empty);
    }

    #[tokio::test]
    async fn test_write_to_disk_avatar() {
        let config = Config::new_test();
        let image = UploadedFile {
            mime: IMAGE_PNG,
            raw: BLACK_PIXEL_PNG.to_vec(),
        };
        let avatar_payload = AvatarPayload { image };

        avatar_payload.write_to_disk(1, &config.upload_dir, None).await.unwrap();

        let stored_file = Path::new(&config.upload_dir).join("avatar").join("1.jpeg");
        let is_file_stored: bool = stored_file.exists();
        let is_file_ok: bool = image::open(stored_file).is_ok();

        assert!(is_file_stored);
        assert!(is_file_ok);
    }

    #[tokio::test]
    async fn test_write_to_disk_avatar_with_garbage_data() {
        let config = Config::new_test();
        let image = UploadedFile {
            mime: IMAGE_PNG,
            raw: CORRUPTED_BLACK_PIXEL_PNG.to_vec(),
        };
        let avatar_payload = AvatarPayload { image };

        let did_write_to_disk: bool = avatar_payload.write_to_disk(1, &config.upload_dir, None).await.is_ok();
        let exist_avatar: bool = Path::new(&config.upload_dir).join("avatar").join("1.jpeg").exists();

        assert_eq!(did_write_to_disk, false);
        assert_eq!(exist_avatar, false);
    }
}
