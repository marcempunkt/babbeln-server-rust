use actix_web::web;
use std::path::Path;
use tokio::fs;

pub async fn delete_avatar(user_id: usize, upload_dir: impl Into<String>) {
    let upload_dir: String = upload_dir.into();

    web::block(move || async move {
        let avatar_path = Path::new(&upload_dir)
            .join("avatar")
            .join(format!("{user_id}.jpeg"));
        if avatar_path.exists() {
            fs::remove_file(avatar_path).await.unwrap();
        }
    })
    .await
        .unwrap()
        .await;
}

pub async fn delete_post_pics(user_id: usize, upload_dir: impl Into<String>) {
    todo!()
}
