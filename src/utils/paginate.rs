use serde::{Deserialize, Serialize};

pub struct Paginator<T> {
    data: Vec<T>,
    page: usize,
    page_size: usize,
    url: String,
    pathname: String,
    extra_params: Option<Vec<(String, String)>>,
}

#[derive(Serialize, Deserialize)]
pub struct PaginatedResponse<T> {
    pub count: usize,
    pub next: Option<String>,
    pub previous: Option<String>,
    pub result: Vec<T>,
}

impl<T> Paginator<T> {
    pub fn new() -> Self {
        Self {
            ..Default::default()
        }
    }

    pub fn data(mut self, data: Vec<T>) -> Self {
        self.data = data;
        self
    }

    pub fn page(mut self, page: Option<usize>) -> Self {
        let page: usize = page.unwrap_or(1);
        if page == 0 {
            self.page = page;
        } else {
            self.page = page - 1;
        }
        self
    }

    pub fn page_size(mut self, page_size: Option<usize>) -> Self {
        let page_size: usize = page_size.unwrap_or(10);
        self.page_size = page_size;
        self
    }

    pub fn url(mut self, url: impl Into<String>) -> Self {
        self.url = url.into();
        self
    }

    pub fn pathname(mut self, pathname: impl Into<String>) -> Self {
        self.pathname = pathname.into();
        self
    }

    pub fn extra_params(mut self, extra_params: Vec<(String, String)>) -> Self {
        self.extra_params = Some(extra_params);
        self
    }

    pub fn into_paginated_response(self) -> PaginatedResponse<T> {
        let count: usize = self.data.len();
        let mut next: Option<String> = None;
        let mut previous: Option<String> = None;
        let result: Vec<T> = self
            .data
            .into_iter()
            .skip(self.page * self.page_size)
            .take(self.page_size)
            .collect();

        let is_next_page: bool = ((self.page + 1) * self.page_size) < count;
        let is_prev_page: bool = (self.page + 1) > 1;

        if is_next_page {
            let mut url: String = format!(
                "{}{}/?page={}&page_size={}",
                &self.url,
                &self.pathname,
                self.page + 2,
                self.page_size
            );

            if self.extra_params.is_some() {
                self.extra_params
                    .as_ref()
                    .unwrap()
                    .iter()
                    .for_each(|p: &(String, String)| {
                        let q: String = format!("&{}={}", p.0, p.1);
                        url.push_str(&q);
                    });
            }

            next = Some(url);
        }

        if is_prev_page {
            let mut url: String = format!(
                "{}{}/?page={}&page_size={}",
                &self.url, &self.pathname, self.page, self.page_size
            );

            if self.extra_params.is_some() {
                self.extra_params
                    .as_ref()
                    .unwrap()
                    .iter()
                    .for_each(|p: &(String, String)| {
                        let q: String = format!("&{}={}", p.0, p.1);
                        url.push_str(&q);
                    });
            }

            previous = Some(url);
        }

        PaginatedResponse {
            count,
            next,
            previous,
            result,
        }
    }
}

impl<T> Default for Paginator<T> {
    fn default() -> Self {
        Self {
            data: Vec::new(),
            page: 0,
            page_size: 5,
            url: "http://localhost:4000".to_string(),
            pathname: "/unknown".to_string(),
            extra_params: None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::APPNAME;

    #[test]
    fn test_paginator_into_paginated_reponse() {
        let data: Vec<&str> = vec!["marc", "leon"];
        let expected = PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: data.clone(),
        };
        assert_eq!(
            serde_json::to_string(&Paginator::new().data(data).into_paginated_response()).unwrap(),
            serde_json::to_string(&expected).unwrap()
        );
    }

    #[test]
    fn test_paginated_response_next_page() {
        let data: Vec<&str> = vec!["marc", "leon"];
        let expected: &str = "http://localhost:4000/unknown/?page=2&page_size=1";
        assert_eq!(
            Paginator::new()
                .data(data)
                .page_size(Some(1))
                .into_paginated_response()
                .next
                .unwrap(),
            expected,
        );
    }

    #[test]
    fn test_paginated_response_previous_page() {
        let data: Vec<&str> = vec!["marc", "leon"];
        let expected: &str = "http://localhost:4000/unknown/?page=1&page_size=1";
        assert_eq!(
            Paginator::new()
                .data(data)
                .page_size(Some(1))
                .page(Some(2))
                .into_paginated_response()
                .previous
                .unwrap(),
            expected,
        );
    }

    #[test]
    fn test_paginated_response_url() {
        let data: Vec<&str> = vec!["a", "b", "c", "d", "e", "f"];

        let url: &str = &format!("https://server.{APPNAME}.org");
        let expected_previous_url: String = format!("{}/unknown/?page=1&page_size=1", url);
        let expected_next_url: String = format!("{}/unknown/?page=3&page_size=1", url);

        let paginated_response = Paginator::new()
            .data(data)
            .page_size(Some(1))
            .page(Some(2))
            .url(url)
            .into_paginated_response();

        assert_eq!(paginated_response.previous.unwrap(), expected_previous_url);
        assert_eq!(paginated_response.next.unwrap(), expected_next_url);
    }

    #[test]
    fn test_paginated_response_pathname() {
        let data: Vec<&str> = vec!["a", "b", "c", "d", "e", "f"];

        let pathname: &str = "/fruits";
        let expected_previous_url: String =
            format!("http://localhost:4000{}/?page=1&page_size=1", pathname);
        let expected_next_url: String =
            format!("http://localhost:4000{}/?page=3&page_size=1", pathname);

        let paginated_response = Paginator::new()
            .data(data)
            .page_size(Some(1))
            .page(Some(2))
            .pathname(pathname)
            .into_paginated_response();

        assert_eq!(paginated_response.previous.unwrap(), expected_previous_url);
        assert_eq!(paginated_response.next.unwrap(), expected_next_url);
    }

    #[test]
    fn test_paginated_response_with_extra_params() {
        let data: Vec<&str> = vec!["marc", "leon"];
        let expected: &str = "http://localhost:4000/unknown/?page=2&page_size=1&test=test";

        assert_eq!(
            Paginator::new()
                .data(data)
                .page_size(Some(1))
                .extra_params(vec![("test".to_string(), "test".to_string())])
                .into_paginated_response()
                .next
                .unwrap(),
            expected,
        );
    }

    #[test]
    fn test_paginated_response_result() {
        let data: Vec<&str> = vec!["marc", "leon"];
        let expected = PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: data.clone(),
        };

        assert_eq!(
            Paginator::new().data(data).into_paginated_response().result,
            expected.result,
        );
    }

    #[test]
    fn test_paginated_response_count() {
        let data: Vec<&str> = vec!["marc", "leon"];
        let expected = PaginatedResponse {
            count: 2,
            next: None,
            previous: None,
            result: data.clone(),
        };

        assert_eq!(
            Paginator::new().data(data).into_paginated_response().count,
            expected.count,
        );
    }
}
